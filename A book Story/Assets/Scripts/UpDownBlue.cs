﻿using UnityEngine;

public class UpDownBlue : MonoBehaviour
{

    bool TengoQueBajar = false;
    int Rapidez = 1;

    void Update()
    {
        if (transform.position.y >= 3)
        {
            TengoQueBajar = true;
        }
        if (transform.position.y <= 2)
        {
            TengoQueBajar = false;
        }

        if (TengoQueBajar)
        {
            subir(); 
        }
        else
        {
            bajar();
        }

    }

    void subir()
    {
        transform.position += transform.up * Rapidez * Time.deltaTime;
    }

    void bajar()
    {
        transform.position -= transform.up * Rapidez * Time.deltaTime;
    }
}
