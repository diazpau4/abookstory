﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class camera : MonoBehaviour
{
    public GameObject Jugador;
    public Vector3 offset;

    void Start()
    {
        offset = transform.position - Jugador.transform.position;
    }

    void LateUpdate()
    {
        transform.position = Jugador.transform.position + offset;
      
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(1);
        }
    }
}
