﻿using UnityEngine;

public class UpDown : MonoBehaviour
{

    bool tengoQueBajar = false;
    int rapidez = 1;

    void Update()
    {
        if (transform.position.y >= 2.7)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 2)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}

