﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guardia0 : MonoBehaviour
{
    bool mover = false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.x >= -55)
        {
            mover = true;
        }
        if (transform.position.x <= -63)
        {
            mover = false;
        }

        if (mover)
        {
            der();
        }
        else
        {
            iz();
        }

    }

    void iz()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void der()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

}
