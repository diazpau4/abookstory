﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guardia2 : MonoBehaviour
{
    bool mover = false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.x >= -25)
        {
            mover = true;
        }
        if (transform.position.x <= -43)
        {
            mover = false;
        }

        if (mover)
        {
            der();
        }
        else
        {
            iz();
        }

    }

    void iz()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void der()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }
}
