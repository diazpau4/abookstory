﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


public class AudioMenu : MonoBehaviour
{
    public Slider Volume;
    public AudioMixer mixer;

    private void Awake()
    {

        Volume.onValueChanged.AddListener(ChangeVolumeMaster);

    }

    public void ChangeVolumeMaster(float v)
    {
        mixer.SetFloat("Volume", v);
    }
}
