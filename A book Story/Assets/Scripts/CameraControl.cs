﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    public float speedH;
    public float speedV;

    float yaw;
    float pitch;

    void Start()
    {
        
    }

   
    void Update()
    {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");

        pitch = Mathf.Clamp(pitch, -60.0f, 60.0f);
        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
    }
}
