﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Explocion ex;
    public float v = 5.0f;
    private Rigidbody rb;
    public CapsuleCollider col;
    public LayerMask piso;
    public float magnitudSalto = 7.0f;
    public GameObject jefe;
    public GameObject dispa;
    public GameObject dispa1;
    public Image im;
    public Text recolectados;
    private int cont;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cont = 0;
        textoss();
    }

    // Update is called once per frame
    void Update()
    {
        float adelanteatras = Input.GetAxis("Horizontal") * v;
        adelanteatras *= Time.deltaTime;
        transform.Translate(adelanteatras, 0, 0);
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }


        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

       
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, piso);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            
            
            cont += 1;
            textoss();
            other.gameObject.SetActive(false);

        }
        if (other.gameObject.CompareTag("jefe") == true)
        {
            jefe.SetActive(true);
        }
        if (other.gameObject.CompareTag("dispa") == true)
        {
            dispa.SetActive(true);
            dispa1.SetActive(true);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("enemigo"))
        {
            im.enabled = false;
            ex.enabled = true;

        }
        if (collision.gameObject.CompareTag("bala"))
        {
            
            im.enabled = false;
            ex.enabled = true;
        }
    }
    private void textoss()
    {
        recolectados.text = cont.ToString();
    }
}
