﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// InternalRealtimeCSG.CSGModelExported
struct CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90;
// InternalRealtimeCSG.GeneratedMeshInstance
struct GeneratedMeshInstance_tBD769E0C8E8CA45484343BF79C4021AB71238522;
// InternalRealtimeCSG.GeneratedMeshes
struct GeneratedMeshes_t5C134866E7C8EA7D47DD7147F7562979AAE48AD5;
// InternalRealtimeCSG.HiddenComponentData
struct HiddenComponentData_t21DAF9F1CB4ECB2271EB217863E5A5275364D4E6;
// InternalRealtimeCSG.HiddenComponentData[]
struct HiddenComponentDataU5BU5D_tAE885D531C808483BE4F9821CF9438AD1B8720C8;
// InternalRealtimeCSG.LegacyGeneratedMeshContainer
struct LegacyGeneratedMeshContainer_t4335E9BE434755096FC5770B2503EEE9FFB530AF;
// RealtimeCSG.Components.CSGBrush
struct CSGBrush_t5B9697019AE2C344733C0D257178EB7AB85C69DF;
// RealtimeCSG.Components.CSGModel
struct CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF;
// RealtimeCSG.Components.CSGNode
struct CSGNode_t2C6EA7769DD4175D7DD26DDA9947C29E91CB349C;
// RealtimeCSG.Components.CSGOperation
struct CSGOperation_t0598053E4D2FD430C5A27CE5ECE53B67C283C8DA;
// RealtimeCSG.EnumAsFlagsAttribute
struct EnumAsFlagsAttribute_t6143875D6DD196ECAB16C0C373116DF3C8D5B9E7;
// RealtimeCSG.Foundation.GeneratedMeshContents
struct GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C;
// RealtimeCSG.Legacy.ControlMesh
struct ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0;
// RealtimeCSG.Legacy.HalfEdge[]
struct HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436;
// RealtimeCSG.Legacy.Polygon
struct Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19;
// RealtimeCSG.Legacy.Polygon[]
struct PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513;
// RealtimeCSG.Legacy.Shape
struct Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F;
// RealtimeCSG.Legacy.Surface[]
struct SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C;
// RealtimeCSG.Legacy.TexGenFlags[]
struct TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628;
// RealtimeCSG.Legacy.TexGen[]
struct TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437;
// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Globalization.Calendar
struct Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5;
// System.Globalization.CompareInfo
struct CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1;
// System.Globalization.CultureData
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8;
// System.Globalization.TextInfo
struct TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8;
// System.IFormatProvider
struct IFormatProvider_t4247E13AE2D97A079B88D594B7ABABF313259901;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Component[]
struct ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral3D803514209E4FD4CD38079A03486581A84DA097;
IL2CPP_EXTERN_C String_t* _stringLiteral42F4000E95E51C94EC813EB1D8C5A408E16D34D9;
IL2CPP_EXTERN_C String_t* _stringLiteral52DA7846E396FF38CD6729BEC21B6F9159F9BD1C;
IL2CPP_EXTERN_C String_t* _stringLiteral6AB2A5D1FE0A10CB014FC4303709BA72103C7CD0;
IL2CPP_EXTERN_C String_t* _stringLiteral7F17C1A4474B196924E9DBB8952AFE2D51F71C90;
IL2CPP_EXTERN_C String_t* _stringLiteralB3063AE7AB0A9A3C4F8ED0DF283175F7F34A8CEE;
IL2CPP_EXTERN_C String_t* _stringLiteralEB047BACA03D70516231AF895D077E14DE1E26F6;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInChildren_TisComponent_t05064EF382ABCAF4B8C94F8A350EA85184C26621_m054FAB8B5909A3250B61889166E5952C68E687B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInChildren_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_m357A4E9F10F3DFA3823658320FD842261FD205C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t CSGMeshUtility_Clone_m4E3FDDC630BAFC1A52B2E34E9ED98FC179BA81D8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGModel__cctor_m68D842D99DC57996CD00259E16E32A676EF40B3B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGModel__ctor_m156A0623EA2BF1DF350B2D71E5F745CA8854FB2D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_Intersection_m77828575E659430171184D441835224E158F1898_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_op_Equality_m5DDC85F3465F45DB2045D95DAC2CCE87D4DD1E00_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CSGPlane_op_Inequality_m351E7463AB7335D358F0B8A814FFE638F67965B9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ControlMesh_Clone_m22DA0E5DE55549B13B5E4B2FB235A9764CC1E912_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ControlMesh_CopyFrom_m010324448CDA71EAC8776694628A7307CE186C42_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ControlMesh_GetVertex_mE0F471B92EB47F38895C9D97A12F066B885F9662_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ControlMesh_GetVertices_mD78ABF872903C01463776ADD972AC9667D4EB3FB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameObjectExtensions_Destroy_m7F688FC353A6D032E74735F218D717755882AD4F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameObjectExtensions_Destroy_mC3E6EF0B1DA3A96073259D8882A9CC5D23EF7375_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameObjectExtensions_SanitizeGameObject_mE95D20EB8201288ADEC9F31C694DF9CAC6D751DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameObjectExtensions_TryDestroy_m2336410EBE2F3B30E857C090F0392C4EB4AA0B8C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GeneratedMeshes_Awake_m09A83476C251049B04AA716B49E2015A5B39FE75_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LegacyGeneratedMeshContainer_Awake_m856864A9B92959D607A19300A513DA9D99B24D3F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MathConstants__cctor_m11C0D67BFD9B99DDDA20168C4981644FFA5BBC8F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Shape__ctor_m8D108B1E6FEEE23E0E75E1C7EF6D36D373B9BD65_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE_MetadataUsageId;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436;
struct PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513;
struct SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C;
struct TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628;
struct TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437;
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
struct ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155;
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
struct MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A;
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};


// System.Object


// GameObjectExtensions
struct  GameObjectExtensions_tEC5B50866DBD5ECF991D19826543DACC3252DA81  : public RuntimeObject
{
public:

public:
};


// RealtimeCSG.CSGMeshUtility
struct  CSGMeshUtility_tD98008D7C7C600637842EC2AA6E07817BE845B81  : public RuntimeObject
{
public:

public:
};


// RealtimeCSG.Legacy.ControlMesh
struct  ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] RealtimeCSG.Legacy.ControlMesh::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_0;
	// RealtimeCSG.Legacy.HalfEdge[] RealtimeCSG.Legacy.ControlMesh::Edges
	HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* ___Edges_1;
	// RealtimeCSG.Legacy.Polygon[] RealtimeCSG.Legacy.ControlMesh::Polygons
	PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* ___Polygons_2;
	// System.Boolean RealtimeCSG.Legacy.ControlMesh::valid
	bool ___valid_3;
	// System.Int32 RealtimeCSG.Legacy.ControlMesh::Generation
	int32_t ___Generation_4;

public:
	inline static int32_t get_offset_of_Vertices_0() { return static_cast<int32_t>(offsetof(ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0, ___Vertices_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_0() const { return ___Vertices_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_0() { return &___Vertices_0; }
	inline void set_Vertices_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Vertices_0), (void*)value);
	}

	inline static int32_t get_offset_of_Edges_1() { return static_cast<int32_t>(offsetof(ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0, ___Edges_1)); }
	inline HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* get_Edges_1() const { return ___Edges_1; }
	inline HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436** get_address_of_Edges_1() { return &___Edges_1; }
	inline void set_Edges_1(HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* value)
	{
		___Edges_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Edges_1), (void*)value);
	}

	inline static int32_t get_offset_of_Polygons_2() { return static_cast<int32_t>(offsetof(ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0, ___Polygons_2)); }
	inline PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* get_Polygons_2() const { return ___Polygons_2; }
	inline PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513** get_address_of_Polygons_2() { return &___Polygons_2; }
	inline void set_Polygons_2(PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* value)
	{
		___Polygons_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Polygons_2), (void*)value);
	}

	inline static int32_t get_offset_of_valid_3() { return static_cast<int32_t>(offsetof(ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0, ___valid_3)); }
	inline bool get_valid_3() const { return ___valid_3; }
	inline bool* get_address_of_valid_3() { return &___valid_3; }
	inline void set_valid_3(bool value)
	{
		___valid_3 = value;
	}

	inline static int32_t get_offset_of_Generation_4() { return static_cast<int32_t>(offsetof(ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0, ___Generation_4)); }
	inline int32_t get_Generation_4() const { return ___Generation_4; }
	inline int32_t* get_address_of_Generation_4() { return &___Generation_4; }
	inline void set_Generation_4(int32_t value)
	{
		___Generation_4 = value;
	}
};


// RealtimeCSG.Legacy.Polygon
struct  Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19  : public RuntimeObject
{
public:
	// System.Int32[] RealtimeCSG.Legacy.Polygon::EdgeIndices
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EdgeIndices_0;
	// System.Int32 RealtimeCSG.Legacy.Polygon::TexGenIndex
	int32_t ___TexGenIndex_1;

public:
	inline static int32_t get_offset_of_EdgeIndices_0() { return static_cast<int32_t>(offsetof(Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19, ___EdgeIndices_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EdgeIndices_0() const { return ___EdgeIndices_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EdgeIndices_0() { return &___EdgeIndices_0; }
	inline void set_EdgeIndices_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EdgeIndices_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EdgeIndices_0), (void*)value);
	}

	inline static int32_t get_offset_of_TexGenIndex_1() { return static_cast<int32_t>(offsetof(Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19, ___TexGenIndex_1)); }
	inline int32_t get_TexGenIndex_1() const { return ___TexGenIndex_1; }
	inline int32_t* get_address_of_TexGenIndex_1() { return &___TexGenIndex_1; }
	inline void set_TexGenIndex_1(int32_t value)
	{
		___TexGenIndex_1 = value;
	}
};


// RealtimeCSG.Legacy.Shape
struct  Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F  : public RuntimeObject
{
public:
	// System.Single RealtimeCSG.Legacy.Shape::Version
	float ___Version_0;
	// RealtimeCSG.Legacy.Surface[] RealtimeCSG.Legacy.Shape::Surfaces
	SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C* ___Surfaces_1;
	// RealtimeCSG.Legacy.TexGen[] RealtimeCSG.Legacy.Shape::TexGens
	TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437* ___TexGens_2;
	// RealtimeCSG.Legacy.TexGenFlags[] RealtimeCSG.Legacy.Shape::TexGenFlags
	TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628* ___TexGenFlags_3;
	// UnityEngine.Material[] RealtimeCSG.Legacy.Shape::Materials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___Materials_4;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F, ___Version_0)); }
	inline float get_Version_0() const { return ___Version_0; }
	inline float* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(float value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Surfaces_1() { return static_cast<int32_t>(offsetof(Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F, ___Surfaces_1)); }
	inline SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C* get_Surfaces_1() const { return ___Surfaces_1; }
	inline SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C** get_address_of_Surfaces_1() { return &___Surfaces_1; }
	inline void set_Surfaces_1(SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C* value)
	{
		___Surfaces_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Surfaces_1), (void*)value);
	}

	inline static int32_t get_offset_of_TexGens_2() { return static_cast<int32_t>(offsetof(Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F, ___TexGens_2)); }
	inline TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437* get_TexGens_2() const { return ___TexGens_2; }
	inline TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437** get_address_of_TexGens_2() { return &___TexGens_2; }
	inline void set_TexGens_2(TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437* value)
	{
		___TexGens_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TexGens_2), (void*)value);
	}

	inline static int32_t get_offset_of_TexGenFlags_3() { return static_cast<int32_t>(offsetof(Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F, ___TexGenFlags_3)); }
	inline TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628* get_TexGenFlags_3() const { return ___TexGenFlags_3; }
	inline TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628** get_address_of_TexGenFlags_3() { return &___TexGenFlags_3; }
	inline void set_TexGenFlags_3(TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628* value)
	{
		___TexGenFlags_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TexGenFlags_3), (void*)value);
	}

	inline static int32_t get_offset_of_Materials_4() { return static_cast<int32_t>(offsetof(Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F, ___Materials_4)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_Materials_4() const { return ___Materials_4; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_Materials_4() { return &___Materials_4; }
	inline void set_Materials_4(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___Materials_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Materials_4), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Globalization.CultureInfo
struct  CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___numInfo_10)); }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numInfo_10), (void*)value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dateTimeInfo_11), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textInfo_12)); }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_13), (void*)value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___englishname_14), (void*)value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativename_15), (void*)value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso3lang_16), (void*)value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso2lang_17), (void*)value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___win3lang_18), (void*)value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___territory_19), (void*)value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___native_calendar_names_20)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_calendar_names_20), (void*)value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___compareInfo_21)); }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compareInfo_21), (void*)value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___calendar_24)); }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___calendar_24), (void*)value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_culture_25)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_culture_25), (void*)value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cached_serialized_form_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_cultureData_28)); }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cultureData_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariant_culture_info_0), (void*)value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_table_lock_1), (void*)value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___default_current_culture_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentUICulture_33), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentCulture_34), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_number_35), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_name_36), (void*)value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// RealtimeCSG.Foundation.MeshQuery
#pragma pack(push, tp, 4)
struct  MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D 
{
public:
	// System.UInt32 RealtimeCSG.Foundation.MeshQuery::layers
	uint32_t ___layers_2;
	// System.UInt32 RealtimeCSG.Foundation.MeshQuery::maskAndChannels
	uint32_t ___maskAndChannels_3;

public:
	inline static int32_t get_offset_of_layers_2() { return static_cast<int32_t>(offsetof(MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D, ___layers_2)); }
	inline uint32_t get_layers_2() const { return ___layers_2; }
	inline uint32_t* get_address_of_layers_2() { return &___layers_2; }
	inline void set_layers_2(uint32_t value)
	{
		___layers_2 = value;
	}

	inline static int32_t get_offset_of_maskAndChannels_3() { return static_cast<int32_t>(offsetof(MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D, ___maskAndChannels_3)); }
	inline uint32_t get_maskAndChannels_3() const { return ___maskAndChannels_3; }
	inline uint32_t* get_address_of_maskAndChannels_3() { return &___maskAndChannels_3; }
	inline void set_maskAndChannels_3(uint32_t value)
	{
		___maskAndChannels_3 = value;
	}
};
#pragma pack(pop, tp)


// RealtimeCSG.Legacy.CSGPlane
#pragma pack(push, tp, 4)
struct  CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 
{
public:
	// System.Single RealtimeCSG.Legacy.CSGPlane::a
	float ___a_0;
	// System.Single RealtimeCSG.Legacy.CSGPlane::b
	float ___b_1;
	// System.Single RealtimeCSG.Legacy.CSGPlane::c
	float ___c_2;
	// System.Single RealtimeCSG.Legacy.CSGPlane::d
	float ___d_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403, ___a_0)); }
	inline float get_a_0() const { return ___a_0; }
	inline float* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(float value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403, ___b_1)); }
	inline float get_b_1() const { return ___b_1; }
	inline float* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(float value)
	{
		___b_1 = value;
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403, ___c_2)); }
	inline float get_c_2() const { return ___c_2; }
	inline float* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(float value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403, ___d_3)); }
	inline float get_d_3() const { return ___d_3; }
	inline float* get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(float value)
	{
		___d_3 = value;
	}
};
#pragma pack(pop, tp)


// RealtimeCSG.Legacy.HalfEdge
struct  HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 
{
public:
	// System.Int32 RealtimeCSG.Legacy.HalfEdge::TwinIndex
	int32_t ___TwinIndex_0;
	// System.Int16 RealtimeCSG.Legacy.HalfEdge::PolygonIndex
	int16_t ___PolygonIndex_1;
	// System.Boolean RealtimeCSG.Legacy.HalfEdge::HardEdge
	bool ___HardEdge_2;
	// System.Int16 RealtimeCSG.Legacy.HalfEdge::VertexIndex
	int16_t ___VertexIndex_3;

public:
	inline static int32_t get_offset_of_TwinIndex_0() { return static_cast<int32_t>(offsetof(HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42, ___TwinIndex_0)); }
	inline int32_t get_TwinIndex_0() const { return ___TwinIndex_0; }
	inline int32_t* get_address_of_TwinIndex_0() { return &___TwinIndex_0; }
	inline void set_TwinIndex_0(int32_t value)
	{
		___TwinIndex_0 = value;
	}

	inline static int32_t get_offset_of_PolygonIndex_1() { return static_cast<int32_t>(offsetof(HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42, ___PolygonIndex_1)); }
	inline int16_t get_PolygonIndex_1() const { return ___PolygonIndex_1; }
	inline int16_t* get_address_of_PolygonIndex_1() { return &___PolygonIndex_1; }
	inline void set_PolygonIndex_1(int16_t value)
	{
		___PolygonIndex_1 = value;
	}

	inline static int32_t get_offset_of_HardEdge_2() { return static_cast<int32_t>(offsetof(HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42, ___HardEdge_2)); }
	inline bool get_HardEdge_2() const { return ___HardEdge_2; }
	inline bool* get_address_of_HardEdge_2() { return &___HardEdge_2; }
	inline void set_HardEdge_2(bool value)
	{
		___HardEdge_2 = value;
	}

	inline static int32_t get_offset_of_VertexIndex_3() { return static_cast<int32_t>(offsetof(HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42, ___VertexIndex_3)); }
	inline int16_t get_VertexIndex_3() const { return ___VertexIndex_3; }
	inline int16_t* get_address_of_VertexIndex_3() { return &___VertexIndex_3; }
	inline void set_VertexIndex_3(int16_t value)
	{
		___VertexIndex_3 = value;
	}
};

// Native definition for P/Invoke marshalling of RealtimeCSG.Legacy.HalfEdge
struct HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_pinvoke
{
	int32_t ___TwinIndex_0;
	int16_t ___PolygonIndex_1;
	int32_t ___HardEdge_2;
	int16_t ___VertexIndex_3;
};
// Native definition for COM marshalling of RealtimeCSG.Legacy.HalfEdge
struct HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_com
{
	int32_t ___TwinIndex_0;
	int16_t ___PolygonIndex_1;
	int32_t ___HardEdge_2;
	int16_t ___VertexIndex_3;
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Powers10_6), (void*)value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int16
struct  Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// InternalRealtimeCSG.RenderSurfaceType
struct  RenderSurfaceType_t97BDE9A966C4A67CBD52B62BDB1ED6ED69229FC2 
{
public:
	// System.Int32 InternalRealtimeCSG.RenderSurfaceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderSurfaceType_t97BDE9A966C4A67CBD52B62BDB1ED6ED69229FC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Components.BrushFlags
struct  BrushFlags_tC2BA113E203F2BF2FD3CC1F8CA8C7BA4F15148EF 
{
public:
	// System.Int32 RealtimeCSG.Components.BrushFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BrushFlags_tC2BA113E203F2BF2FD3CC1F8CA8C7BA4F15148EF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Components.ExportType
struct  ExportType_t476FE698905009AB45957559C366F155508F7F08 
{
public:
	// System.Int32 RealtimeCSG.Components.ExportType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExportType_t476FE698905009AB45957559C366F155508F7F08, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Components.ModelSettingsFlags
struct  ModelSettingsFlags_t05EB62D28029CD49C6B47E802A38DB9E1E3479A4 
{
public:
	// System.Int32 RealtimeCSG.Components.ModelSettingsFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModelSettingsFlags_t05EB62D28029CD49C6B47E802A38DB9E1E3479A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Components.OriginType
struct  OriginType_t7EECDAA4C690D856EAD7FEAF11FE32EFF048847E 
{
public:
	// System.Int32 RealtimeCSG.Components.OriginType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OriginType_t7EECDAA4C690D856EAD7FEAF11FE32EFF048847E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.EnumAsFlagsAttribute
struct  EnumAsFlagsAttribute_t6143875D6DD196ECAB16C0C373116DF3C8D5B9E7  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};


// RealtimeCSG.Foundation.CSGOperationType
struct  CSGOperationType_tB2930C2AEFBF128A959243A58C563566E9DA141B 
{
public:
	// System.Byte RealtimeCSG.Foundation.CSGOperationType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSGOperationType_tB2930C2AEFBF128A959243A58C563566E9DA141B, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Foundation.GeneratedMeshDescription
#pragma pack(push, tp, 8)
struct  GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A 
{
public:
	// RealtimeCSG.Foundation.MeshQuery RealtimeCSG.Foundation.GeneratedMeshDescription::meshQuery
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___meshQuery_0;
	// System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::surfaceParameter
	int32_t ___surfaceParameter_1;
	// System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::meshQueryIndex
	int32_t ___meshQueryIndex_2;
	// System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::subMeshQueryIndex
	int32_t ___subMeshQueryIndex_3;
	// System.Int64 RealtimeCSG.Foundation.GeneratedMeshDescription::geometryHashValue
	int64_t ___geometryHashValue_4;
	// System.Int64 RealtimeCSG.Foundation.GeneratedMeshDescription::surfaceHashValue
	int64_t ___surfaceHashValue_5;
	// System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::vertexCount
	int32_t ___vertexCount_6;
	// System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::indexCount
	int32_t ___indexCount_7;

public:
	inline static int32_t get_offset_of_meshQuery_0() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___meshQuery_0)); }
	inline MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  get_meshQuery_0() const { return ___meshQuery_0; }
	inline MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * get_address_of_meshQuery_0() { return &___meshQuery_0; }
	inline void set_meshQuery_0(MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  value)
	{
		___meshQuery_0 = value;
	}

	inline static int32_t get_offset_of_surfaceParameter_1() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___surfaceParameter_1)); }
	inline int32_t get_surfaceParameter_1() const { return ___surfaceParameter_1; }
	inline int32_t* get_address_of_surfaceParameter_1() { return &___surfaceParameter_1; }
	inline void set_surfaceParameter_1(int32_t value)
	{
		___surfaceParameter_1 = value;
	}

	inline static int32_t get_offset_of_meshQueryIndex_2() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___meshQueryIndex_2)); }
	inline int32_t get_meshQueryIndex_2() const { return ___meshQueryIndex_2; }
	inline int32_t* get_address_of_meshQueryIndex_2() { return &___meshQueryIndex_2; }
	inline void set_meshQueryIndex_2(int32_t value)
	{
		___meshQueryIndex_2 = value;
	}

	inline static int32_t get_offset_of_subMeshQueryIndex_3() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___subMeshQueryIndex_3)); }
	inline int32_t get_subMeshQueryIndex_3() const { return ___subMeshQueryIndex_3; }
	inline int32_t* get_address_of_subMeshQueryIndex_3() { return &___subMeshQueryIndex_3; }
	inline void set_subMeshQueryIndex_3(int32_t value)
	{
		___subMeshQueryIndex_3 = value;
	}

	inline static int32_t get_offset_of_geometryHashValue_4() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___geometryHashValue_4)); }
	inline int64_t get_geometryHashValue_4() const { return ___geometryHashValue_4; }
	inline int64_t* get_address_of_geometryHashValue_4() { return &___geometryHashValue_4; }
	inline void set_geometryHashValue_4(int64_t value)
	{
		___geometryHashValue_4 = value;
	}

	inline static int32_t get_offset_of_surfaceHashValue_5() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___surfaceHashValue_5)); }
	inline int64_t get_surfaceHashValue_5() const { return ___surfaceHashValue_5; }
	inline int64_t* get_address_of_surfaceHashValue_5() { return &___surfaceHashValue_5; }
	inline void set_surfaceHashValue_5(int64_t value)
	{
		___surfaceHashValue_5 = value;
	}

	inline static int32_t get_offset_of_vertexCount_6() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___vertexCount_6)); }
	inline int32_t get_vertexCount_6() const { return ___vertexCount_6; }
	inline int32_t* get_address_of_vertexCount_6() { return &___vertexCount_6; }
	inline void set_vertexCount_6(int32_t value)
	{
		___vertexCount_6 = value;
	}

	inline static int32_t get_offset_of_indexCount_7() { return static_cast<int32_t>(offsetof(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A, ___indexCount_7)); }
	inline int32_t get_indexCount_7() const { return ___indexCount_7; }
	inline int32_t* get_address_of_indexCount_7() { return &___indexCount_7; }
	inline void set_indexCount_7(int32_t value)
	{
		___indexCount_7 = value;
	}
};
#pragma pack(pop, tp)


// RealtimeCSG.Foundation.LayerParameterIndex
struct  LayerParameterIndex_t36724AE6B8C287BA873C0C5ABDBBB4D5A6C1365F 
{
public:
	// System.Byte RealtimeCSG.Foundation.LayerParameterIndex::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LayerParameterIndex_t36724AE6B8C287BA873C0C5ABDBBB4D5A6C1365F, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Foundation.LayerUsageFlags
struct  LayerUsageFlags_t95335977A0799669F354755E6A83ED28B4224382 
{
public:
	// System.Int32 RealtimeCSG.Foundation.LayerUsageFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LayerUsageFlags_t95335977A0799669F354755E6A83ED28B4224382, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Foundation.VertexChannelFlags
struct  VertexChannelFlags_tE4A9732B741591C3B44650EFB8FEDD63FE7EC8E4 
{
public:
	// System.Byte RealtimeCSG.Foundation.VertexChannelFlags::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexChannelFlags_tE4A9732B741591C3B44650EFB8FEDD63FE7EC8E4, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// RealtimeCSG.Legacy.Surface
#pragma pack(push, tp, 4)
struct  Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 
{
public:
	// RealtimeCSG.Legacy.CSGPlane RealtimeCSG.Legacy.Surface::Plane
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___Plane_0;
	// UnityEngine.Vector3 RealtimeCSG.Legacy.Surface::Tangent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Tangent_1;
	// UnityEngine.Vector3 RealtimeCSG.Legacy.Surface::BiNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BiNormal_2;
	// System.Int32 RealtimeCSG.Legacy.Surface::TexGenIndex
	int32_t ___TexGenIndex_3;

public:
	inline static int32_t get_offset_of_Plane_0() { return static_cast<int32_t>(offsetof(Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17, ___Plane_0)); }
	inline CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  get_Plane_0() const { return ___Plane_0; }
	inline CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * get_address_of_Plane_0() { return &___Plane_0; }
	inline void set_Plane_0(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  value)
	{
		___Plane_0 = value;
	}

	inline static int32_t get_offset_of_Tangent_1() { return static_cast<int32_t>(offsetof(Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17, ___Tangent_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Tangent_1() const { return ___Tangent_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Tangent_1() { return &___Tangent_1; }
	inline void set_Tangent_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Tangent_1 = value;
	}

	inline static int32_t get_offset_of_BiNormal_2() { return static_cast<int32_t>(offsetof(Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17, ___BiNormal_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_BiNormal_2() const { return ___BiNormal_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_BiNormal_2() { return &___BiNormal_2; }
	inline void set_BiNormal_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___BiNormal_2 = value;
	}

	inline static int32_t get_offset_of_TexGenIndex_3() { return static_cast<int32_t>(offsetof(Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17, ___TexGenIndex_3)); }
	inline int32_t get_TexGenIndex_3() const { return ___TexGenIndex_3; }
	inline int32_t* get_address_of_TexGenIndex_3() { return &___TexGenIndex_3; }
	inline void set_TexGenIndex_3(int32_t value)
	{
		___TexGenIndex_3 = value;
	}
};
#pragma pack(pop, tp)


// RealtimeCSG.Legacy.TexGen
struct  TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 
{
public:
	// UnityEngine.Vector2 RealtimeCSG.Legacy.TexGen::Translation
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Translation_0;
	// UnityEngine.Vector2 RealtimeCSG.Legacy.TexGen::Scale
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Scale_1;
	// System.Single RealtimeCSG.Legacy.TexGen::RotationAngle
	float ___RotationAngle_2;
	// UnityEngine.Material RealtimeCSG.Legacy.TexGen::RenderMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___RenderMaterial_3;
	// UnityEngine.PhysicMaterial RealtimeCSG.Legacy.TexGen::PhysicsMaterial
	PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___PhysicsMaterial_4;
	// System.UInt32 RealtimeCSG.Legacy.TexGen::SmoothingGroup
	uint32_t ___SmoothingGroup_5;

public:
	inline static int32_t get_offset_of_Translation_0() { return static_cast<int32_t>(offsetof(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45, ___Translation_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Translation_0() const { return ___Translation_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Translation_0() { return &___Translation_0; }
	inline void set_Translation_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Translation_0 = value;
	}

	inline static int32_t get_offset_of_Scale_1() { return static_cast<int32_t>(offsetof(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45, ___Scale_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Scale_1() const { return ___Scale_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Scale_1() { return &___Scale_1; }
	inline void set_Scale_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Scale_1 = value;
	}

	inline static int32_t get_offset_of_RotationAngle_2() { return static_cast<int32_t>(offsetof(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45, ___RotationAngle_2)); }
	inline float get_RotationAngle_2() const { return ___RotationAngle_2; }
	inline float* get_address_of_RotationAngle_2() { return &___RotationAngle_2; }
	inline void set_RotationAngle_2(float value)
	{
		___RotationAngle_2 = value;
	}

	inline static int32_t get_offset_of_RenderMaterial_3() { return static_cast<int32_t>(offsetof(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45, ___RenderMaterial_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_RenderMaterial_3() const { return ___RenderMaterial_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_RenderMaterial_3() { return &___RenderMaterial_3; }
	inline void set_RenderMaterial_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___RenderMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RenderMaterial_3), (void*)value);
	}

	inline static int32_t get_offset_of_PhysicsMaterial_4() { return static_cast<int32_t>(offsetof(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45, ___PhysicsMaterial_4)); }
	inline PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * get_PhysicsMaterial_4() const { return ___PhysicsMaterial_4; }
	inline PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 ** get_address_of_PhysicsMaterial_4() { return &___PhysicsMaterial_4; }
	inline void set_PhysicsMaterial_4(PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * value)
	{
		___PhysicsMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PhysicsMaterial_4), (void*)value);
	}

	inline static int32_t get_offset_of_SmoothingGroup_5() { return static_cast<int32_t>(offsetof(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45, ___SmoothingGroup_5)); }
	inline uint32_t get_SmoothingGroup_5() const { return ___SmoothingGroup_5; }
	inline uint32_t* get_address_of_SmoothingGroup_5() { return &___SmoothingGroup_5; }
	inline void set_SmoothingGroup_5(uint32_t value)
	{
		___SmoothingGroup_5 = value;
	}
};

// Native definition for P/Invoke marshalling of RealtimeCSG.Legacy.TexGen
#pragma pack(push, tp, 4)
struct TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_pinvoke
{
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Translation_0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Scale_1;
	float ___RotationAngle_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___RenderMaterial_3;
	PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___PhysicsMaterial_4;
	uint32_t ___SmoothingGroup_5;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of RealtimeCSG.Legacy.TexGen
#pragma pack(push, tp, 4)
struct TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_com
{
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Translation_0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Scale_1;
	float ___RotationAngle_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___RenderMaterial_3;
	PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___PhysicsMaterial_4;
	uint32_t ___SmoothingGroup_5;
};
#pragma pack(pop, tp)

// RealtimeCSG.Legacy.TexGenFlags
struct  TexGenFlags_tA7E3299A69F0CFD0F6CA7625B617D1C1133CCA98 
{
public:
	// System.Int32 RealtimeCSG.Legacy.TexGenFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TexGenFlags_tA7E3299A69F0CFD0F6CA7625B617D1C1133CCA98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.HideFlags
struct  HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.MeshColliderCookingOptions
struct  MeshColliderCookingOptions_t1065EBBF838E25FE864D3F1347B146D5448D8385 
{
public:
	// System.Int32 UnityEngine.MeshColliderCookingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MeshColliderCookingOptions_t1065EBBF838E25FE864D3F1347B146D5448D8385, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Plane
struct  Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Normal_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};


// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.ReceiveGI
struct  ReceiveGI_t56EBCFF10018B77A3C4E37B587D75967D96A4E59 
{
public:
	// System.Int32 UnityEngine.ReceiveGI::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReceiveGI_t56EBCFF10018B77A3C4E37B587D75967D96A4E59, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// InternalRealtimeCSG.HiddenComponentData
struct  HiddenComponentData_t21DAF9F1CB4ECB2271EB217863E5A5275364D4E6  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour InternalRealtimeCSG.HiddenComponentData::behaviour
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___behaviour_0;
	// System.Boolean InternalRealtimeCSG.HiddenComponentData::enabled
	bool ___enabled_1;
	// UnityEngine.HideFlags InternalRealtimeCSG.HiddenComponentData::hideFlags
	int32_t ___hideFlags_2;

public:
	inline static int32_t get_offset_of_behaviour_0() { return static_cast<int32_t>(offsetof(HiddenComponentData_t21DAF9F1CB4ECB2271EB217863E5A5275364D4E6, ___behaviour_0)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_behaviour_0() const { return ___behaviour_0; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_behaviour_0() { return &___behaviour_0; }
	inline void set_behaviour_0(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___behaviour_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___behaviour_0), (void*)value);
	}

	inline static int32_t get_offset_of_enabled_1() { return static_cast<int32_t>(offsetof(HiddenComponentData_t21DAF9F1CB4ECB2271EB217863E5A5275364D4E6, ___enabled_1)); }
	inline bool get_enabled_1() const { return ___enabled_1; }
	inline bool* get_address_of_enabled_1() { return &___enabled_1; }
	inline void set_enabled_1(bool value)
	{
		___enabled_1 = value;
	}

	inline static int32_t get_offset_of_hideFlags_2() { return static_cast<int32_t>(offsetof(HiddenComponentData_t21DAF9F1CB4ECB2271EB217863E5A5275364D4E6, ___hideFlags_2)); }
	inline int32_t get_hideFlags_2() const { return ___hideFlags_2; }
	inline int32_t* get_address_of_hideFlags_2() { return &___hideFlags_2; }
	inline void set_hideFlags_2(int32_t value)
	{
		___hideFlags_2 = value;
	}
};


// InternalRealtimeCSG.MathConstants
struct  MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA  : public RuntimeObject
{
public:

public:
};

struct MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields
{
public:
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::zeroVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector3_16;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::oneVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector3_17;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::unitXVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___unitXVector3_18;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::unitYVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___unitYVector3_19;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::unitZVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___unitZVector3_20;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::upVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector3_21;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::forwardVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector3_22;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::rightVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector3_23;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::leftVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector3_24;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::downVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector3_25;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::backVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector3_26;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::NaNVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___NaNVector3_27;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::PositiveInfinityVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___PositiveInfinityVector3_28;
	// UnityEngine.Vector3 InternalRealtimeCSG.MathConstants::NegativeInfinityVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___NegativeInfinityVector3_29;
	// UnityEngine.Vector2 InternalRealtimeCSG.MathConstants::zeroVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector2_30;
	// UnityEngine.Vector2 InternalRealtimeCSG.MathConstants::oneVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector2_31;
	// UnityEngine.Vector2 InternalRealtimeCSG.MathConstants::upVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector2_32;
	// UnityEngine.Ray InternalRealtimeCSG.MathConstants::emptyRay
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___emptyRay_33;
	// UnityEngine.Matrix4x4 InternalRealtimeCSG.MathConstants::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_34;
	// UnityEngine.Quaternion InternalRealtimeCSG.MathConstants::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_35;
	// UnityEngine.Quaternion InternalRealtimeCSG.MathConstants::YQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___YQuaternion_36;
	// UnityEngine.Quaternion InternalRealtimeCSG.MathConstants::ZQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___ZQuaternion_37;
	// UnityEngine.Quaternion InternalRealtimeCSG.MathConstants::XQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___XQuaternion_38;

public:
	inline static int32_t get_offset_of_zeroVector3_16() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___zeroVector3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector3_16() const { return ___zeroVector3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector3_16() { return &___zeroVector3_16; }
	inline void set_zeroVector3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector3_16 = value;
	}

	inline static int32_t get_offset_of_oneVector3_17() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___oneVector3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector3_17() const { return ___oneVector3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector3_17() { return &___oneVector3_17; }
	inline void set_oneVector3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector3_17 = value;
	}

	inline static int32_t get_offset_of_unitXVector3_18() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___unitXVector3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_unitXVector3_18() const { return ___unitXVector3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_unitXVector3_18() { return &___unitXVector3_18; }
	inline void set_unitXVector3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___unitXVector3_18 = value;
	}

	inline static int32_t get_offset_of_unitYVector3_19() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___unitYVector3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_unitYVector3_19() const { return ___unitYVector3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_unitYVector3_19() { return &___unitYVector3_19; }
	inline void set_unitYVector3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___unitYVector3_19 = value;
	}

	inline static int32_t get_offset_of_unitZVector3_20() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___unitZVector3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_unitZVector3_20() const { return ___unitZVector3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_unitZVector3_20() { return &___unitZVector3_20; }
	inline void set_unitZVector3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___unitZVector3_20 = value;
	}

	inline static int32_t get_offset_of_upVector3_21() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___upVector3_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector3_21() const { return ___upVector3_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector3_21() { return &___upVector3_21; }
	inline void set_upVector3_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector3_21 = value;
	}

	inline static int32_t get_offset_of_forwardVector3_22() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___forwardVector3_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector3_22() const { return ___forwardVector3_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector3_22() { return &___forwardVector3_22; }
	inline void set_forwardVector3_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector3_22 = value;
	}

	inline static int32_t get_offset_of_rightVector3_23() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___rightVector3_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector3_23() const { return ___rightVector3_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector3_23() { return &___rightVector3_23; }
	inline void set_rightVector3_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector3_23 = value;
	}

	inline static int32_t get_offset_of_leftVector3_24() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___leftVector3_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector3_24() const { return ___leftVector3_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector3_24() { return &___leftVector3_24; }
	inline void set_leftVector3_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector3_24 = value;
	}

	inline static int32_t get_offset_of_downVector3_25() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___downVector3_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector3_25() const { return ___downVector3_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector3_25() { return &___downVector3_25; }
	inline void set_downVector3_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector3_25 = value;
	}

	inline static int32_t get_offset_of_backVector3_26() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___backVector3_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector3_26() const { return ___backVector3_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector3_26() { return &___backVector3_26; }
	inline void set_backVector3_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector3_26 = value;
	}

	inline static int32_t get_offset_of_NaNVector3_27() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___NaNVector3_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_NaNVector3_27() const { return ___NaNVector3_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_NaNVector3_27() { return &___NaNVector3_27; }
	inline void set_NaNVector3_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___NaNVector3_27 = value;
	}

	inline static int32_t get_offset_of_PositiveInfinityVector3_28() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___PositiveInfinityVector3_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_PositiveInfinityVector3_28() const { return ___PositiveInfinityVector3_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_PositiveInfinityVector3_28() { return &___PositiveInfinityVector3_28; }
	inline void set_PositiveInfinityVector3_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___PositiveInfinityVector3_28 = value;
	}

	inline static int32_t get_offset_of_NegativeInfinityVector3_29() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___NegativeInfinityVector3_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_NegativeInfinityVector3_29() const { return ___NegativeInfinityVector3_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_NegativeInfinityVector3_29() { return &___NegativeInfinityVector3_29; }
	inline void set_NegativeInfinityVector3_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___NegativeInfinityVector3_29 = value;
	}

	inline static int32_t get_offset_of_zeroVector2_30() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___zeroVector2_30)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector2_30() const { return ___zeroVector2_30; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector2_30() { return &___zeroVector2_30; }
	inline void set_zeroVector2_30(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector2_30 = value;
	}

	inline static int32_t get_offset_of_oneVector2_31() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___oneVector2_31)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector2_31() const { return ___oneVector2_31; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector2_31() { return &___oneVector2_31; }
	inline void set_oneVector2_31(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector2_31 = value;
	}

	inline static int32_t get_offset_of_upVector2_32() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___upVector2_32)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector2_32() const { return ___upVector2_32; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector2_32() { return &___upVector2_32; }
	inline void set_upVector2_32(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector2_32 = value;
	}

	inline static int32_t get_offset_of_emptyRay_33() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___emptyRay_33)); }
	inline Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  get_emptyRay_33() const { return ___emptyRay_33; }
	inline Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * get_address_of_emptyRay_33() { return &___emptyRay_33; }
	inline void set_emptyRay_33(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  value)
	{
		___emptyRay_33 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_34() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___identityMatrix_34)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_34() const { return ___identityMatrix_34; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_34() { return &___identityMatrix_34; }
	inline void set_identityMatrix_34(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_34 = value;
	}

	inline static int32_t get_offset_of_identityQuaternion_35() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___identityQuaternion_35)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_35() const { return ___identityQuaternion_35; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_35() { return &___identityQuaternion_35; }
	inline void set_identityQuaternion_35(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_35 = value;
	}

	inline static int32_t get_offset_of_YQuaternion_36() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___YQuaternion_36)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_YQuaternion_36() const { return ___YQuaternion_36; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_YQuaternion_36() { return &___YQuaternion_36; }
	inline void set_YQuaternion_36(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___YQuaternion_36 = value;
	}

	inline static int32_t get_offset_of_ZQuaternion_37() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___ZQuaternion_37)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_ZQuaternion_37() const { return ___ZQuaternion_37; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_ZQuaternion_37() { return &___ZQuaternion_37; }
	inline void set_ZQuaternion_37(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___ZQuaternion_37 = value;
	}

	inline static int32_t get_offset_of_XQuaternion_38() { return static_cast<int32_t>(offsetof(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields, ___XQuaternion_38)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_XQuaternion_38() const { return ___XQuaternion_38; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_XQuaternion_38() { return &___XQuaternion_38; }
	inline void set_XQuaternion_38(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___XQuaternion_38 = value;
	}
};


// RealtimeCSG.Foundation.GeneratedMeshContents
struct  GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C  : public RuntimeObject
{
public:
	// RealtimeCSG.Foundation.GeneratedMeshDescription RealtimeCSG.Foundation.GeneratedMeshContents::description
	GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  ___description_0;
	// System.Int32[] RealtimeCSG.Foundation.GeneratedMeshContents::indices
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___indices_1;
	// UnityEngine.Vector3[] RealtimeCSG.Foundation.GeneratedMeshContents::positions
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___positions_2;
	// UnityEngine.Vector4[] RealtimeCSG.Foundation.GeneratedMeshContents::tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___tangents_3;
	// UnityEngine.Vector3[] RealtimeCSG.Foundation.GeneratedMeshContents::normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals_4;
	// UnityEngine.Vector2[] RealtimeCSG.Foundation.GeneratedMeshContents::uv0
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___uv0_5;
	// UnityEngine.Bounds RealtimeCSG.Foundation.GeneratedMeshContents::bounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___bounds_6;

public:
	inline static int32_t get_offset_of_description_0() { return static_cast<int32_t>(offsetof(GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C, ___description_0)); }
	inline GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  get_description_0() const { return ___description_0; }
	inline GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * get_address_of_description_0() { return &___description_0; }
	inline void set_description_0(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  value)
	{
		___description_0 = value;
	}

	inline static int32_t get_offset_of_indices_1() { return static_cast<int32_t>(offsetof(GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C, ___indices_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_indices_1() const { return ___indices_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_indices_1() { return &___indices_1; }
	inline void set_indices_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___indices_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___indices_1), (void*)value);
	}

	inline static int32_t get_offset_of_positions_2() { return static_cast<int32_t>(offsetof(GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C, ___positions_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_positions_2() const { return ___positions_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_positions_2() { return &___positions_2; }
	inline void set_positions_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___positions_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positions_2), (void*)value);
	}

	inline static int32_t get_offset_of_tangents_3() { return static_cast<int32_t>(offsetof(GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C, ___tangents_3)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_tangents_3() const { return ___tangents_3; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_tangents_3() { return &___tangents_3; }
	inline void set_tangents_3(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___tangents_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tangents_3), (void*)value);
	}

	inline static int32_t get_offset_of_normals_4() { return static_cast<int32_t>(offsetof(GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C, ___normals_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_normals_4() const { return ___normals_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_normals_4() { return &___normals_4; }
	inline void set_normals_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___normals_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normals_4), (void*)value);
	}

	inline static int32_t get_offset_of_uv0_5() { return static_cast<int32_t>(offsetof(GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C, ___uv0_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_uv0_5() const { return ___uv0_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_uv0_5() { return &___uv0_5; }
	inline void set_uv0_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___uv0_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uv0_5), (void*)value);
	}

	inline static int32_t get_offset_of_bounds_6() { return static_cast<int32_t>(offsetof(GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C, ___bounds_6)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_bounds_6() const { return ___bounds_6; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_bounds_6() { return &___bounds_6; }
	inline void set_bounds_6(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___bounds_6 = value;
	}
};


// RealtimeCSG.Foundation.SurfaceLayers
#pragma pack(push, tp, 4)
struct  SurfaceLayers_tF98D43B2FB5F40FB298CFCC915FCFBACE1A595F6 
{
public:
	// RealtimeCSG.Foundation.LayerUsageFlags RealtimeCSG.Foundation.SurfaceLayers::layerUsage
	int32_t ___layerUsage_0;
	// System.Int32 RealtimeCSG.Foundation.SurfaceLayers::layerParameter1
	int32_t ___layerParameter1_1;
	// System.Int32 RealtimeCSG.Foundation.SurfaceLayers::layerParameter2
	int32_t ___layerParameter2_2;
	// System.Int32 RealtimeCSG.Foundation.SurfaceLayers::layerParameter3
	int32_t ___layerParameter3_3;

public:
	inline static int32_t get_offset_of_layerUsage_0() { return static_cast<int32_t>(offsetof(SurfaceLayers_tF98D43B2FB5F40FB298CFCC915FCFBACE1A595F6, ___layerUsage_0)); }
	inline int32_t get_layerUsage_0() const { return ___layerUsage_0; }
	inline int32_t* get_address_of_layerUsage_0() { return &___layerUsage_0; }
	inline void set_layerUsage_0(int32_t value)
	{
		___layerUsage_0 = value;
	}

	inline static int32_t get_offset_of_layerParameter1_1() { return static_cast<int32_t>(offsetof(SurfaceLayers_tF98D43B2FB5F40FB298CFCC915FCFBACE1A595F6, ___layerParameter1_1)); }
	inline int32_t get_layerParameter1_1() const { return ___layerParameter1_1; }
	inline int32_t* get_address_of_layerParameter1_1() { return &___layerParameter1_1; }
	inline void set_layerParameter1_1(int32_t value)
	{
		___layerParameter1_1 = value;
	}

	inline static int32_t get_offset_of_layerParameter2_2() { return static_cast<int32_t>(offsetof(SurfaceLayers_tF98D43B2FB5F40FB298CFCC915FCFBACE1A595F6, ___layerParameter2_2)); }
	inline int32_t get_layerParameter2_2() const { return ___layerParameter2_2; }
	inline int32_t* get_address_of_layerParameter2_2() { return &___layerParameter2_2; }
	inline void set_layerParameter2_2(int32_t value)
	{
		___layerParameter2_2 = value;
	}

	inline static int32_t get_offset_of_layerParameter3_3() { return static_cast<int32_t>(offsetof(SurfaceLayers_tF98D43B2FB5F40FB298CFCC915FCFBACE1A595F6, ___layerParameter3_3)); }
	inline int32_t get_layerParameter3_3() const { return ___layerParameter3_3; }
	inline int32_t* get_address_of_layerParameter3_3() { return &___layerParameter3_3; }
	inline void set_layerParameter3_3(int32_t value)
	{
		___layerParameter3_3 = value;
	}
};
#pragma pack(pop, tp)


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Mesh
struct  Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.PhysicMaterial
struct  PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// InternalRealtimeCSG.CSGModelExported
struct  CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single InternalRealtimeCSG.CSGModelExported::Version
	float ___Version_4;
	// RealtimeCSG.Components.CSGModel InternalRealtimeCSG.CSGModelExported::containedModel
	CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * ___containedModel_5;
	// UnityEngine.GameObject InternalRealtimeCSG.CSGModelExported::containedExportedModel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___containedExportedModel_6;
	// InternalRealtimeCSG.HiddenComponentData[] InternalRealtimeCSG.CSGModelExported::hiddenComponents
	HiddenComponentDataU5BU5D_tAE885D531C808483BE4F9821CF9438AD1B8720C8* ___hiddenComponents_7;
	// System.Boolean InternalRealtimeCSG.CSGModelExported::disarm
	bool ___disarm_8;

public:
	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90, ___Version_4)); }
	inline float get_Version_4() const { return ___Version_4; }
	inline float* get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(float value)
	{
		___Version_4 = value;
	}

	inline static int32_t get_offset_of_containedModel_5() { return static_cast<int32_t>(offsetof(CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90, ___containedModel_5)); }
	inline CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * get_containedModel_5() const { return ___containedModel_5; }
	inline CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF ** get_address_of_containedModel_5() { return &___containedModel_5; }
	inline void set_containedModel_5(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * value)
	{
		___containedModel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___containedModel_5), (void*)value);
	}

	inline static int32_t get_offset_of_containedExportedModel_6() { return static_cast<int32_t>(offsetof(CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90, ___containedExportedModel_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_containedExportedModel_6() const { return ___containedExportedModel_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_containedExportedModel_6() { return &___containedExportedModel_6; }
	inline void set_containedExportedModel_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___containedExportedModel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___containedExportedModel_6), (void*)value);
	}

	inline static int32_t get_offset_of_hiddenComponents_7() { return static_cast<int32_t>(offsetof(CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90, ___hiddenComponents_7)); }
	inline HiddenComponentDataU5BU5D_tAE885D531C808483BE4F9821CF9438AD1B8720C8* get_hiddenComponents_7() const { return ___hiddenComponents_7; }
	inline HiddenComponentDataU5BU5D_tAE885D531C808483BE4F9821CF9438AD1B8720C8** get_address_of_hiddenComponents_7() { return &___hiddenComponents_7; }
	inline void set_hiddenComponents_7(HiddenComponentDataU5BU5D_tAE885D531C808483BE4F9821CF9438AD1B8720C8* value)
	{
		___hiddenComponents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hiddenComponents_7), (void*)value);
	}

	inline static int32_t get_offset_of_disarm_8() { return static_cast<int32_t>(offsetof(CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90, ___disarm_8)); }
	inline bool get_disarm_8() const { return ___disarm_8; }
	inline bool* get_address_of_disarm_8() { return &___disarm_8; }
	inline void set_disarm_8(bool value)
	{
		___disarm_8 = value;
	}
};


// InternalRealtimeCSG.GeneratedMeshInstance
struct  GeneratedMeshInstance_tBD769E0C8E8CA45484343BF79C4021AB71238522  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single InternalRealtimeCSG.GeneratedMeshInstance::Version
	float ___Version_4;

public:
	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(GeneratedMeshInstance_tBD769E0C8E8CA45484343BF79C4021AB71238522, ___Version_4)); }
	inline float get_Version_4() const { return ___Version_4; }
	inline float* get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(float value)
	{
		___Version_4 = value;
	}
};


// InternalRealtimeCSG.GeneratedMeshes
struct  GeneratedMeshes_t5C134866E7C8EA7D47DD7147F7562979AAE48AD5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single InternalRealtimeCSG.GeneratedMeshes::Version
	float ___Version_4;

public:
	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(GeneratedMeshes_t5C134866E7C8EA7D47DD7147F7562979AAE48AD5, ___Version_4)); }
	inline float get_Version_4() const { return ___Version_4; }
	inline float* get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(float value)
	{
		___Version_4 = value;
	}
};


// InternalRealtimeCSG.LegacyGeneratedMeshContainer
struct  LegacyGeneratedMeshContainer_t4335E9BE434755096FC5770B2503EEE9FFB530AF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// RealtimeCSG.Components.CSGNode
struct  CSGNode_t2C6EA7769DD4175D7DD26DDA9947C29E91CB349C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// RealtimeCSG.Components.CSGBrush
struct  CSGBrush_t5B9697019AE2C344733C0D257178EB7AB85C69DF  : public CSGNode_t2C6EA7769DD4175D7DD26DDA9947C29E91CB349C
{
public:
	// System.Single RealtimeCSG.Components.CSGBrush::Version
	float ___Version_5;

public:
	inline static int32_t get_offset_of_Version_5() { return static_cast<int32_t>(offsetof(CSGBrush_t5B9697019AE2C344733C0D257178EB7AB85C69DF, ___Version_5)); }
	inline float get_Version_5() const { return ___Version_5; }
	inline float* get_address_of_Version_5() { return &___Version_5; }
	inline void set_Version_5(float value)
	{
		___Version_5 = value;
	}
};


// RealtimeCSG.Components.CSGModel
struct  CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF  : public CSGNode_t2C6EA7769DD4175D7DD26DDA9947C29E91CB349C
{
public:
	// System.Single RealtimeCSG.Components.CSGModel::Version
	float ___Version_6;
	// RealtimeCSG.Components.ModelSettingsFlags RealtimeCSG.Components.CSGModel::Settings
	int32_t ___Settings_7;
	// RealtimeCSG.Foundation.VertexChannelFlags RealtimeCSG.Components.CSGModel::VertexChannels
	uint8_t ___VertexChannels_8;
	// UnityEngine.ReceiveGI RealtimeCSG.Components.CSGModel::ReceiveGI
	int32_t ___ReceiveGI_9;
	// UnityEngine.MeshColliderCookingOptions RealtimeCSG.Components.CSGModel::MeshColliderCookingOptions
	int32_t ___MeshColliderCookingOptions_10;
	// System.Single RealtimeCSG.Components.CSGModel::angleError
	float ___angleError_15;
	// System.Single RealtimeCSG.Components.CSGModel::areaError
	float ___areaError_16;
	// System.Single RealtimeCSG.Components.CSGModel::hardAngle
	float ___hardAngle_17;
	// System.Single RealtimeCSG.Components.CSGModel::packMargin
	float ___packMargin_18;
	// System.Single RealtimeCSG.Components.CSGModel::scaleInLightmap
	float ___scaleInLightmap_19;
	// System.Single RealtimeCSG.Components.CSGModel::autoUVMaxDistance
	float ___autoUVMaxDistance_20;
	// System.Single RealtimeCSG.Components.CSGModel::autoUVMaxAngle
	float ___autoUVMaxAngle_21;
	// System.Int32 RealtimeCSG.Components.CSGModel::minimumChartSize
	int32_t ___minimumChartSize_22;

public:
	inline static int32_t get_offset_of_Version_6() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___Version_6)); }
	inline float get_Version_6() const { return ___Version_6; }
	inline float* get_address_of_Version_6() { return &___Version_6; }
	inline void set_Version_6(float value)
	{
		___Version_6 = value;
	}

	inline static int32_t get_offset_of_Settings_7() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___Settings_7)); }
	inline int32_t get_Settings_7() const { return ___Settings_7; }
	inline int32_t* get_address_of_Settings_7() { return &___Settings_7; }
	inline void set_Settings_7(int32_t value)
	{
		___Settings_7 = value;
	}

	inline static int32_t get_offset_of_VertexChannels_8() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___VertexChannels_8)); }
	inline uint8_t get_VertexChannels_8() const { return ___VertexChannels_8; }
	inline uint8_t* get_address_of_VertexChannels_8() { return &___VertexChannels_8; }
	inline void set_VertexChannels_8(uint8_t value)
	{
		___VertexChannels_8 = value;
	}

	inline static int32_t get_offset_of_ReceiveGI_9() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___ReceiveGI_9)); }
	inline int32_t get_ReceiveGI_9() const { return ___ReceiveGI_9; }
	inline int32_t* get_address_of_ReceiveGI_9() { return &___ReceiveGI_9; }
	inline void set_ReceiveGI_9(int32_t value)
	{
		___ReceiveGI_9 = value;
	}

	inline static int32_t get_offset_of_MeshColliderCookingOptions_10() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___MeshColliderCookingOptions_10)); }
	inline int32_t get_MeshColliderCookingOptions_10() const { return ___MeshColliderCookingOptions_10; }
	inline int32_t* get_address_of_MeshColliderCookingOptions_10() { return &___MeshColliderCookingOptions_10; }
	inline void set_MeshColliderCookingOptions_10(int32_t value)
	{
		___MeshColliderCookingOptions_10 = value;
	}

	inline static int32_t get_offset_of_angleError_15() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___angleError_15)); }
	inline float get_angleError_15() const { return ___angleError_15; }
	inline float* get_address_of_angleError_15() { return &___angleError_15; }
	inline void set_angleError_15(float value)
	{
		___angleError_15 = value;
	}

	inline static int32_t get_offset_of_areaError_16() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___areaError_16)); }
	inline float get_areaError_16() const { return ___areaError_16; }
	inline float* get_address_of_areaError_16() { return &___areaError_16; }
	inline void set_areaError_16(float value)
	{
		___areaError_16 = value;
	}

	inline static int32_t get_offset_of_hardAngle_17() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___hardAngle_17)); }
	inline float get_hardAngle_17() const { return ___hardAngle_17; }
	inline float* get_address_of_hardAngle_17() { return &___hardAngle_17; }
	inline void set_hardAngle_17(float value)
	{
		___hardAngle_17 = value;
	}

	inline static int32_t get_offset_of_packMargin_18() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___packMargin_18)); }
	inline float get_packMargin_18() const { return ___packMargin_18; }
	inline float* get_address_of_packMargin_18() { return &___packMargin_18; }
	inline void set_packMargin_18(float value)
	{
		___packMargin_18 = value;
	}

	inline static int32_t get_offset_of_scaleInLightmap_19() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___scaleInLightmap_19)); }
	inline float get_scaleInLightmap_19() const { return ___scaleInLightmap_19; }
	inline float* get_address_of_scaleInLightmap_19() { return &___scaleInLightmap_19; }
	inline void set_scaleInLightmap_19(float value)
	{
		___scaleInLightmap_19 = value;
	}

	inline static int32_t get_offset_of_autoUVMaxDistance_20() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___autoUVMaxDistance_20)); }
	inline float get_autoUVMaxDistance_20() const { return ___autoUVMaxDistance_20; }
	inline float* get_address_of_autoUVMaxDistance_20() { return &___autoUVMaxDistance_20; }
	inline void set_autoUVMaxDistance_20(float value)
	{
		___autoUVMaxDistance_20 = value;
	}

	inline static int32_t get_offset_of_autoUVMaxAngle_21() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___autoUVMaxAngle_21)); }
	inline float get_autoUVMaxAngle_21() const { return ___autoUVMaxAngle_21; }
	inline float* get_address_of_autoUVMaxAngle_21() { return &___autoUVMaxAngle_21; }
	inline void set_autoUVMaxAngle_21(float value)
	{
		___autoUVMaxAngle_21 = value;
	}

	inline static int32_t get_offset_of_minimumChartSize_22() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF, ___minimumChartSize_22)); }
	inline int32_t get_minimumChartSize_22() const { return ___minimumChartSize_22; }
	inline int32_t* get_address_of_minimumChartSize_22() { return &___minimumChartSize_22; }
	inline void set_minimumChartSize_22(int32_t value)
	{
		___minimumChartSize_22 = value;
	}
};

struct CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_StaticFields
{
public:
	// RealtimeCSG.Components.ModelSettingsFlags RealtimeCSG.Components.CSGModel::DefaultSettings
	int32_t ___DefaultSettings_5;

public:
	inline static int32_t get_offset_of_DefaultSettings_5() { return static_cast<int32_t>(offsetof(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_StaticFields, ___DefaultSettings_5)); }
	inline int32_t get_DefaultSettings_5() const { return ___DefaultSettings_5; }
	inline int32_t* get_address_of_DefaultSettings_5() { return &___DefaultSettings_5; }
	inline void set_DefaultSettings_5(int32_t value)
	{
		___DefaultSettings_5 = value;
	}
};


// RealtimeCSG.Components.CSGOperation
struct  CSGOperation_t0598053E4D2FD430C5A27CE5ECE53B67C283C8DA  : public CSGNode_t2C6EA7769DD4175D7DD26DDA9947C29E91CB349C
{
public:
	// System.Single RealtimeCSG.Components.CSGOperation::Version
	float ___Version_5;

public:
	inline static int32_t get_offset_of_Version_5() { return static_cast<int32_t>(offsetof(CSGOperation_t0598053E4D2FD430C5A27CE5ECE53B67C283C8DA, ___Version_5)); }
	inline float get_Version_5() const { return ___Version_5; }
	inline float* get_address_of_Version_5() { return &___Version_5; }
	inline void set_Version_5(float value)
	{
		___Version_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * m_Items[1];

public:
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Component[]
struct ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * m_Items[1];

public:
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  m_Items[1];

public:
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  m_Items[1];

public:
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  m_Items[1];

public:
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  m_Items[1];

public:
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// RealtimeCSG.Legacy.HalfEdge[]
struct HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42  m_Items[1];

public:
	inline HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42  value)
	{
		m_Items[index] = value;
	}
};
// RealtimeCSG.Legacy.Polygon[]
struct PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * m_Items[1];

public:
	inline Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// RealtimeCSG.Legacy.Surface[]
struct SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17  m_Items[1];

public:
	inline Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17  value)
	{
		m_Items[index] = value;
	}
};
// RealtimeCSG.Legacy.TexGen[]
struct TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45  m_Items[1];

public:
	inline TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___RenderMaterial_3), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___PhysicsMaterial_4), (void*)NULL);
		#endif
	}
	inline TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___RenderMaterial_3), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___PhysicsMaterial_4), (void*)NULL);
		#endif
	}
};
// RealtimeCSG.Legacy.TexGenFlags[]
struct TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * m_Items[1];

public:
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* GameObject_GetComponentsInChildren_TisRuntimeObject_m800C0157D5841A8D4C6A741E6C4F9FFFC0E0E33F_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___exists0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Boolean GameObjectExtensions::TryDestroy(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObjectExtensions_TryDestroy_m2336410EBE2F3B30E857C090F0392C4EB4AA0B8C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void GameObjectExtensions::SanitizeGameObject(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObjectExtensions_SanitizeGameObject_mE95D20EB8201288ADEC9F31C694DF9CAC6D751DE (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.MonoBehaviour>()
inline MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* GameObject_GetComponentsInChildren_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_m357A4E9F10F3DFA3823658320FD842261FD205C2 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m800C0157D5841A8D4C6A741E6C4F9FFFC0E0E33F_gshared)(__this, method);
}
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Component>()
inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* GameObject_GetComponentsInChildren_TisComponent_t05064EF382ABCAF4B8C94F8A350EA85184C26621_m054FAB8B5909A3250B61889166E5952C68E687B2 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m800C0157D5841A8D4C6A741E6C4F9FFFC0E0E33F_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void GameObjectExtensions::Destroy(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObjectExtensions_Destroy_mC3E6EF0B1DA3A96073259D8882A9CC5D23EF7375 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject0, const RuntimeMethod* method);
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Object_get_hideFlags_mCC5D0A1480AC0CDA190A63120B39C2C531428FC8 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::set_tag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_set_tag_mEF09E323917FAA2CECA527F821111A92031C1138 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB (const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_left_m74B52D8CFD8C62138067B2EB6846B6E9E51B7C20 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_back_mE7EF8625637E6F8B9E6B42A6AE140777C51E02F7 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_one_m6E01BE09CEA40781CB12CCB6AF33BBDA0F60CEED (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441 (const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_get_identity_mA0CECDE2A5E85CF014375084624F3770B5B7B79B (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0 (float ___angle0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___axis1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* Mesh_get_vertices_m7D07DC0F071C142B87F675B148FC0F7A243238B9 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___value0, const RuntimeMethod* method);
// UnityEngine.Color[] UnityEngine.Mesh::get_colors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* Mesh_get_colors_m16A1999334A7013DD5A1FF619E51983B4BFF7848 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_colors_m704D0EF58B7AED0D64AE4763EA375638FB08E026 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* Mesh_get_tangents_mFF92BD7D6EBA8C7EB8340E1529B1CB98006F44DD (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_tangents(UnityEngine.Vector4[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv_m0EBA5CA4644C9D5F1B2125AF3FE3873EFC8A4616 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv2_m3E70D5DD7A5C6910A074A78296269EBF2CBAE97F (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv3_mC56484D8B69A65DA948C7F23B06ED490BCFBE8B0 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv3(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv3_mDDBCCD5C65CC6F9378561C56E89DD7D0C3DF183D (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv4_m1C5734938A443D8004339E8D8DDDC33B3E0935F6 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv4(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv4_m2EBB956F121C204AE03250CE4F81D6926D129C3D (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv5()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv5_m92C1E27630FA8FA81175595BF25196E1DE823DAF (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv5(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv5_m8BC8CB2E658F2078EA3418C5B52C6F64542D7BF0 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv6()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv6_mD135BA25E0E2E889F06E1C4AD2F3999BE3F69E7A (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv6(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv6_m6C4D92C12B5DE58D9558A9B8EA25046F0EFF670A (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv7()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv7_m271653FCCCE9DBB20FBC7E585E5E02746C16537C (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv7(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv7_mA449FB27C59D2C878960E2AA11942BA527461D86 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv8_m02BBCED65154B17D321F92B12F9B90DA346C8EC0 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv8(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv8_m974B214CE08FBFCCBF9C21442F5407F17421B8FF (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// System.Int32[] UnityEngine.Mesh::get_triangles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* Mesh_get_triangles_mAAAFC770B8EE3F56992D5764EF8C2EC06EF743AC (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___value0, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  Mesh_get_bounds_m15A146B9397AA62A81986030D289320EDE4B3037 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_bounds(UnityEngine.Bounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_bounds_mB09338F622466456ADBCC449BB1F62F2EF1731B6 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___value0, const RuntimeMethod* method);
// System.Void RealtimeCSG.Components.CSGNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGNode__ctor_mE9B0ADCA67AFD7B22004171B6E48C7C386FB3EE0 (CSGNode_t2C6EA7769DD4175D7DD26DDA9947C29E91CB349C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAttribute__ctor_m7F5C473F39D5601486C1127DA0D52F2DC293FC35 (PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54 * __this, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m26DC3463C6F3C98BF33EA39598DD2B32F0249CA8 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetTriangles(System.Int32[],System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_SetTriangles_mA799D3ECD82713495DBEEEDD1DB14DA8CCDAF1C2 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___triangles0, int32_t ___submesh1, bool ___calculateBounds2, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Foundation.MeshQuery::op_Equality(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_op_Equality_m3A2B9D930633CA85ABB5CE3A89E3E6C176D0F7BE (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___left0, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___right1, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Foundation.GeneratedMeshDescription::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF (GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Int32 RealtimeCSG.Foundation.MeshQuery::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshQuery_GetHashCode_m5FBF184A4ED516B0A600B2B4C0DC2C1C4DDE1B10 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method);
// System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GeneratedMeshDescription_GetHashCode_mAEEFC5F7B260A59A966113874DC4AA3F77277C0B (GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * __this, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Foundation.MeshQuery::op_Inequality(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_op_Inequality_m754576CB4F0C360EF2DDB08983FCAB87513DB3FC (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___left0, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___right1, const RuntimeMethod* method);
// System.Void RealtimeCSG.Foundation.MeshQuery::.ctor(RealtimeCSG.Foundation.LayerUsageFlags,RealtimeCSG.Foundation.LayerUsageFlags,RealtimeCSG.Foundation.LayerParameterIndex,RealtimeCSG.Foundation.VertexChannelFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery__ctor_mAFFC39222E6FE5BD686A17A40FDF664DCFB6210D (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, int32_t ___query0, int32_t ___mask1, uint8_t ___parameterIndex2, uint8_t ___vertexChannels3, const RuntimeMethod* method);
// RealtimeCSG.Foundation.LayerUsageFlags RealtimeCSG.Foundation.MeshQuery::get_LayerQuery()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshQuery_get_LayerQuery_m4703C9835C13FCF59B4B409303FCB42E0532FA36 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerQuery(RealtimeCSG.Foundation.LayerUsageFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_LayerQuery_mC9946EC13A1FD87A223B830FF7A421B68D883374 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, int32_t ___value0, const RuntimeMethod* method);
// RealtimeCSG.Foundation.LayerUsageFlags RealtimeCSG.Foundation.MeshQuery::get_LayerQueryMask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshQuery_get_LayerQueryMask_mC503693548152B334FD4BE66859983D6D045C84D (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerQueryMask(RealtimeCSG.Foundation.LayerUsageFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_LayerQueryMask_mDE272289C3E300914F392DFC5F893CE18D83DD57 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, int32_t ___value0, const RuntimeMethod* method);
// RealtimeCSG.Foundation.LayerParameterIndex RealtimeCSG.Foundation.MeshQuery::get_LayerParameterIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t MeshQuery_get_LayerParameterIndex_mDCB4D744B10C68E3B7D28DE40ECFEE24CD264186 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerParameterIndex(RealtimeCSG.Foundation.LayerParameterIndex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_LayerParameterIndex_m8369D65DEFA1422B838AE8E56EAEEBFB5B733A6D (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, uint8_t ___value0, const RuntimeMethod* method);
// RealtimeCSG.Foundation.VertexChannelFlags RealtimeCSG.Foundation.MeshQuery::get_UsedVertexChannels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t MeshQuery_get_UsedVertexChannels_mA414AB75F94B51B487825C0DD43BA83759836E6A (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Foundation.MeshQuery::set_UsedVertexChannels(RealtimeCSG.Foundation.VertexChannelFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_UsedVertexChannels_mE603C1D0593AA6FB6012D060B4047A3F6E8BD3F0 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, uint8_t ___value0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.String RealtimeCSG.Foundation.MeshQuery::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(RealtimeCSG.Foundation.MeshQuery)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_Equals_m855368ED8A4BB10172F924F3050916596BBEFB74 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___other0, const RuntimeMethod* method);
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::set_normal(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane_set_normal_m946BA6635B964F0675FBC69F5EFC9BD95706D8BD (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::get_pointOnPlane()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84 (Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Plane::get_distance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Plane_get_distance_m5358B80C35E1E295C0133E7DC6449BB09C456DEE (Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Plane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m93D9A7BCD9C724AF1005E13B7DF4FCA19D50963F (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___plane0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m31DC6D44D2B5A5C6C686E45A5523EF7E4ECA77F2 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___inNormal0, float ___inD1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lhs0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rhs1, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___inNormal0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pointOnPlane1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point1, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___inRotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pointOnPlane1, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::Normalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, float ___inA0, float ___inB1, float ___inC2, float ___inD3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_Cross_m3E9DBC445228FDB850BDBB4B01D6F61AC0111887 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lhs0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rhs1, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point21, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point32, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::RayIntersection(UnityEngine.Ray)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_RayIntersection_m617BEB5AFB352E3526C5D3980CEEDA793F89DD0B (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Single RealtimeCSG.Legacy.CSGPlane::Distance(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, const RuntimeMethod* method);
// System.Boolean System.Single::IsInfinity(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B (float ___f0, const RuntimeMethod* method);
// System.Boolean System.Single::IsNaN(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB (float ___f0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839 (float ___d0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a1, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Legacy.CSGPlane::TryRayIntersection(UnityEngine.Ray,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___intersection1, const RuntimeMethod* method);
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::LineIntersection(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, const RuntimeMethod* method);
// System.Decimal System.Decimal::op_Explicit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39 (float ___value0, const RuntimeMethod* method);
// System.Decimal System.Decimal::op_Multiply(System.Decimal,System.Decimal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08 (Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d10, Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d21, const RuntimeMethod* method);
// System.Decimal System.Decimal::op_Subtraction(System.Decimal,System.Decimal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  Decimal_op_Subtraction_mA8822FE4BA50620B0A58B46C8B46A08361C7FF4E (Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d10, Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d21, const RuntimeMethod* method);
// System.Decimal System.Decimal::op_Addition(System.Decimal,System.Decimal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981 (Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d10, Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d21, const RuntimeMethod* method);
// System.Decimal System.Decimal::op_UnaryNegation(System.Decimal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  Decimal_op_UnaryNegation_mFC91CB75C3905B5CD2847152B45B28E85F4D4884 (Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d0, const RuntimeMethod* method);
// System.Decimal System.Decimal::op_Division(System.Decimal,System.Decimal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  Decimal_op_Division_m5A14CCDBC929DEB14F9AC195C7456DF0AE76F514 (Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d10, Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d21, const RuntimeMethod* method);
// System.Single System.Decimal::op_Explicit(System.Decimal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Decimal_op_Explicit_mC7ED730AE7C6D42F19F06246B242E8B60EDDAC62 (Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___value0, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_get_inverse_mBD3145C0D7977962E18C8B3BF63DD671F7917166 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_transpose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_get_transpose_mD7983388309D5C806DF8E79A76FF0209964C0D02 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  Matrix4x4_op_Multiply_m33683566CAD5B20F7D6D3CCF26166EC93FB39893 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___lhs0, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___vector1, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.CSGPlane::Transform(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transformation0, const RuntimeMethod* method);
// RealtimeCSG.Legacy.CSGPlane RealtimeCSG.Legacy.CSGPlane::Negated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  CSGPlane_Negated_m197727B4586DC28A9A753E82FFE636D05DB5F9E5 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method);
// RealtimeCSG.Legacy.CSGPlane RealtimeCSG.Legacy.CSGPlane::Translated(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  CSGPlane_Translated_mAB1A8898F2402DAF9F25A44F2A503566C240443D (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___translation0, const RuntimeMethod* method);
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::Project(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_Project_m579D50E710FBC39A748ABEE2AAFCC2C6E9241B94 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, const RuntimeMethod* method);
// System.Int32 System.Single::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0 (float* __this, const RuntimeMethod* method);
// System.Int32 RealtimeCSG.Legacy.CSGPlane::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CSGPlane_GetHashCode_mA53E0BF2F7FDAA715588FBA59C4C49E6DFB1E33B (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Legacy.CSGPlane::Equals(RealtimeCSG.Legacy.CSGPlane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___other0, const RuntimeMethod* method);
// System.Boolean RealtimeCSG.Legacy.CSGPlane::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72 (const RuntimeMethod* method);
// System.String System.String::Format(System.IFormatProvider,System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mF68EE0DEC1AA5ADE9DFEF9AE0508E428FBB10EFD (RuntimeObject* ___provider0, String_t* ___format1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args2, const RuntimeMethod* method);
// System.String RealtimeCSG.Legacy.CSGPlane::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.ControlMesh::CopyFrom(RealtimeCSG.Legacy.ControlMesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh_CopyFrom_m010324448CDA71EAC8776694628A7307CE186C42 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * ___other0, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.ControlMesh::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh_Reset_m70A47B47DED0106F851EF950DDCA2FAF497800F1 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434 (RuntimeArray * ___sourceArray0, RuntimeArray * ___destinationArray1, int32_t ___length2, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.Polygon::.ctor(System.Int32[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Polygon__ctor_m2E15F7F95331FC3DB64EE581891D0F1746349511 (Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___edges0, int32_t ___texGenIndex1, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.ControlMesh::.ctor(RealtimeCSG.Legacy.ControlMesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh__ctor_m12DF3940EE5E54F6E41903867A37567A0D4743B4 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * ___other0, const RuntimeMethod* method);
// System.Int32 RealtimeCSG.Legacy.ControlMesh::GetNextEdgeIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ControlMesh_GetNextEdgeIndex_m0E2F6BC7E88481CBDE3C3B166BBA15BB94D744D6 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method);
// System.Int32 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ControlMesh_GetTwinEdgeIndex_mEB16E205B788C2614EFB4CD2EA3A1F62C224B7F3 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.HalfEdge::.ctor(System.Int16,System.Int32,System.Int16,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalfEdge__ctor_m3A641C7DC2F71E2E1BFC0B960494AA9258BA8F1A (HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * __this, int16_t ___polygonIndex0, int32_t ___twinIndex1, int16_t ___vertexIndex2, bool ___hardEdge3, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 RealtimeCSG.Legacy.Surface::GenerateLocalBrushSpaceToPlaneSpaceMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C (Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865 (String_t* ___format0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.String RealtimeCSG.Legacy.Surface::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB (Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v0, const RuntimeMethod* method);
// System.Void RealtimeCSG.Legacy.TexGen::.ctor(UnityEngine.Material,UnityEngine.PhysicMaterial)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE (TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___renderMaterial0, PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___physicsMaterial1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_op_Multiply_mF6693A950E1917204E356366892C3CCB0553436E (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___lhs0, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 RealtimeCSG.Legacy.TexGen::GeneratePlaneSpaceToTextureSpaceMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8 (TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameObjectExtensions::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObjectExtensions_Destroy_m7F688FC353A6D032E74735F218D717755882AD4F (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObjectExtensions_Destroy_m7F688FC353A6D032E74735F218D717755882AD4F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!obj)
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// UnityEngine.Object.DestroyImmediate(obj);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameObjectExtensions::Destroy(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObjectExtensions_Destroy_mC3E6EF0B1DA3A96073259D8882A9CC5D23EF7375 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObjectExtensions_Destroy_mC3E6EF0B1DA3A96073259D8882A9CC5D23EF7375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!gameObject)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___gameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// if (!TryDestroy(gameObject))
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = ___gameObject0;
		bool L_3 = GameObjectExtensions_TryDestroy_m2336410EBE2F3B30E857C090F0392C4EB4AA0B8C(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_004b;
		}
	}
	{
		// if (gameObject.activeSelf &&
		//     gameObject.name != kUnableToDelete)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ___gameObject0;
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = ___gameObject0;
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_6, /*hidden argument*/NULL);
		bool L_8 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_7, _stringLiteral42F4000E95E51C94EC813EB1D8C5A408E16D34D9, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		// gameObject.hideFlags = HideFlags.DontSaveInBuild;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = ___gameObject0;
		NullCheck(L_9);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_9, ((int32_t)16), /*hidden argument*/NULL);
		// gameObject.SetActive(false);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = ___gameObject0;
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)0, /*hidden argument*/NULL);
		// SanitizeGameObject(gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = ___gameObject0;
		GameObjectExtensions_SanitizeGameObject_mE95D20EB8201288ADEC9F31C694DF9CAC6D751DE(L_11, /*hidden argument*/NULL);
		// gameObject.name = kUnableToDelete;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = ___gameObject0;
		NullCheck(L_12);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_12, _stringLiteral42F4000E95E51C94EC813EB1D8C5A408E16D34D9, /*hidden argument*/NULL);
	}

IL_004b:
	{
		// }
		return;
	}
}
// System.Void GameObjectExtensions::SanitizeGameObject(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObjectExtensions_SanitizeGameObject_mE95D20EB8201288ADEC9F31C694DF9CAC6D751DE (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObjectExtensions_SanitizeGameObject_mE95D20EB8201288ADEC9F31C694DF9CAC6D751DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* V_0 = NULL;
	int32_t V_1 = 0;
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * V_2 = NULL;
	ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* V_3 = NULL;
	Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	RuntimeObject* V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 5);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var childComponents = gameObject.GetComponentsInChildren<MonoBehaviour>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___gameObject0;
		NullCheck(L_0);
		MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* L_1 = GameObject_GetComponentsInChildren_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_m357A4E9F10F3DFA3823658320FD842261FD205C2(L_0, /*hidden argument*/GameObject_GetComponentsInChildren_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_m357A4E9F10F3DFA3823658320FD842261FD205C2_RuntimeMethod_var);
		// foreach (var component in childComponents)
		V_0 = L_1;
		V_1 = 0;
		goto IL_001e;
	}

IL_000b:
	{
		// foreach (var component in childComponents)
		MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_6, /*hidden argument*/NULL);
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		goto IL_001a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Object)
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		goto IL_001a;
	} // end catch (depth: 1)

IL_001a:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_001e:
	{
		// foreach (var component in childComponents)
		int32_t L_8 = V_1;
		MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		// var childComponents = gameObject.GetComponentsInChildren<Component>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = ___gameObject0;
		NullCheck(L_10);
		ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* L_11 = GameObject_GetComponentsInChildren_TisComponent_t05064EF382ABCAF4B8C94F8A350EA85184C26621_m054FAB8B5909A3250B61889166E5952C68E687B2(L_10, /*hidden argument*/GameObject_GetComponentsInChildren_TisComponent_t05064EF382ABCAF4B8C94F8A350EA85184C26621_m054FAB8B5909A3250B61889166E5952C68E687B2_RuntimeMethod_var);
		// foreach (var component in childComponents)
		V_3 = L_11;
		V_1 = 0;
		goto IL_004d;
	}

IL_002f:
	{
		// foreach (var component in childComponents)
		ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* L_12 = V_3;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = L_15;
		// if (!(component is Transform))
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_16 = V_4;
		if (((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)IsInstClass((RuntimeObject*)L_16, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var)))
		{
			goto IL_0049;
		}
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_17 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_17, /*hidden argument*/NULL);
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		goto IL_0049;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0046;
		throw e;
	}

CATCH_0046:
	{ // begin catch(System.Object)
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		// try { UnityEngine.Object.DestroyImmediate(component); } catch { }
		goto IL_0049;
	} // end catch (depth: 1)

IL_0049:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_004d:
	{
		// foreach (var component in childComponents)
		int32_t L_19 = V_1;
		ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* L_20 = V_3;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))))
		{
			goto IL_002f;
		}
	}
	{
		// var transform = gameObject.transform;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = ___gameObject0;
		NullCheck(L_21);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_21, /*hidden argument*/NULL);
		// foreach (Transform childTransform in transform)
		NullCheck(L_22);
		RuntimeObject* L_23 = Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC(L_22, /*hidden argument*/NULL);
		V_5 = L_23;
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0078;
		}

IL_0062:
		{
			// foreach (Transform childTransform in transform)
			RuntimeObject* L_24 = V_5;
			NullCheck(L_24);
			RuntimeObject * L_25 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_24);
			// Destroy(childTransform.gameObject);
			NullCheck(((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_25, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var)));
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_25, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			GameObjectExtensions_Destroy_mC3E6EF0B1DA3A96073259D8882A9CC5D23EF7375(L_26, /*hidden argument*/NULL);
		}

IL_0078:
		{
			// foreach (Transform childTransform in transform)
			RuntimeObject* L_27 = V_5;
			NullCheck(L_27);
			bool L_28 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_27);
			if (L_28)
			{
				goto IL_0062;
			}
		}

IL_0081:
		{
			IL2CPP_LEAVE(0x98, FINALLY_0083);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_29 = V_5;
			V_6 = ((RuntimeObject*)IsInst((RuntimeObject*)L_29, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_30 = V_6;
			if (!L_30)
			{
				goto IL_0097;
			}
		}

IL_0090:
		{
			RuntimeObject* L_31 = V_6;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_31);
		}

IL_0097:
		{
			IL2CPP_END_FINALLY(131)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x98, IL_0098)
	}

IL_0098:
	{
		// }
		return;
	}
}
// System.Boolean GameObjectExtensions::TryDestroy(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObjectExtensions_TryDestroy_m2336410EBE2F3B30E857C090F0392C4EB4AA0B8C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObjectExtensions_TryDestroy_m2336410EBE2F3B30E857C090F0392C4EB4AA0B8C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (!gameObject)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___gameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_000a:
	{
		// var prevHideFlags = gameObject.hideFlags;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = ___gameObject0;
		NullCheck(L_2);
		int32_t L_3 = Object_get_hideFlags_mCC5D0A1480AC0CDA190A63120B39C2C531428FC8(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// gameObject.hideFlags = HideFlags.None;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ___gameObject0;
		NullCheck(L_4);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_4, 0, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		// UnityEngine.Object.DestroyImmediate(gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = ___gameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_5, /*hidden argument*/NULL);
		// return true;
		V_1 = (bool)1;
		goto IL_002e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Object)
		// catch
		// gameObject.hideFlags = prevHideFlags;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = ___gameObject0;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_6, L_7, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_002e;
	} // end catch (depth: 1)

IL_002e:
	{
		// }
		bool L_8 = V_1;
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InternalRealtimeCSG.CSGModelExported::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGModelExported_Awake_mF191E38CA0ED42C2B9B86682740E4E8C3957741E (CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90 * __this, const RuntimeMethod* method)
{
	{
		// this.hideFlags |= HideFlags.DontSaveInBuild;
		int32_t L_0 = Object_get_hideFlags_mCC5D0A1480AC0CDA190A63120B39C2C531428FC8(__this, /*hidden argument*/NULL);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(__this, ((int32_t)((int32_t)L_0|(int32_t)((int32_t)16))), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InternalRealtimeCSG.CSGModelExported::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGModelExported__ctor_mEB8026CFB8477A739C7E196C95BA0D6D037E35AD (CSGModelExported_tA586B22CD213DD0104FEDE87D767C673E6C41C90 * __this, const RuntimeMethod* method)
{
	{
		// [HideInInspector] public float Version = 1.00f;
		__this->set_Version_4((1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InternalRealtimeCSG.GeneratedMeshInstance::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedMeshInstance_Awake_m0DAF3822B276AB9315F3A3CA3FD859515BE308F2 (GeneratedMeshInstance_tBD769E0C8E8CA45484343BF79C4021AB71238522 * __this, const RuntimeMethod* method)
{
	{
		// this.hideFlags = HideFlags.DontSaveInBuild;
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(__this, ((int32_t)16), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InternalRealtimeCSG.GeneratedMeshInstance::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedMeshInstance__ctor_m5074F8C7790B1F24FD9C6DF507A7728524C6EA55 (GeneratedMeshInstance_tBD769E0C8E8CA45484343BF79C4021AB71238522 * __this, const RuntimeMethod* method)
{
	{
		// [HideInInspector] public float Version = 1.00f;
		__this->set_Version_4((1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InternalRealtimeCSG.GeneratedMeshes::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedMeshes_Awake_m09A83476C251049B04AA716B49E2015A5B39FE75 (GeneratedMeshes_t5C134866E7C8EA7D47DD7147F7562979AAE48AD5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedMeshes_Awake_m09A83476C251049B04AA716B49E2015A5B39FE75_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.gameObject.tag = "EditorOnly";
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_set_tag_mEF09E323917FAA2CECA527F821111A92031C1138(L_0, _stringLiteral7F17C1A4474B196924E9DBB8952AFE2D51F71C90, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InternalRealtimeCSG.GeneratedMeshes::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedMeshes__ctor_mEA03FAA18011B37C2BCC4C7DC37B61887591332F (GeneratedMeshes_t5C134866E7C8EA7D47DD7147F7562979AAE48AD5 * __this, const RuntimeMethod* method)
{
	{
		// [HideInInspector] public float Version = 1.00f;
		__this->set_Version_4((1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InternalRealtimeCSG.HiddenComponentData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HiddenComponentData__ctor_m01542360BAD54D4EEFF8A4139826D6EB7C20B977 (HiddenComponentData_t21DAF9F1CB4ECB2271EB217863E5A5275364D4E6 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InternalRealtimeCSG.LegacyGeneratedMeshContainer::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LegacyGeneratedMeshContainer_Awake_m856864A9B92959D607A19300A513DA9D99B24D3F (LegacyGeneratedMeshContainer_t4335E9BE434755096FC5770B2503EEE9FFB530AF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LegacyGeneratedMeshContainer_Awake_m856864A9B92959D607A19300A513DA9D99B24D3F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityEngine.Object.DestroyImmediate(this.gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InternalRealtimeCSG.LegacyGeneratedMeshContainer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LegacyGeneratedMeshContainer__ctor_m3F0DC1CC4ACEF1770812F4CD2EE88E440A1D94ED (LegacyGeneratedMeshContainer_t4335E9BE434755096FC5770B2503EEE9FFB530AF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InternalRealtimeCSG.MathConstants::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MathConstants__cctor_m11C0D67BFD9B99DDDA20168C4981644FFA5BBC8F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MathConstants__cctor_m11C0D67BFD9B99DDDA20168C4981644FFA5BBC8F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly Vector3 zeroVector3                = Vector3.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_zeroVector3_16(L_0);
		// public static readonly Vector3 oneVector3                = Vector3.one;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_oneVector3_17(L_1);
		// public static readonly Vector3 unitXVector3                = new Vector3(1,0,0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_2), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_unitXVector3_18(L_2);
		// public static readonly Vector3 unitYVector3                = new Vector3(0,1,0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_3), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_unitYVector3_19(L_3);
		// public static readonly Vector3 unitZVector3                = new Vector3(0,0,1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_4), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_unitZVector3_20(L_4);
		// public static readonly Vector3 upVector3                = Vector3.up;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_upVector3_21(L_5);
		// public static readonly Vector3 forwardVector3            = Vector3.forward;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_forwardVector3_22(L_6);
		// public static readonly Vector3 rightVector3                = Vector3.right;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_rightVector3_23(L_7);
		// public static readonly Vector3 leftVector3                = Vector3.left;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_get_left_m74B52D8CFD8C62138067B2EB6846B6E9E51B7C20(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_leftVector3_24(L_8);
		// public static readonly Vector3 downVector3                = Vector3.down;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_downVector3_25(L_9);
		// public static readonly Vector3 backVector3                = Vector3.back;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector3_get_back_mE7EF8625637E6F8B9E6B42A6AE140777C51E02F7(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_backVector3_26(L_10);
		// public static readonly Vector3 NaNVector3                = new Vector3(float.NaN, float.NaN, float.NaN);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_11), (std::numeric_limits<float>::quiet_NaN()), (std::numeric_limits<float>::quiet_NaN()), (std::numeric_limits<float>::quiet_NaN()), /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_NaNVector3_27(L_11);
		// public static readonly Vector3 PositiveInfinityVector3    = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_12), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_PositiveInfinityVector3_28(L_12);
		// public static readonly Vector3 NegativeInfinityVector3    = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_NegativeInfinityVector3_29(L_13);
		// public static readonly Vector2 zeroVector2        = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_zeroVector2_30(L_14);
		// public static readonly Vector2 oneVector2        = Vector2.one;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_15 = Vector2_get_one_m6E01BE09CEA40781CB12CCB6AF33BBDA0F60CEED(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_oneVector2_31(L_15);
		// public static readonly Vector2 upVector2        = Vector2.up;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_upVector2_32(L_16);
		// public static readonly Ray emptyRay                = new Ray();
		il2cpp_codegen_initobj((((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_address_of_emptyRay_33()), sizeof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 ));
		// public static readonly Matrix4x4 identityMatrix            = Matrix4x4.identity;
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_17 = Matrix4x4_get_identity_mA0CECDE2A5E85CF014375084624F3770B5B7B79B(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_identityMatrix_34(L_17);
		// public static readonly Quaternion identityQuaternion    = Quaternion.identity;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_18 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_identityQuaternion_35(L_18);
		// public static readonly Quaternion YQuaternion            = Quaternion.AngleAxis(90, Vector3.left);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Vector3_get_left_m74B52D8CFD8C62138067B2EB6846B6E9E51B7C20(/*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_20 = Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0((90.0f), L_19, /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_YQuaternion_36(L_20);
		// public static readonly Quaternion ZQuaternion            = Quaternion.AngleAxis(90, Vector3.forward);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_22 = Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0((90.0f), L_21, /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_ZQuaternion_37(L_22);
		// public static readonly Quaternion XQuaternion            = Quaternion.AngleAxis(90, Vector3.up);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_24 = Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0((90.0f), L_23, /*hidden argument*/NULL);
		((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->set_XQuaternion_38(L_24);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Mesh RealtimeCSG.CSGMeshUtility::Clone(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * CSGMeshUtility_Clone_m4E3FDDC630BAFC1A52B2E34E9ED98FC179BA81D8 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGMeshUtility_Clone_m4E3FDDC630BAFC1A52B2E34E9ED98FC179BA81D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		//             return new Mesh
		//             {
		//                 vertices    = mesh.vertices,
		//                 colors      = mesh.colors,
		//                 name        = mesh.name,
		//                 normals     = mesh.normals,
		//                 tangents    = mesh.tangents,
		//                 uv          = mesh.uv,
		//                 uv2         = mesh.uv2,
		//                 uv3         = mesh.uv3,
		//                 uv4         = mesh.uv4,
		// #if UNITY_2018_2_OR_NEWER
		//                 uv5         = mesh.uv5,
		//                 uv6         = mesh.uv6,
		//                 uv7         = mesh.uv7,
		//                 uv8         = mesh.uv8,
		// #endif
		//                 triangles   = mesh.triangles,
		//                 bounds      = mesh.bounds
		//             };
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *)il2cpp_codegen_object_new(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var);
		Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4(L_0, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = L_0;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = ___mesh0;
		NullCheck(L_2);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_3 = Mesh_get_vertices_m7D07DC0F071C142B87F675B148FC0F7A243238B9(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6(L_1, L_3, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_4 = L_1;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_5 = ___mesh0;
		NullCheck(L_5);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_6 = Mesh_get_colors_m16A1999334A7013DD5A1FF619E51983B4BFF7848(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Mesh_set_colors_m704D0EF58B7AED0D64AE4763EA375638FB08E026(L_4, L_6, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_7 = L_4;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_8 = ___mesh0;
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_7, L_9, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_10 = L_7;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_11 = ___mesh0;
		NullCheck(L_11);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_12 = Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589(L_10, L_12, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_13 = L_10;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_14 = ___mesh0;
		NullCheck(L_14);
		Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* L_15 = Mesh_get_tangents_mFF92BD7D6EBA8C7EB8340E1529B1CB98006F44DD(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57(L_13, L_15, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_16 = L_13;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_17 = ___mesh0;
		NullCheck(L_17);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_18 = Mesh_get_uv_m0EBA5CA4644C9D5F1B2125AF3FE3873EFC8A4616(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8(L_16, L_18, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_19 = L_16;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_20 = ___mesh0;
		NullCheck(L_20);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_21 = Mesh_get_uv2_m3E70D5DD7A5C6910A074A78296269EBF2CBAE97F(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038(L_19, L_21, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_22 = L_19;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_23 = ___mesh0;
		NullCheck(L_23);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_24 = Mesh_get_uv3_mC56484D8B69A65DA948C7F23B06ED490BCFBE8B0(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Mesh_set_uv3_mDDBCCD5C65CC6F9378561C56E89DD7D0C3DF183D(L_22, L_24, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_25 = L_22;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_26 = ___mesh0;
		NullCheck(L_26);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_27 = Mesh_get_uv4_m1C5734938A443D8004339E8D8DDDC33B3E0935F6(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Mesh_set_uv4_m2EBB956F121C204AE03250CE4F81D6926D129C3D(L_25, L_27, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_28 = L_25;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_29 = ___mesh0;
		NullCheck(L_29);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_30 = Mesh_get_uv5_m92C1E27630FA8FA81175595BF25196E1DE823DAF(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Mesh_set_uv5_m8BC8CB2E658F2078EA3418C5B52C6F64542D7BF0(L_28, L_30, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_31 = L_28;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_32 = ___mesh0;
		NullCheck(L_32);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_33 = Mesh_get_uv6_mD135BA25E0E2E889F06E1C4AD2F3999BE3F69E7A(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Mesh_set_uv6_m6C4D92C12B5DE58D9558A9B8EA25046F0EFF670A(L_31, L_33, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_34 = L_31;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_35 = ___mesh0;
		NullCheck(L_35);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_36 = Mesh_get_uv7_m271653FCCCE9DBB20FBC7E585E5E02746C16537C(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Mesh_set_uv7_mA449FB27C59D2C878960E2AA11942BA527461D86(L_34, L_36, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_37 = L_34;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_38 = ___mesh0;
		NullCheck(L_38);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_39 = Mesh_get_uv8_m02BBCED65154B17D321F92B12F9B90DA346C8EC0(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Mesh_set_uv8_m974B214CE08FBFCCBF9C21442F5407F17421B8FF(L_37, L_39, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_40 = L_37;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_41 = ___mesh0;
		NullCheck(L_41);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_42 = Mesh_get_triangles_mAAAFC770B8EE3F56992D5764EF8C2EC06EF743AC(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED(L_40, L_42, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_43 = L_40;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_44 = ___mesh0;
		NullCheck(L_44);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_45 = Mesh_get_bounds_m15A146B9397AA62A81986030D289320EDE4B3037(L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		Mesh_set_bounds_mB09338F622466456ADBCC449BB1F62F2EF1731B6(L_43, L_45, /*hidden argument*/NULL);
		return L_43;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.Components.CSGBrush::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGBrush__ctor_m954F3029A94264D30A3A35AE1343EF327BC20364 (CSGBrush_t5B9697019AE2C344733C0D257178EB7AB85C69DF * __this, const RuntimeMethod* method)
{
	{
		// [HideInInspector] public float Version = CurrentVersion;
		__this->set_Version_5((2.1f));
		CSGNode__ctor_mE9B0ADCA67AFD7B22004171B6E48C7C386FB3EE0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RealtimeCSG.Components.CSGModel::get_IsRenderable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_IsRenderable_m7523B8AB0B0889D549728402BB305A85203FC394 (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    IsRenderable            { get { return (Settings & ModelSettingsFlags.DoNotRender) == (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_IsTwoSidedShadows()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_IsTwoSidedShadows_m51838E2565C55977464F948440710B12DC2FFF0C (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    IsTwoSidedShadows        { get { return (Settings & ModelSettingsFlags.TwoSidedShadows) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16384)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_HaveCollider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_HaveCollider_m354F9EAB9A3AA0F5EEC799199817E094DED64A6E (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    HaveCollider            { get { return (Settings & ModelSettingsFlags.NoCollider) == (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_IsTrigger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_IsTrigger_mC4B35628103C632A811008B15BA795DC4DF92AAE (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    IsTrigger                { get { return (Settings & ModelSettingsFlags.IsTrigger) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_InvertedWorld()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_InvertedWorld_m6C5FA2F7E53BB1F4A8DD0D54BDD19BC19DA5DF5B (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    InvertedWorld            { get { return (Settings & ModelSettingsFlags.InvertedWorld) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)128)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_SetColliderConvex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_SetColliderConvex_mD9B220CD338F81D758CD3D4F539AD46BCD0DA848 (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    SetColliderConvex        { get { return (Settings & ModelSettingsFlags.SetColliderConvex) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)256)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_NeedAutoUpdateRigidBody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_NeedAutoUpdateRigidBody_m2F9D1D4B737B6837B8C84F8D255B51F5FFE6AE35 (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    NeedAutoUpdateRigidBody    { get { return (Settings & ModelSettingsFlags.AutoUpdateRigidBody) == (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)512)))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_PreserveUVs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_PreserveUVs_mF08E71E035F262083F67FE8CCDACC429E23428D9 (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    PreserveUVs             { get { return (Settings & ModelSettingsFlags.PreserveUVs) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)1024)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_StitchLightmapSeams()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_StitchLightmapSeams_mEE2D674002DB25512A1925E08AEDB5C1652804D1 (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool StitchLightmapSeams        { get { return (Settings & ModelSettingsFlags.StitchLightmapSeams) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)4096)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_AutoRebuildUVs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_AutoRebuildUVs_m1307BCFF754F6A001AD138571A0B0704F6AA87AC (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    AutoRebuildUVs             { get { return (Settings & ModelSettingsFlags.AutoRebuildUVs) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)2048)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean RealtimeCSG.Components.CSGModel::get_IgnoreNormals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGModel_get_IgnoreNormals_m557DC787F6EB5E2376CD7DF8CDDE6BBAD0EFEA34 (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	{
		// public bool    IgnoreNormals              { get { return (Settings & ModelSettingsFlags.IgnoreNormals) != (ModelSettingsFlags)0; } }
		int32_t L_0 = __this->get_Settings_7();
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)8192)))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Void RealtimeCSG.Components.CSGModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGModel__ctor_m156A0623EA2BF1DF350B2D71E5F745CA8854FB2D (CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGModel__ctor_m156A0623EA2BF1DF350B2D71E5F745CA8854FB2D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [HideInInspector] public float Version = CurrentVersion;
		__this->set_Version_6((1.1f));
		// [EnumAsFlags] public ModelSettingsFlags                Settings        = DefaultSettings;
		IL2CPP_RUNTIME_CLASS_INIT(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_StaticFields*)il2cpp_codegen_static_fields_for(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_il2cpp_TypeInfo_var))->get_DefaultSettings_5();
		__this->set_Settings_7(L_0);
		// [EnumAsFlags] public Foundation.VertexChannelFlags    VertexChannels    = Foundation.VertexChannelFlags.All;
		__this->set_VertexChannels_8(((int32_t)14));
		// [EnumAsFlags] public ReceiveGI                      ReceiveGI       = ReceiveGI.LightProbes;
		__this->set_ReceiveGI_9(2);
		// [EnumAsFlags] public MeshColliderCookingOptions MeshColliderCookingOptions = MeshColliderCookingOptions.CookForFasterSimulation |
		//                                                                              MeshColliderCookingOptions.EnableMeshCleaning |
		//                                                                              MeshColliderCookingOptions.WeldColocatedVertices;
		__this->set_MeshColliderCookingOptions_10(((int32_t)14));
		// public float            angleError                = 1.00f;//0.08f;
		__this->set_angleError_15((1.0f));
		// public float            areaError                = 1.00f;//0.15f;
		__this->set_areaError_16((1.0f));
		// public float            hardAngle                = 60.0f;//88.0f;
		__this->set_hardAngle_17((60.0f));
		// public float            packMargin                = 20.0f;//4.0f / 1024.0f;
		__this->set_packMargin_18((20.0f));
		// public float            scaleInLightmap            = 1.0f;
		__this->set_scaleInLightmap_19((1.0f));
		// public float            autoUVMaxDistance        = 0.5f;
		__this->set_autoUVMaxDistance_20((0.5f));
		// public float            autoUVMaxAngle            = 89.0f;
		__this->set_autoUVMaxAngle_21((89.0f));
		// public int                minimumChartSize        = 4;
		__this->set_minimumChartSize_22(4);
		CSGNode__ctor_mE9B0ADCA67AFD7B22004171B6E48C7C386FB3EE0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RealtimeCSG.Components.CSGModel::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGModel__cctor_m68D842D99DC57996CD00259E16E32A676EF40B3B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGModel__cctor_m68D842D99DC57996CD00259E16E32A676EF40B3B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ModelSettingsFlags DefaultSettings = ((ModelSettingsFlags)UnityEngine.Rendering.ShadowCastingMode.On) | ModelSettingsFlags.PreserveUVs | ModelSettingsFlags.AutoUpdateRigidBody;
		((CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_StaticFields*)il2cpp_codegen_static_fields_for(CSGModel_t25CD73569B9BE7800C04635DD93DF96239E953EF_il2cpp_TypeInfo_var))->set_DefaultSettings_5(((int32_t)1537));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.Components.CSGNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGNode__ctor_mE9B0ADCA67AFD7B22004171B6E48C7C386FB3EE0 (CSGNode_t2C6EA7769DD4175D7DD26DDA9947C29E91CB349C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.Components.CSGOperation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGOperation__ctor_mD3CA12D90C37AE6BE3C6C8D1FE9402E80FFAB050 (CSGOperation_t0598053E4D2FD430C5A27CE5ECE53B67C283C8DA * __this, const RuntimeMethod* method)
{
	{
		// [HideInInspector] public float Version = CurrentVersion;
		__this->set_Version_5((1.1f));
		CSGNode__ctor_mE9B0ADCA67AFD7B22004171B6E48C7C386FB3EE0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.EnumAsFlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumAsFlagsAttribute__ctor_m115FEF82DC42DF1EB3A67C1E3297A75E7BB72A36 (EnumAsFlagsAttribute_t6143875D6DD196ECAB16C0C373116DF3C8D5B9E7 * __this, const RuntimeMethod* method)
{
	{
		// public EnumAsFlagsAttribute() { }
		PropertyAttribute__ctor_m7F5C473F39D5601486C1127DA0D52F2DC293FC35(__this, /*hidden argument*/NULL);
		// public EnumAsFlagsAttribute() { }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.Foundation.GeneratedMeshContents::CopyTo(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088 (GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C * __this, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (object.ReferenceEquals(mesh, null))
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = ___mesh0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException("mesh");
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteral52DA7846E396FF38CD6729BEC21B6F9159F9BD1C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088_RuntimeMethod_var);
	}

IL_000e:
	{
		// if (!mesh)
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = ___mesh0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		// throw new ArgumentException("mesh", "mesh is not valid, it might've already be destroyed");
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_4 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m26DC3463C6F3C98BF33EA39598DD2B32F0249CA8(L_4, _stringLiteral52DA7846E396FF38CD6729BEC21B6F9159F9BD1C, _stringLiteralB3063AE7AB0A9A3C4F8ED0DF283175F7F34A8CEE, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088_RuntimeMethod_var);
	}

IL_0026:
	{
		// if (description.vertexCount < 3 ||
		//     description.indexCount < 3)
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * L_5 = __this->get_address_of_description_0();
		int32_t L_6 = L_5->get_vertexCount_6();
		if ((((int32_t)L_6) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * L_7 = __this->get_address_of_description_0();
		int32_t L_8 = L_7->get_indexCount_7();
		if ((((int32_t)L_8) >= ((int32_t)3)))
		{
			goto IL_0049;
		}
	}

IL_0042:
	{
		// mesh.Clear();
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_9 = ___mesh0;
		NullCheck(L_9);
		Mesh_Clear_mB750E1DCAB658124AAD81A02B93DED7601047B60(L_9, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0049:
	{
		// mesh.vertices = positions;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_10 = ___mesh0;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_11 = __this->get_positions_2();
		NullCheck(L_10);
		Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6(L_10, L_11, /*hidden argument*/NULL);
		// if (normals  != null) mesh.normals    = normals;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_12 = __this->get_normals_4();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		// if (normals  != null) mesh.normals    = normals;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_13 = ___mesh0;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_14 = __this->get_normals_4();
		NullCheck(L_13);
		Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		// if (tangents != null) mesh.tangents    = tangents;
		Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* L_15 = __this->get_tangents_3();
		if (!L_15)
		{
			goto IL_007d;
		}
	}
	{
		// if (tangents != null) mesh.tangents    = tangents;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_16 = ___mesh0;
		Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* L_17 = __this->get_tangents_3();
		NullCheck(L_16);
		Mesh_set_tangents_mE66D8020B76E43A5CA3C4E60DB61CD962D7D3C57(L_16, L_17, /*hidden argument*/NULL);
	}

IL_007d:
	{
		// if (uv0      != null) mesh.uv        = uv0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_18 = __this->get_uv0_5();
		if (!L_18)
		{
			goto IL_0091;
		}
	}
	{
		// if (uv0      != null) mesh.uv        = uv0;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_19 = ___mesh0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_20 = __this->get_uv0_5();
		NullCheck(L_19);
		Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0091:
	{
		// mesh.SetTriangles(indices, 0, calculateBounds: true);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_21 = ___mesh0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_22 = __this->get_indices_1();
		NullCheck(L_21);
		Mesh_SetTriangles_mA799D3ECD82713495DBEEEDD1DB14DA8CCDAF1C2(L_21, L_22, 0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void RealtimeCSG.Foundation.GeneratedMeshContents::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratedMeshContents__ctor_mE4E0696A024F80FD5CAD7950FD95B0CAAB64A7F1 (GeneratedMeshContents_t5592C59A4FC4C3FBA848C7910B2E316B8425402C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RealtimeCSG.Foundation.GeneratedMeshDescription::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF (GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!(obj is GeneratedMeshDescription))
		RuntimeObject * L_0 = ___obj0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A_il2cpp_TypeInfo_var)))
		{
			goto IL_000a;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_000a:
	{
		// var description = (GeneratedMeshDescription)obj;
		RuntimeObject * L_1 = ___obj0;
		V_0 = ((*(GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A *)((GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A *)UnBox(L_1, GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A_il2cpp_TypeInfo_var))));
		// return meshQuery         == description.meshQuery &&
		//        surfaceParameter     == description.surfaceParameter &&
		//        subMeshQueryIndex == description.subMeshQueryIndex &&
		//        meshQueryIndex     == description.meshQueryIndex &&
		//        geometryHashValue == description.geometryHashValue &&
		//        surfaceHashValue  == description.surfaceHashValue &&
		//        vertexCount       == description.vertexCount &&
		//        indexCount        == description.indexCount;
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_2 = __this->get_meshQuery_0();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_3 = V_0;
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_4 = L_3.get_meshQuery_0();
		bool L_5 = MeshQuery_op_Equality_m3A2B9D930633CA85ABB5CE3A89E3E6C176D0F7BE(L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_6 = __this->get_surfaceParameter_1();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_7 = V_0;
		int32_t L_8 = L_7.get_surfaceParameter_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_9 = __this->get_subMeshQueryIndex_3();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_10 = V_0;
		int32_t L_11 = L_10.get_subMeshQueryIndex_3();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_11))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_12 = __this->get_meshQueryIndex_2();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_13 = V_0;
		int32_t L_14 = L_13.get_meshQueryIndex_2();
		if ((!(((uint32_t)L_12) == ((uint32_t)L_14))))
		{
			goto IL_0087;
		}
	}
	{
		int64_t L_15 = __this->get_geometryHashValue_4();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_16 = V_0;
		int64_t L_17 = L_16.get_geometryHashValue_4();
		if ((!(((uint64_t)L_15) == ((uint64_t)L_17))))
		{
			goto IL_0087;
		}
	}
	{
		int64_t L_18 = __this->get_surfaceHashValue_5();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_19 = V_0;
		int64_t L_20 = L_19.get_surfaceHashValue_5();
		if ((!(((uint64_t)L_18) == ((uint64_t)L_20))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_21 = __this->get_vertexCount_6();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_22 = V_0;
		int32_t L_23 = L_22.get_vertexCount_6();
		if ((!(((uint32_t)L_21) == ((uint32_t)L_23))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_24 = __this->get_indexCount_7();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_25 = V_0;
		int32_t L_26 = L_25.get_indexCount_7();
		return (bool)((((int32_t)L_24) == ((int32_t)L_26))? 1 : 0);
	}

IL_0087:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * _thisAdjusted = reinterpret_cast<GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A *>(__this + _offset);
	return GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF(_thisAdjusted, ___obj0, method);
}
// System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GeneratedMeshDescription_GetHashCode_mAEEFC5F7B260A59A966113874DC4AA3F77277C0B (GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * __this, const RuntimeMethod* method)
{
	{
		// var hashCode = -190551774;
		// hashCode = hashCode * -1521134295;
		// hashCode = hashCode * -1521134295 + meshQuery.GetHashCode();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * L_0 = __this->get_address_of_meshQuery_0();
		int32_t L_1 = MeshQuery_GetHashCode_m5FBF184A4ED516B0A600B2B4C0DC2C1C4DDE1B10((MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *)L_0, /*hidden argument*/NULL);
		// hashCode = hashCode * -1521134295 + (int)surfaceParameter;
		int32_t L_2 = __this->get_surfaceParameter_1();
		// hashCode = hashCode * -1521134295 + (int)subMeshQueryIndex;
		int32_t L_3 = __this->get_subMeshQueryIndex_3();
		// hashCode = hashCode * -1521134295 + (int)meshQueryIndex;
		int32_t L_4 = __this->get_meshQueryIndex_2();
		// hashCode = hashCode * -1521134295 + (int)geometryHashValue;
		int64_t L_5 = __this->get_geometryHashValue_4();
		// hashCode = hashCode * -1521134295 + (int)surfaceHashValue;
		int64_t L_6 = __this->get_surfaceHashValue_5();
		// hashCode = hashCode * -1521134295 + (int)vertexCount;
		int32_t L_7 = __this->get_vertexCount_6();
		// hashCode = hashCode * -1521134295 + (int)indexCount;
		int32_t L_8 = __this->get_indexCount_7();
		// return hashCode;
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)-190551774), (int32_t)((int32_t)-1521134295))), (int32_t)((int32_t)-1521134295))), (int32_t)L_1)), (int32_t)((int32_t)-1521134295))), (int32_t)L_2)), (int32_t)((int32_t)-1521134295))), (int32_t)L_3)), (int32_t)((int32_t)-1521134295))), (int32_t)L_4)), (int32_t)((int32_t)-1521134295))), (int32_t)(((int32_t)((int32_t)L_5))))), (int32_t)((int32_t)-1521134295))), (int32_t)(((int32_t)((int32_t)L_6))))), (int32_t)((int32_t)-1521134295))), (int32_t)L_7)), (int32_t)((int32_t)-1521134295))), (int32_t)L_8));
	}
}
IL2CPP_EXTERN_C  int32_t GeneratedMeshDescription_GetHashCode_mAEEFC5F7B260A59A966113874DC4AA3F77277C0B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A * _thisAdjusted = reinterpret_cast<GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A *>(__this + _offset);
	return GeneratedMeshDescription_GetHashCode_mAEEFC5F7B260A59A966113874DC4AA3F77277C0B(_thisAdjusted, method);
}
// System.Boolean RealtimeCSG.Foundation.GeneratedMeshDescription::op_Equality(RealtimeCSG.Foundation.GeneratedMeshDescription,RealtimeCSG.Foundation.GeneratedMeshDescription)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeneratedMeshDescription_op_Equality_m7DD880CECD1AC75CC2C1193DCAA7585CB0721B10 (GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  ___left0, GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  ___right1, const RuntimeMethod* method)
{
	{
		// return    left.meshQuery            == right.meshQuery &&
		//         left.surfaceParameter    == right.surfaceParameter &&
		//         left.subMeshQueryIndex    == right.subMeshQueryIndex &&
		//         left.meshQueryIndex        == right.meshQueryIndex &&
		//         left.geometryHashValue    == right.geometryHashValue &&
		//         left.surfaceHashValue    == right.surfaceHashValue &&
		//         left.vertexCount        == right.vertexCount &&
		//         left.indexCount            == right.indexCount;
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_0 = ___left0;
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_1 = L_0.get_meshQuery_0();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_2 = ___right1;
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_3 = L_2.get_meshQuery_0();
		bool L_4 = MeshQuery_op_Equality_m3A2B9D930633CA85ABB5CE3A89E3E6C176D0F7BE(L_1, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0076;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_5 = ___left0;
		int32_t L_6 = L_5.get_surfaceParameter_1();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_7 = ___right1;
		int32_t L_8 = L_7.get_surfaceParameter_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0076;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_9 = ___left0;
		int32_t L_10 = L_9.get_subMeshQueryIndex_3();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_11 = ___right1;
		int32_t L_12 = L_11.get_subMeshQueryIndex_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0076;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_13 = ___left0;
		int32_t L_14 = L_13.get_meshQueryIndex_2();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_15 = ___right1;
		int32_t L_16 = L_15.get_meshQueryIndex_2();
		if ((!(((uint32_t)L_14) == ((uint32_t)L_16))))
		{
			goto IL_0076;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_17 = ___left0;
		int64_t L_18 = L_17.get_geometryHashValue_4();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_19 = ___right1;
		int64_t L_20 = L_19.get_geometryHashValue_4();
		if ((!(((uint64_t)L_18) == ((uint64_t)L_20))))
		{
			goto IL_0076;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_21 = ___left0;
		int64_t L_22 = L_21.get_surfaceHashValue_5();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_23 = ___right1;
		int64_t L_24 = L_23.get_surfaceHashValue_5();
		if ((!(((uint64_t)L_22) == ((uint64_t)L_24))))
		{
			goto IL_0076;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_25 = ___left0;
		int32_t L_26 = L_25.get_vertexCount_6();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_27 = ___right1;
		int32_t L_28 = L_27.get_vertexCount_6();
		if ((!(((uint32_t)L_26) == ((uint32_t)L_28))))
		{
			goto IL_0076;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_29 = ___left0;
		int32_t L_30 = L_29.get_indexCount_7();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_31 = ___right1;
		int32_t L_32 = L_31.get_indexCount_7();
		return (bool)((((int32_t)L_30) == ((int32_t)L_32))? 1 : 0);
	}

IL_0076:
	{
		return (bool)0;
	}
}
// System.Boolean RealtimeCSG.Foundation.GeneratedMeshDescription::op_Inequality(RealtimeCSG.Foundation.GeneratedMeshDescription,RealtimeCSG.Foundation.GeneratedMeshDescription)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeneratedMeshDescription_op_Inequality_m50A84C22C19C8CD2DC1909CBC8363C992550AD06 (GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  ___left0, GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  ___right1, const RuntimeMethod* method)
{
	{
		// return    left.meshQuery            != right.meshQuery ||
		//         left.surfaceParameter    != right.surfaceParameter ||
		//         left.subMeshQueryIndex    != right.subMeshQueryIndex ||
		//         left.meshQueryIndex        != right.meshQueryIndex ||
		//         left.geometryHashValue    != right.geometryHashValue ||
		//         left.surfaceHashValue    != right.surfaceHashValue ||
		//         left.vertexCount        != right.vertexCount ||
		//         left.indexCount            != right.indexCount;
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_0 = ___left0;
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_1 = L_0.get_meshQuery_0();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_2 = ___right1;
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_3 = L_2.get_meshQuery_0();
		bool L_4 = MeshQuery_op_Inequality_m754576CB4F0C360EF2DDB08983FCAB87513DB3FC(L_1, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0079;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_5 = ___left0;
		int32_t L_6 = L_5.get_surfaceParameter_1();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_7 = ___right1;
		int32_t L_8 = L_7.get_surfaceParameter_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0079;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_9 = ___left0;
		int32_t L_10 = L_9.get_subMeshQueryIndex_3();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_11 = ___right1;
		int32_t L_12 = L_11.get_subMeshQueryIndex_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0079;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_13 = ___left0;
		int32_t L_14 = L_13.get_meshQueryIndex_2();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_15 = ___right1;
		int32_t L_16 = L_15.get_meshQueryIndex_2();
		if ((!(((uint32_t)L_14) == ((uint32_t)L_16))))
		{
			goto IL_0079;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_17 = ___left0;
		int64_t L_18 = L_17.get_geometryHashValue_4();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_19 = ___right1;
		int64_t L_20 = L_19.get_geometryHashValue_4();
		if ((!(((uint64_t)L_18) == ((uint64_t)L_20))))
		{
			goto IL_0079;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_21 = ___left0;
		int64_t L_22 = L_21.get_surfaceHashValue_5();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_23 = ___right1;
		int64_t L_24 = L_23.get_surfaceHashValue_5();
		if ((!(((uint64_t)L_22) == ((uint64_t)L_24))))
		{
			goto IL_0079;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_25 = ___left0;
		int32_t L_26 = L_25.get_vertexCount_6();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_27 = ___right1;
		int32_t L_28 = L_27.get_vertexCount_6();
		if ((!(((uint32_t)L_26) == ((uint32_t)L_28))))
		{
			goto IL_0079;
		}
	}
	{
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_29 = ___left0;
		int32_t L_30 = L_29.get_indexCount_7();
		GeneratedMeshDescription_t7D2AD8B53FD5CDC1319A4D97AF7C50F4AE88ED7A  L_31 = ___right1;
		int32_t L_32 = L_31.get_indexCount_7();
		return (bool)((((int32_t)((((int32_t)L_30) == ((int32_t)L_32))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0079:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.Foundation.MeshQuery::.ctor(RealtimeCSG.Foundation.LayerUsageFlags,RealtimeCSG.Foundation.LayerUsageFlags,RealtimeCSG.Foundation.LayerParameterIndex,RealtimeCSG.Foundation.VertexChannelFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery__ctor_mAFFC39222E6FE5BD686A17A40FDF664DCFB6210D (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, int32_t ___query0, int32_t ___mask1, uint8_t ___parameterIndex2, uint8_t ___vertexChannels3, const RuntimeMethod* method)
{
	{
		// if (mask == LayerUsageFlags.None) mask = query;
		int32_t L_0 = ___mask1;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		// if (mask == LayerUsageFlags.None) mask = query;
		int32_t L_1 = ___query0;
		___mask1 = L_1;
	}

IL_0006:
	{
		// this.layers                = ((uint)query & ~BitMask) | ((uint)parameterIndex << BitShift);
		int32_t L_2 = ___query0;
		uint8_t L_3 = ___parameterIndex2;
		__this->set_layers_2(((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2130706433)))|(int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)24))))));
		// this.maskAndChannels    = ((uint)mask  & ~BitMask) | ((uint)vertexChannels << BitShift);
		int32_t L_4 = ___mask1;
		uint8_t L_5 = ___vertexChannels3;
		__this->set_maskAndChannels_3(((int32_t)((int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)-2130706433)))|(int32_t)((int32_t)((int32_t)L_5<<(int32_t)((int32_t)24))))));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void MeshQuery__ctor_mAFFC39222E6FE5BD686A17A40FDF664DCFB6210D_AdjustorThunk (RuntimeObject * __this, int32_t ___query0, int32_t ___mask1, uint8_t ___parameterIndex2, uint8_t ___vertexChannels3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	MeshQuery__ctor_mAFFC39222E6FE5BD686A17A40FDF664DCFB6210D(_thisAdjusted, ___query0, ___mask1, ___parameterIndex2, ___vertexChannels3, method);
}
// RealtimeCSG.Foundation.LayerUsageFlags RealtimeCSG.Foundation.MeshQuery::get_LayerQuery()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshQuery_get_LayerQuery_m4703C9835C13FCF59B4B409303FCB42E0532FA36 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method)
{
	{
		// public LayerUsageFlags        LayerQuery            { get { return (LayerUsageFlags)((uint)layers          & ~BitMask); } set { layers          = ((uint)value & ~BitMask) | ((uint)layers          & BitMask); } }
		uint32_t L_0 = __this->get_layers_2();
		return (int32_t)(((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2130706433))));
	}
}
IL2CPP_EXTERN_C  int32_t MeshQuery_get_LayerQuery_m4703C9835C13FCF59B4B409303FCB42E0532FA36_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_get_LayerQuery_m4703C9835C13FCF59B4B409303FCB42E0532FA36(_thisAdjusted, method);
}
// System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerQuery(RealtimeCSG.Foundation.LayerUsageFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_LayerQuery_mC9946EC13A1FD87A223B830FF7A421B68D883374 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public LayerUsageFlags        LayerQuery            { get { return (LayerUsageFlags)((uint)layers          & ~BitMask); } set { layers          = ((uint)value & ~BitMask) | ((uint)layers          & BitMask); } }
		int32_t L_0 = ___value0;
		uint32_t L_1 = __this->get_layers_2();
		__this->set_layers_2(((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2130706433)))|(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)2130706432))))));
		// public LayerUsageFlags        LayerQuery            { get { return (LayerUsageFlags)((uint)layers          & ~BitMask); } set { layers          = ((uint)value & ~BitMask) | ((uint)layers          & BitMask); } }
		return;
	}
}
IL2CPP_EXTERN_C  void MeshQuery_set_LayerQuery_mC9946EC13A1FD87A223B830FF7A421B68D883374_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	MeshQuery_set_LayerQuery_mC9946EC13A1FD87A223B830FF7A421B68D883374(_thisAdjusted, ___value0, method);
}
// RealtimeCSG.Foundation.LayerUsageFlags RealtimeCSG.Foundation.MeshQuery::get_LayerQueryMask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshQuery_get_LayerQueryMask_mC503693548152B334FD4BE66859983D6D045C84D (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method)
{
	{
		// public LayerUsageFlags        LayerQueryMask        { get { return (LayerUsageFlags)((uint)maskAndChannels & ~BitMask); } set { maskAndChannels = ((uint)value & ~BitMask) | ((uint)maskAndChannels & BitMask); } }
		uint32_t L_0 = __this->get_maskAndChannels_3();
		return (int32_t)(((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2130706433))));
	}
}
IL2CPP_EXTERN_C  int32_t MeshQuery_get_LayerQueryMask_mC503693548152B334FD4BE66859983D6D045C84D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_get_LayerQueryMask_mC503693548152B334FD4BE66859983D6D045C84D(_thisAdjusted, method);
}
// System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerQueryMask(RealtimeCSG.Foundation.LayerUsageFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_LayerQueryMask_mDE272289C3E300914F392DFC5F893CE18D83DD57 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public LayerUsageFlags        LayerQueryMask        { get { return (LayerUsageFlags)((uint)maskAndChannels & ~BitMask); } set { maskAndChannels = ((uint)value & ~BitMask) | ((uint)maskAndChannels & BitMask); } }
		int32_t L_0 = ___value0;
		uint32_t L_1 = __this->get_maskAndChannels_3();
		__this->set_maskAndChannels_3(((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2130706433)))|(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)2130706432))))));
		// public LayerUsageFlags        LayerQueryMask        { get { return (LayerUsageFlags)((uint)maskAndChannels & ~BitMask); } set { maskAndChannels = ((uint)value & ~BitMask) | ((uint)maskAndChannels & BitMask); } }
		return;
	}
}
IL2CPP_EXTERN_C  void MeshQuery_set_LayerQueryMask_mDE272289C3E300914F392DFC5F893CE18D83DD57_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	MeshQuery_set_LayerQueryMask_mDE272289C3E300914F392DFC5F893CE18D83DD57(_thisAdjusted, ___value0, method);
}
// RealtimeCSG.Foundation.LayerParameterIndex RealtimeCSG.Foundation.MeshQuery::get_LayerParameterIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t MeshQuery_get_LayerParameterIndex_mDCB4D744B10C68E3B7D28DE40ECFEE24CD264186 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method)
{
	{
		// public LayerParameterIndex    LayerParameterIndex { get { return (LayerParameterIndex)(((uint)layers          & BitMask) >> BitShift); } set { layers          = ((uint)layers          & ~BitMask) | ((uint)value << BitShift); } }
		uint32_t L_0 = __this->get_layers_2();
		return (uint8_t)((((int32_t)((uint8_t)((int32_t)((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)2130706432)))>>((int32_t)24)))))));
	}
}
IL2CPP_EXTERN_C  uint8_t MeshQuery_get_LayerParameterIndex_mDCB4D744B10C68E3B7D28DE40ECFEE24CD264186_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_get_LayerParameterIndex_mDCB4D744B10C68E3B7D28DE40ECFEE24CD264186(_thisAdjusted, method);
}
// System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerParameterIndex(RealtimeCSG.Foundation.LayerParameterIndex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_LayerParameterIndex_m8369D65DEFA1422B838AE8E56EAEEBFB5B733A6D (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	{
		// public LayerParameterIndex    LayerParameterIndex { get { return (LayerParameterIndex)(((uint)layers          & BitMask) >> BitShift); } set { layers          = ((uint)layers          & ~BitMask) | ((uint)value << BitShift); } }
		uint32_t L_0 = __this->get_layers_2();
		uint8_t L_1 = ___value0;
		__this->set_layers_2(((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2130706433)))|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)24))))));
		// public LayerParameterIndex    LayerParameterIndex { get { return (LayerParameterIndex)(((uint)layers          & BitMask) >> BitShift); } set { layers          = ((uint)layers          & ~BitMask) | ((uint)value << BitShift); } }
		return;
	}
}
IL2CPP_EXTERN_C  void MeshQuery_set_LayerParameterIndex_m8369D65DEFA1422B838AE8E56EAEEBFB5B733A6D_AdjustorThunk (RuntimeObject * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	MeshQuery_set_LayerParameterIndex_m8369D65DEFA1422B838AE8E56EAEEBFB5B733A6D(_thisAdjusted, ___value0, method);
}
// RealtimeCSG.Foundation.VertexChannelFlags RealtimeCSG.Foundation.MeshQuery::get_UsedVertexChannels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t MeshQuery_get_UsedVertexChannels_mA414AB75F94B51B487825C0DD43BA83759836E6A (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method)
{
	{
		// public VertexChannelFlags    UsedVertexChannels    { get { return (VertexChannelFlags )(((uint)maskAndChannels & BitMask) >> BitShift); } set { maskAndChannels = ((uint)maskAndChannels & ~BitMask) | ((uint)value << BitShift); } }
		uint32_t L_0 = __this->get_maskAndChannels_3();
		return (uint8_t)((((int32_t)((uint8_t)((int32_t)((uint32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)2130706432)))>>((int32_t)24)))))));
	}
}
IL2CPP_EXTERN_C  uint8_t MeshQuery_get_UsedVertexChannels_mA414AB75F94B51B487825C0DD43BA83759836E6A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_get_UsedVertexChannels_mA414AB75F94B51B487825C0DD43BA83759836E6A(_thisAdjusted, method);
}
// System.Void RealtimeCSG.Foundation.MeshQuery::set_UsedVertexChannels(RealtimeCSG.Foundation.VertexChannelFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshQuery_set_UsedVertexChannels_mE603C1D0593AA6FB6012D060B4047A3F6E8BD3F0 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	{
		// public VertexChannelFlags    UsedVertexChannels    { get { return (VertexChannelFlags )(((uint)maskAndChannels & BitMask) >> BitShift); } set { maskAndChannels = ((uint)maskAndChannels & ~BitMask) | ((uint)value << BitShift); } }
		uint32_t L_0 = __this->get_maskAndChannels_3();
		uint8_t L_1 = ___value0;
		__this->set_maskAndChannels_3(((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2130706433)))|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)24))))));
		// public VertexChannelFlags    UsedVertexChannels    { get { return (VertexChannelFlags )(((uint)maskAndChannels & BitMask) >> BitShift); } set { maskAndChannels = ((uint)maskAndChannels & ~BitMask) | ((uint)value << BitShift); } }
		return;
	}
}
IL2CPP_EXTERN_C  void MeshQuery_set_UsedVertexChannels_mE603C1D0593AA6FB6012D060B4047A3F6E8BD3F0_AdjustorThunk (RuntimeObject * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	MeshQuery_set_UsedVertexChannels_mE603C1D0593AA6FB6012D060B4047A3F6E8BD3F0(_thisAdjusted, ___value0, method);
}
// System.String RealtimeCSG.Foundation.MeshQuery::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("({0}, {1})", layers, maskAndChannels);
		uint32_t L_0 = __this->get_layers_2();
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = __this->get_maskAndChannels_3();
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A(_stringLiteral6AB2A5D1FE0A10CB014FC4303709BA72103C7CD0, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_EXTERN_C  String_t* MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5(_thisAdjusted, method);
}
// System.Boolean RealtimeCSG.Foundation.MeshQuery::op_Equality(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_op_Equality_m3A2B9D930633CA85ABB5CE3A89E3E6C176D0F7BE (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___left0, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___right1, const RuntimeMethod* method)
{
	{
		// public static bool operator == (MeshQuery left, MeshQuery right) { return left.layers == right.layers && left.maskAndChannels == right.maskAndChannels; }
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_0 = ___left0;
		uint32_t L_1 = L_0.get_layers_2();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_2 = ___right1;
		uint32_t L_3 = L_2.get_layers_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_001d;
		}
	}
	{
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_4 = ___left0;
		uint32_t L_5 = L_4.get_maskAndChannels_3();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_6 = ___right1;
		uint32_t L_7 = L_6.get_maskAndChannels_3();
		return (bool)((((int32_t)L_5) == ((int32_t)L_7))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Boolean RealtimeCSG.Foundation.MeshQuery::op_Inequality(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_op_Inequality_m754576CB4F0C360EF2DDB08983FCAB87513DB3FC (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___left0, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___right1, const RuntimeMethod* method)
{
	{
		// public static bool operator != (MeshQuery left, MeshQuery right) { return left.layers != right.layers || left.maskAndChannels != right.maskAndChannels; }
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_0 = ___left0;
		uint32_t L_1 = L_0.get_layers_2();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_2 = ___right1;
		uint32_t L_3 = L_2.get_layers_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0020;
		}
	}
	{
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_4 = ___left0;
		uint32_t L_5 = L_4.get_maskAndChannels_3();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_6 = ___right1;
		uint32_t L_7 = L_6.get_maskAndChannels_3();
		return (bool)((((int32_t)((((int32_t)L_5) == ((int32_t)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0020:
	{
		return (bool)1;
	}
}
// System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// public override bool Equals(object obj) { if (!(obj is MeshQuery)) return false; var other = (MeshQuery)obj; return layers == other.layers && maskAndChannels == other.maskAndChannels; }
		RuntimeObject * L_0 = ___obj0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D_il2cpp_TypeInfo_var)))
		{
			goto IL_000a;
		}
	}
	{
		// public override bool Equals(object obj) { if (!(obj is MeshQuery)) return false; var other = (MeshQuery)obj; return layers == other.layers && maskAndChannels == other.maskAndChannels; }
		return (bool)0;
	}

IL_000a:
	{
		// public override bool Equals(object obj) { if (!(obj is MeshQuery)) return false; var other = (MeshQuery)obj; return layers == other.layers && maskAndChannels == other.maskAndChannels; }
		RuntimeObject * L_1 = ___obj0;
		V_0 = ((*(MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *)((MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *)UnBox(L_1, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D_il2cpp_TypeInfo_var))));
		// public override bool Equals(object obj) { if (!(obj is MeshQuery)) return false; var other = (MeshQuery)obj; return layers == other.layers && maskAndChannels == other.maskAndChannels; }
		uint32_t L_2 = __this->get_layers_2();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_3 = V_0;
		uint32_t L_4 = L_3.get_layers_2();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_002e;
		}
	}
	{
		uint32_t L_5 = __this->get_maskAndChannels_3();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_6 = V_0;
		uint32_t L_7 = L_6.get_maskAndChannels_3();
		return (bool)((((int32_t)L_5) == ((int32_t)L_7))? 1 : 0);
	}

IL_002e:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA(_thisAdjusted, ___obj0, method);
}
// System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(RealtimeCSG.Foundation.MeshQuery)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_Equals_m855368ED8A4BB10172F924F3050916596BBEFB74 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___other0, const RuntimeMethod* method)
{
	{
		// public bool Equals(MeshQuery other) { return layers == other.layers && maskAndChannels == other.maskAndChannels; }
		uint32_t L_0 = __this->get_layers_2();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_1 = ___other0;
		uint32_t L_2 = L_1.get_layers_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		uint32_t L_3 = __this->get_maskAndChannels_3();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_4 = ___other0;
		uint32_t L_5 = L_4.get_maskAndChannels_3();
		return (bool)((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool MeshQuery_Equals_m855368ED8A4BB10172F924F3050916596BBEFB74_AdjustorThunk (RuntimeObject * __this, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_Equals_m855368ED8A4BB10172F924F3050916596BBEFB74(_thisAdjusted, ___other0, method);
}
// System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MeshQuery_Equals_m8F0D1975C3DC6414A0B3917BF3A3E788138D5366 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___left0, MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  ___right1, const RuntimeMethod* method)
{
	{
		// public static bool Equals(MeshQuery left, MeshQuery right) { return left.layers == right.layers && left.maskAndChannels == right.maskAndChannels; }
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_0 = ___left0;
		uint32_t L_1 = L_0.get_layers_2();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_2 = ___right1;
		uint32_t L_3 = L_2.get_layers_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_001d;
		}
	}
	{
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_4 = ___left0;
		uint32_t L_5 = L_4.get_maskAndChannels_3();
		MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D  L_6 = ___right1;
		uint32_t L_7 = L_6.get_maskAndChannels_3();
		return (bool)((((int32_t)L_5) == ((int32_t)L_7))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 RealtimeCSG.Foundation.MeshQuery::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshQuery_GetHashCode_m5FBF184A4ED516B0A600B2B4C0DC2C1C4DDE1B10 (MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { var hashCode = -1385006369; hashCode = hashCode * -1521134295; hashCode = hashCode * -1521134295 + (int)layers; hashCode = hashCode * -1521134295 + (int)maskAndChannels; return hashCode; }
		// public override int GetHashCode() { var hashCode = -1385006369; hashCode = hashCode * -1521134295; hashCode = hashCode * -1521134295 + (int)layers; hashCode = hashCode * -1521134295 + (int)maskAndChannels; return hashCode; }
		// public override int GetHashCode() { var hashCode = -1385006369; hashCode = hashCode * -1521134295; hashCode = hashCode * -1521134295 + (int)layers; hashCode = hashCode * -1521134295 + (int)maskAndChannels; return hashCode; }
		uint32_t L_0 = __this->get_layers_2();
		// public override int GetHashCode() { var hashCode = -1385006369; hashCode = hashCode * -1521134295; hashCode = hashCode * -1521134295 + (int)layers; hashCode = hashCode * -1521134295 + (int)maskAndChannels; return hashCode; }
		uint32_t L_1 = __this->get_maskAndChannels_3();
		// public override int GetHashCode() { var hashCode = -1385006369; hashCode = hashCode * -1521134295; hashCode = hashCode * -1521134295 + (int)layers; hashCode = hashCode * -1521134295 + (int)maskAndChannels; return hashCode; }
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)-1385006369), (int32_t)((int32_t)-1521134295))), (int32_t)((int32_t)-1521134295))), (int32_t)L_0)), (int32_t)((int32_t)-1521134295))), (int32_t)L_1));
	}
}
IL2CPP_EXTERN_C  int32_t MeshQuery_GetHashCode_m5FBF184A4ED516B0A600B2B4C0DC2C1C4DDE1B10_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D * _thisAdjusted = reinterpret_cast<MeshQuery_tF18EE9471A9636AF7C4D971C8779C61F389BD85D *>(__this + _offset);
	return MeshQuery_GetHashCode_m5FBF184A4ED516B0A600B2B4C0DC2C1C4DDE1B10(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method)
{
	{
		// get { return new Vector3(a, b, c); }
		float L_0 = __this->get_a_0();
		float L_1 = __this->get_b_1();
		float L_2 = __this->get_c_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF(_thisAdjusted, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::set_normal(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane_set_normal_m946BA6635B964F0675FBC69F5EFC9BD95706D8BD (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		// set { a = value.x; b = value.y; c = value.z; }
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___value0;
		float L_1 = L_0.get_x_2();
		__this->set_a_0(L_1);
		// set { a = value.x; b = value.y; c = value.z; }
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___value0;
		float L_3 = L_2.get_y_3();
		__this->set_b_1(L_3);
		// set { a = value.x; b = value.y; c = value.z; }
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___value0;
		float L_5 = L_4.get_z_4();
		__this->set_c_2(L_5);
		// set { a = value.x; b = value.y; c = value.z; }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane_set_normal_m946BA6635B964F0675FBC69F5EFC9BD95706D8BD_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane_set_normal_m946BA6635B964F0675FBC69F5EFC9BD95706D8BD(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::get_pointOnPlane()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Vector3 pointOnPlane { get { return normal * d; } }
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, /*hidden argument*/NULL);
		float L_1 = __this->get_d_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB(_thisAdjusted, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Plane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m93D9A7BCD9C724AF1005E13B7DF4FCA19D50963F (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___plane0, const RuntimeMethod* method)
{
	{
		// a = plane.normal.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___plane0), /*hidden argument*/NULL);
		float L_1 = L_0.get_x_2();
		__this->set_a_0(L_1);
		// b = plane.normal.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___plane0), /*hidden argument*/NULL);
		float L_3 = L_2.get_y_3();
		__this->set_b_1(L_3);
		// c = plane.normal.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___plane0), /*hidden argument*/NULL);
		float L_5 = L_4.get_z_4();
		__this->set_c_2(L_5);
		// d = -plane.distance;
		float L_6 = Plane_get_distance_m5358B80C35E1E295C0133E7DC6449BB09C456DEE((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___plane0), /*hidden argument*/NULL);
		__this->set_d_3(((-L_6)));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane__ctor_m93D9A7BCD9C724AF1005E13B7DF4FCA19D50963F_AdjustorThunk (RuntimeObject * __this, Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___plane0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane__ctor_m93D9A7BCD9C724AF1005E13B7DF4FCA19D50963F(_thisAdjusted, ___plane0, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m31DC6D44D2B5A5C6C686E45A5523EF7E4ECA77F2 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___inNormal0, float ___inD1, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var normal = inNormal.normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___inNormal0), /*hidden argument*/NULL);
		V_0 = L_0;
		// a = normal.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = V_0;
		float L_2 = L_1.get_x_2();
		__this->set_a_0(L_2);
		// b = normal.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = V_0;
		float L_4 = L_3.get_y_3();
		__this->set_b_1(L_4);
		// c = normal.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = V_0;
		float L_6 = L_5.get_z_4();
		__this->set_c_2(L_6);
		// d = inD;
		float L_7 = ___inD1;
		__this->set_d_3(L_7);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane__ctor_m31DC6D44D2B5A5C6C686E45A5523EF7E4ECA77F2_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___inNormal0, float ___inD1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane__ctor_m31DC6D44D2B5A5C6C686E45A5523EF7E4ECA77F2(_thisAdjusted, ___inNormal0, ___inD1, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___inNormal0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pointOnPlane1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var normal = inNormal.normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___inNormal0), /*hidden argument*/NULL);
		V_0 = L_0;
		// a = normal.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = V_0;
		float L_2 = L_1.get_x_2();
		__this->set_a_0(L_2);
		// b = normal.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = V_0;
		float L_4 = L_3.get_y_3();
		__this->set_b_1(L_4);
		// c = normal.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = V_0;
		float L_6 = L_5.get_z_4();
		__this->set_c_2(L_6);
		// d = Vector3.Dot(normal, pointOnPlane);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___pointOnPlane1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_9 = Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1(L_7, L_8, /*hidden argument*/NULL);
		__this->set_d_3(L_9);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___inNormal0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pointOnPlane1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3(_thisAdjusted, ___inNormal0, ___pointOnPlane1, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___inRotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pointOnPlane1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var normal    = (inRotation * MathConstants.upVector3).normalized;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_0 = ___inRotation0;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_upVector3_21();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), /*hidden argument*/NULL);
		V_0 = L_3;
		// a    = normal.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = V_0;
		float L_5 = L_4.get_x_2();
		__this->set_a_0(L_5);
		// b    = normal.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = V_0;
		float L_7 = L_6.get_y_3();
		__this->set_b_1(L_7);
		// c    = normal.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_0;
		float L_9 = L_8.get_z_4();
		__this->set_c_2(L_9);
		// d    = Vector3.Dot(normal, pointOnPlane);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = ___pointOnPlane1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_12 = Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1(L_10, L_11, /*hidden argument*/NULL);
		__this->set_d_3(L_12);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130_AdjustorThunk (RuntimeObject * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___inRotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pointOnPlane1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130(_thisAdjusted, ___inRotation0, ___pointOnPlane1, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, float ___inA0, float ___inB1, float ___inC2, float ___inD3, const RuntimeMethod* method)
{
	{
		// a = inA;
		float L_0 = ___inA0;
		__this->set_a_0(L_0);
		// b = inB;
		float L_1 = ___inB1;
		__this->set_b_1(L_1);
		// c = inC;
		float L_2 = ___inC2;
		__this->set_c_2(L_2);
		// d = inD;
		float L_3 = ___inD3;
		__this->set_d_3(L_3);
		// Normalize();
		CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402_AdjustorThunk (RuntimeObject * __this, float ___inA0, float ___inB1, float ___inC2, float ___inD3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402(_thisAdjusted, ___inA0, ___inB1, ___inC2, ___inD3, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point21, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point32, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// var ab = (point2 - point1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___point21;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___point10;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_1, /*hidden argument*/NULL);
		// var ac = (point3 - point1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___point32;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___point10;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// var normal = Vector3.Cross(ab, ac).normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_Cross_m3E9DBC445228FDB850BDBB4B01D6F61AC0111887(L_2, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_2), /*hidden argument*/NULL);
		V_1 = L_8;
		// a = normal.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = V_1;
		float L_10 = L_9.get_x_2();
		__this->set_a_0(L_10);
		// b = normal.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = V_1;
		float L_12 = L_11.get_y_3();
		__this->set_b_1(L_12);
		// c = normal.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_1;
		float L_14 = L_13.get_z_4();
		__this->set_c_2(L_14);
		// d = Vector3.Dot(normal, point1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = ___point10;
		float L_17 = Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1(L_15, L_16, /*hidden argument*/NULL);
		__this->set_d_3(L_17);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point21, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point32, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1(_thisAdjusted, ___point10, ___point21, ___point32, method);
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::RayIntersection(UnityEngine.Ray)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_RayIntersection_m617BEB5AFB352E3526C5D3980CEEDA793F89DD0B (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	double V_6 = 0.0;
	double V_7 = 0.0;
	double V_8 = 0.0;
	double V_9 = 0.0;
	{
		// var start_x            = (double)ray.origin.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_1 = L_0.get_x_2();
		V_0 = (((double)((double)L_1)));
		// var start_y            = (double)ray.origin.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_3 = L_2.get_y_3();
		V_1 = (((double)((double)L_3)));
		// var start_z            = (double)ray.origin.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_5 = L_4.get_z_4();
		V_2 = (((double)((double)L_5)));
		// var direction_x        = (double)ray.direction.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_7 = L_6.get_x_2();
		V_3 = (((double)((double)L_7)));
		// var direction_y        = (double)ray.direction.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		V_4 = (((double)((double)L_9)));
		// var direction_z        = (double)ray.direction.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_11 = L_10.get_z_4();
		V_5 = (((double)((double)L_11)));
		// var distanceA    = (a * start_x) +
		//                   (b * start_y) +
		//                   (c * start_z) -
		//                   (d);
		float L_12 = __this->get_a_0();
		double L_13 = V_0;
		float L_14 = __this->get_b_1();
		double L_15 = V_1;
		float L_16 = __this->get_c_2();
		double L_17 = V_2;
		float L_18 = __this->get_d_3();
		// var length        = (a * direction_x) +
		//                   (b * direction_y) +
		//                   (c * direction_z);
		float L_19 = __this->get_a_0();
		double L_20 = V_3;
		float L_21 = __this->get_b_1();
		double L_22 = V_4;
		float L_23 = __this->get_c_2();
		double L_24 = V_5;
		V_6 = ((double)il2cpp_codegen_add((double)((double)il2cpp_codegen_add((double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_19))), (double)L_20)), (double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_21))), (double)L_22)))), (double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_23))), (double)L_24))));
		// var delta        = distanceA / length;
		double L_25 = V_6;
		V_7 = ((double)((double)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_add((double)((double)il2cpp_codegen_add((double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_12))), (double)L_13)), (double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_14))), (double)L_15)))), (double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_16))), (double)L_17)))), (double)(((double)((double)L_18)))))/(double)L_25));
		// var x = start_x - (delta * direction_x);
		double L_26 = V_0;
		double L_27 = V_7;
		double L_28 = V_3;
		// var y = start_y - (delta * direction_y);
		double L_29 = V_1;
		double L_30 = V_7;
		double L_31 = V_4;
		V_8 = ((double)il2cpp_codegen_subtract((double)L_29, (double)((double)il2cpp_codegen_multiply((double)L_30, (double)L_31))));
		// var z = start_z - (delta * direction_z);
		double L_32 = V_2;
		double L_33 = V_7;
		double L_34 = V_5;
		V_9 = ((double)il2cpp_codegen_subtract((double)L_32, (double)((double)il2cpp_codegen_multiply((double)L_33, (double)L_34))));
		// return new Vector3((float)x, (float)y, (float)z);
		double L_35 = V_8;
		double L_36 = V_9;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37;
		memset((&L_37), 0, sizeof(L_37));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_37), (((float)((float)((double)il2cpp_codegen_subtract((double)L_26, (double)((double)il2cpp_codegen_multiply((double)L_27, (double)L_28))))))), (((float)((float)L_35))), (((float)((float)L_36))), /*hidden argument*/NULL);
		return L_37;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_RayIntersection_m617BEB5AFB352E3526C5D3980CEEDA793F89DD0B_AdjustorThunk (RuntimeObject * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_RayIntersection_m617BEB5AFB352E3526C5D3980CEEDA793F89DD0B(_thisAdjusted, ___ray0, method);
}
// System.Boolean RealtimeCSG.Legacy.CSGPlane::TryRayIntersection(UnityEngine.Ray,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___intersection1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_4;
	memset((&V_4), 0, sizeof(V_4));
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		// var start        = ray.origin;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		V_0 = L_0;
		// var end            = ray.origin + ray.direction * 1000.0f;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_2, (1000.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_1, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// var distanceA    = Distance(start);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = V_0;
		float L_6 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		// if (float.IsInfinity(distanceA) || float.IsNaN(distanceA))
		float L_7 = V_2;
		bool L_8 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003e;
		}
	}
	{
		float L_9 = V_2;
		bool L_10 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004b;
		}
	}

IL_003e:
	{
		// intersection = MathConstants.zeroVector3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_11 = ___intersection1;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_11 = L_12;
		// return false;
		return (bool)0;
	}

IL_004b:
	{
		// var distanceB    = Distance(end);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_1;
		float L_14 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		// if (float.IsInfinity(distanceB) || float.IsNaN(distanceB))
		float L_15 = V_3;
		bool L_16 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0063;
		}
	}
	{
		float L_17 = V_3;
		bool L_18 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0070;
		}
	}

IL_0063:
	{
		// intersection = MathConstants.zeroVector3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_19 = ___intersection1;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_19 = L_20;
		// return false;
		return (bool)0;
	}

IL_0070:
	{
		// Vector3 vector    = end - start;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_21, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		// float length    = distanceB - distanceA;
		float L_24 = V_3;
		float L_25 = V_2;
		V_5 = ((float)il2cpp_codegen_subtract((float)L_24, (float)L_25));
		// float delta        = distanceB / length;
		float L_26 = V_3;
		float L_27 = V_5;
		V_6 = ((float)((float)L_26/(float)L_27));
		// intersection = end - (delta * vector);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_28 = ___intersection1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = V_1;
		float L_30 = V_6;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = V_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_30, L_31, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_29, L_32, /*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_28 = L_33;
		// if (float.IsInfinity(intersection.x) || float.IsNaN(intersection.x) ||
		//     float.IsInfinity(intersection.y) || float.IsNaN(intersection.y) ||
		//     float.IsInfinity(intersection.z) || float.IsNaN(intersection.z))
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_34 = ___intersection1;
		float L_35 = L_34->get_x_2();
		bool L_36 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_00e7;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_37 = ___intersection1;
		float L_38 = L_37->get_x_2();
		bool L_39 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_38, /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_00e7;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_40 = ___intersection1;
		float L_41 = L_40->get_y_3();
		bool L_42 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_41, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_00e7;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_43 = ___intersection1;
		float L_44 = L_43->get_y_3();
		bool L_45 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_00e7;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_46 = ___intersection1;
		float L_47 = L_46->get_z_4();
		bool L_48 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_47, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_00e7;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_49 = ___intersection1;
		float L_50 = L_49->get_z_4();
		bool L_51 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_50, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_00f4;
		}
	}

IL_00e7:
	{
		// intersection = MathConstants.zeroVector3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_52 = ___intersection1;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_53 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_52 = L_53;
		// return false;
		return (bool)0;
	}

IL_00f4:
	{
		// return true;
		return (bool)1;
	}
}
IL2CPP_EXTERN_C  bool CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5_AdjustorThunk (RuntimeObject * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___intersection1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5(_thisAdjusted, ___ray0, ___intersection1, method);
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::LineIntersection(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// Vector3 vector    = end - start;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___end1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___start0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// float edist        = Distance(end);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___end1;
		float L_4 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, L_3, /*hidden argument*/NULL);
		// float sdist        = Distance(start);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = ___start0;
		float L_6 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// float length    = edist - sdist;
		float L_7 = L_4;
		float L_8 = V_1;
		V_2 = ((float)il2cpp_codegen_subtract((float)L_7, (float)L_8));
		// float delta        = edist / length;
		float L_9 = V_2;
		V_3 = ((float)((float)L_7/(float)L_9));
		// return end - (delta * vector);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = ___end1;
		float L_11 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_11, L_12, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_10, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580(_thisAdjusted, ___start0, ___end1, method);
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::Intersection(RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_Intersection_m77828575E659430171184D441835224E158F1898 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___inPlane10, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___inPlane21, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___inPlane32, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_Intersection_m77828575E659430171184D441835224E158F1898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_8;
	memset((&V_8), 0, sizeof(V_8));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_9;
	memset((&V_9), 0, sizeof(V_9));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_10;
	memset((&V_10), 0, sizeof(V_10));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_11;
	memset((&V_11), 0, sizeof(V_11));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_12;
	memset((&V_12), 0, sizeof(V_12));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_13;
	memset((&V_13), 0, sizeof(V_13));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_14;
	memset((&V_14), 0, sizeof(V_14));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_15;
	memset((&V_15), 0, sizeof(V_15));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_16;
	memset((&V_16), 0, sizeof(V_16));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_17;
	memset((&V_17), 0, sizeof(V_17));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_18;
	memset((&V_18), 0, sizeof(V_18));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_19;
	memset((&V_19), 0, sizeof(V_19));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_20;
	memset((&V_20), 0, sizeof(V_20));
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  V_21;
	memset((&V_21), 0, sizeof(V_21));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_22;
	memset((&V_22), 0, sizeof(V_22));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_23;
	memset((&V_23), 0, sizeof(V_23));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			// var plane1a = (decimal)inPlane1.a;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_0 = ___inPlane10;
			float L_1 = L_0.get_a_0();
			IL2CPP_RUNTIME_CLASS_INIT(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_il2cpp_TypeInfo_var);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_2 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			// var plane1b = (decimal)inPlane1.b;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_3 = ___inPlane10;
			float L_4 = L_3.get_b_1();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_5 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_4, /*hidden argument*/NULL);
			V_1 = L_5;
			// var plane1c = (decimal)inPlane1.c;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_6 = ___inPlane10;
			float L_7 = L_6.get_c_2();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_8 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			// var plane1d = (decimal)inPlane1.d;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_9 = ___inPlane10;
			float L_10 = L_9.get_d_3();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_11 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_10, /*hidden argument*/NULL);
			V_3 = L_11;
			// var plane2a = (decimal)inPlane2.a;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_12 = ___inPlane21;
			float L_13 = L_12.get_a_0();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_14 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_13, /*hidden argument*/NULL);
			V_4 = L_14;
			// var plane2b = (decimal)inPlane2.b;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_15 = ___inPlane21;
			float L_16 = L_15.get_b_1();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_17 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_16, /*hidden argument*/NULL);
			V_5 = L_17;
			// var plane2c = (decimal)inPlane2.c;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_18 = ___inPlane21;
			float L_19 = L_18.get_c_2();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_20 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_19, /*hidden argument*/NULL);
			V_6 = L_20;
			// var plane2d = (decimal)inPlane2.d;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_21 = ___inPlane21;
			float L_22 = L_21.get_d_3();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_23 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_22, /*hidden argument*/NULL);
			V_7 = L_23;
			// var plane3a = (decimal)inPlane3.a;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_24 = ___inPlane32;
			float L_25 = L_24.get_a_0();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_26 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_25, /*hidden argument*/NULL);
			V_8 = L_26;
			// var plane3b = (decimal)inPlane3.b;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_27 = ___inPlane32;
			float L_28 = L_27.get_b_1();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_29 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_28, /*hidden argument*/NULL);
			V_9 = L_29;
			// var plane3c = (decimal)inPlane3.c;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_30 = ___inPlane32;
			float L_31 = L_30.get_c_2();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_32 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_31, /*hidden argument*/NULL);
			V_10 = L_32;
			// var plane3d = (decimal)inPlane3.d;
			CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_33 = ___inPlane32;
			float L_34 = L_33.get_d_3();
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_35 = Decimal_op_Explicit_m9AE85BFCE75391680A7D4EA28FF4D42959F37E39(L_34, /*hidden argument*/NULL);
			V_11 = L_35;
			// var bc1 = (plane1b * plane3c) - (plane3b * plane1c);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_36 = V_1;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_37 = V_10;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_38 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_36, L_37, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_39 = V_9;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_40 = V_2;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_41 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_39, L_40, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_42 = Decimal_op_Subtraction_mA8822FE4BA50620B0A58B46C8B46A08361C7FF4E(L_38, L_41, /*hidden argument*/NULL);
			V_12 = L_42;
			// var bc2 = (plane2b * plane1c) - (plane1b * plane2c);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_43 = V_5;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_44 = V_2;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_45 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_43, L_44, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_46 = V_1;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_47 = V_6;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_48 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_46, L_47, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_49 = Decimal_op_Subtraction_mA8822FE4BA50620B0A58B46C8B46A08361C7FF4E(L_45, L_48, /*hidden argument*/NULL);
			V_13 = L_49;
			// var bc3 = (plane3b * plane2c) - (plane2b * plane3c);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_50 = V_9;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_51 = V_6;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_52 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_50, L_51, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_53 = V_5;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_54 = V_10;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_55 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_53, L_54, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_56 = Decimal_op_Subtraction_mA8822FE4BA50620B0A58B46C8B46A08361C7FF4E(L_52, L_55, /*hidden argument*/NULL);
			V_14 = L_56;
			// var w = -((plane1a * bc3) + (plane2a * bc1) + (plane3a * bc2));
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_57 = V_0;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_58 = V_14;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_59 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_57, L_58, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_60 = V_4;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_61 = V_12;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_62 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_60, L_61, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_63 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_59, L_62, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_64 = V_8;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_65 = V_13;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_66 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_64, L_65, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_67 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_63, L_66, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_68 = Decimal_op_UnaryNegation_mFC91CB75C3905B5CD2847152B45B28E85F4D4884(L_67, /*hidden argument*/NULL);
			V_15 = L_68;
			// var ad1 = (plane1a * plane3d) - (plane3a * plane1d);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_69 = V_0;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_70 = V_11;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_71 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_69, L_70, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_72 = V_8;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_73 = V_3;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_74 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_72, L_73, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_75 = Decimal_op_Subtraction_mA8822FE4BA50620B0A58B46C8B46A08361C7FF4E(L_71, L_74, /*hidden argument*/NULL);
			V_16 = L_75;
			// var ad2 = (plane2a * plane1d) - (plane1a * plane2d);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_76 = V_4;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_77 = V_3;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_78 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_76, L_77, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_79 = V_0;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_80 = V_7;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_81 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_79, L_80, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_82 = Decimal_op_Subtraction_mA8822FE4BA50620B0A58B46C8B46A08361C7FF4E(L_78, L_81, /*hidden argument*/NULL);
			V_17 = L_82;
			// var ad3 = (plane3a * plane2d) - (plane2a * plane3d);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_83 = V_8;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_84 = V_7;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_85 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_83, L_84, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_86 = V_4;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_87 = V_11;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_88 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_86, L_87, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_89 = Decimal_op_Subtraction_mA8822FE4BA50620B0A58B46C8B46A08361C7FF4E(L_85, L_88, /*hidden argument*/NULL);
			V_18 = L_89;
			// var x = -((plane1d * bc3) + (plane2d * bc1) + (plane3d * bc2));
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_90 = V_3;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_91 = V_14;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_92 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_90, L_91, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_93 = V_7;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_94 = V_12;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_95 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_93, L_94, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_96 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_92, L_95, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_97 = V_11;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_98 = V_13;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_99 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_97, L_98, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_100 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_96, L_99, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_101 = Decimal_op_UnaryNegation_mFC91CB75C3905B5CD2847152B45B28E85F4D4884(L_100, /*hidden argument*/NULL);
			V_19 = L_101;
			// var y = -((plane1c * ad3) + (plane2c * ad1) + (plane3c * ad2));
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_102 = V_2;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_103 = V_18;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_104 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_102, L_103, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_105 = V_6;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_106 = V_16;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_107 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_105, L_106, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_108 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_104, L_107, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_109 = V_10;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_110 = V_17;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_111 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_109, L_110, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_112 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_108, L_111, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_113 = Decimal_op_UnaryNegation_mFC91CB75C3905B5CD2847152B45B28E85F4D4884(L_112, /*hidden argument*/NULL);
			V_20 = L_113;
			// var z = +((plane1b * ad3) + (plane2b * ad1) + (plane3b * ad2));
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_114 = V_1;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_115 = V_18;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_116 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_114, L_115, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_117 = V_5;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_118 = V_16;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_119 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_117, L_118, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_120 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_116, L_119, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_121 = V_9;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_122 = V_17;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_123 = Decimal_op_Multiply_m115B4FBD28412B58E1911D92A895D7E5C39C2F08(L_121, L_122, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_124 = Decimal_op_Addition_m9DCE5083B3B1FA372C6F72BD91FABC1FA5B76981(L_120, L_123, /*hidden argument*/NULL);
			V_21 = L_124;
			// x /= w;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_125 = V_19;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_126 = V_15;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_127 = Decimal_op_Division_m5A14CCDBC929DEB14F9AC195C7456DF0AE76F514(L_125, L_126, /*hidden argument*/NULL);
			V_19 = L_127;
			// y /= w;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_128 = V_20;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_129 = V_15;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_130 = Decimal_op_Division_m5A14CCDBC929DEB14F9AC195C7456DF0AE76F514(L_128, L_129, /*hidden argument*/NULL);
			V_20 = L_130;
			// z /= w;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_131 = V_21;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_132 = V_15;
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_133 = Decimal_op_Division_m5A14CCDBC929DEB14F9AC195C7456DF0AE76F514(L_131, L_132, /*hidden argument*/NULL);
			V_21 = L_133;
			// var result = new Vector3( (float)x, (float)y, (float)z);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_134 = V_19;
			float L_135 = Decimal_op_Explicit_mC7ED730AE7C6D42F19F06246B242E8B60EDDAC62(L_134, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_136 = V_20;
			float L_137 = Decimal_op_Explicit_mC7ED730AE7C6D42F19F06246B242E8B60EDDAC62(L_136, /*hidden argument*/NULL);
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  L_138 = V_21;
			float L_139 = Decimal_op_Explicit_mC7ED730AE7C6D42F19F06246B242E8B60EDDAC62(L_138, /*hidden argument*/NULL);
			Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_22), (((float)((float)L_135))), (((float)((float)L_137))), (((float)((float)L_139))), /*hidden argument*/NULL);
			// if (float.IsNaN(result.x) || float.IsInfinity(result.x) ||
			//     float.IsNaN(result.y) || float.IsInfinity(result.y) ||
			//     float.IsNaN(result.z) || float.IsInfinity(result.z))
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_140 = V_22;
			float L_141 = L_140.get_x_2();
			bool L_142 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_141, /*hidden argument*/NULL);
			if (L_142)
			{
				goto IL_0261;
			}
		}

IL_021b:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_143 = V_22;
			float L_144 = L_143.get_x_2();
			bool L_145 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_144, /*hidden argument*/NULL);
			if (L_145)
			{
				goto IL_0261;
			}
		}

IL_0229:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_146 = V_22;
			float L_147 = L_146.get_y_3();
			bool L_148 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_147, /*hidden argument*/NULL);
			if (L_148)
			{
				goto IL_0261;
			}
		}

IL_0237:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_149 = V_22;
			float L_150 = L_149.get_y_3();
			bool L_151 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_150, /*hidden argument*/NULL);
			if (L_151)
			{
				goto IL_0261;
			}
		}

IL_0245:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_152 = V_22;
			float L_153 = L_152.get_z_4();
			bool L_154 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_153, /*hidden argument*/NULL);
			if (L_154)
			{
				goto IL_0261;
			}
		}

IL_0253:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_155 = V_22;
			float L_156 = L_155.get_z_4();
			bool L_157 = Single_IsInfinity_m811B198540AB538C4FE145F7C0303C4AD772988B(L_156, /*hidden argument*/NULL);
			if (!L_157)
			{
				goto IL_026a;
			}
		}

IL_0261:
		{
			// return MathConstants.NaNVector3;
			IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_158 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_NaNVector3_27();
			V_23 = L_158;
			goto IL_027a;
		}

IL_026a:
		{
			// return result;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_159 = V_22;
			V_23 = L_159;
			goto IL_027a;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0270;
		throw e;
	}

CATCH_0270:
	{ // begin catch(System.Object)
		// catch
		// return MathConstants.NaNVector3;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_160 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_NaNVector3_27();
		V_23 = L_160;
		goto IL_027a;
	} // end catch (depth: 1)

IL_027a:
	{
		// }
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_161 = V_23;
		return L_161;
	}
}
// System.Single RealtimeCSG.Legacy.CSGPlane::Distance(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, const RuntimeMethod* method)
{
	{
		// return
		//     (
		//         (a * point.x) +
		//         (b * point.y) +
		//         (c * point.z) -
		//         (d)
		//     );
		float L_0 = __this->get_a_0();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___point0;
		float L_2 = L_1.get_x_2();
		float L_3 = __this->get_b_1();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___point0;
		float L_5 = L_4.get_y_3();
		float L_6 = __this->get_c_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = ___point0;
		float L_8 = L_7.get_z_4();
		float L_9 = __this->get_d_3();
		return ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), (float)((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)))), (float)((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)))), (float)L_9));
	}
}
IL2CPP_EXTERN_C  float CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC(_thisAdjusted, ___point0, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::Normalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// var magnitude = 1.0f / Mathf.Sqrt((a * a) + (b * b) + (c * c));
		float L_0 = __this->get_a_0();
		float L_1 = __this->get_a_0();
		float L_2 = __this->get_b_1();
		float L_3 = __this->get_b_1();
		float L_4 = __this->get_c_2();
		float L_5 = __this->get_c_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_0, (float)L_1)), (float)((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)))), (float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)))));
		V_0 = ((float)((float)(1.0f)/(float)L_6));
		// a *= magnitude;
		float L_7 = __this->get_a_0();
		float L_8 = V_0;
		__this->set_a_0(((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)));
		// b *= magnitude;
		float L_9 = __this->get_b_1();
		float L_10 = V_0;
		__this->set_b_1(((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)));
		// c *= magnitude;
		float L_11 = __this->get_c_2();
		float L_12 = V_0;
		__this->set_c_2(((float)il2cpp_codegen_multiply((float)L_11, (float)L_12)));
		// d *= magnitude;
		float L_13 = __this->get_d_3();
		float L_14 = V_0;
		__this->set_d_3(((float)il2cpp_codegen_multiply((float)L_13, (float)L_14)));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581(_thisAdjusted, method);
}
// System.Void RealtimeCSG.Legacy.CSGPlane::Transform(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transformation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var ittrans = transformation.inverse.transpose;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_0 = Matrix4x4_get_inverse_mBD3145C0D7977962E18C8B3BF63DD671F7917166((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___transformation0), /*hidden argument*/NULL);
		V_1 = L_0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_1 = Matrix4x4_get_transpose_mD7983388309D5C806DF8E79A76FF0209964C0D02((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&V_1), /*hidden argument*/NULL);
		// var vector = ittrans * new Vector4(this.a, this.b, this.c, -this.d);
		float L_2 = __this->get_a_0();
		float L_3 = __this->get_b_1();
		float L_4 = __this->get_c_2();
		float L_5 = __this->get_d_3();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D((&L_6), L_2, L_3, L_4, ((-L_5)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var);
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_7 = Matrix4x4_op_Multiply_m33683566CAD5B20F7D6D3CCF26166EC93FB39893(L_1, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		// this.a =  vector.x;
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_8 = V_0;
		float L_9 = L_8.get_x_1();
		__this->set_a_0(L_9);
		// this.b =  vector.y;
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_10 = V_0;
		float L_11 = L_10.get_y_2();
		__this->set_b_1(L_11);
		// this.c =  vector.z;
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_12 = V_0;
		float L_13 = L_12.get_z_3();
		__this->set_c_2(L_13);
		// this.d = -vector.w;
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_14 = V_0;
		float L_15 = L_14.get_w_4();
		__this->set_d_3(((-L_15)));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB_AdjustorThunk (RuntimeObject * __this, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transformation0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB(_thisAdjusted, ___transformation0, method);
}
// RealtimeCSG.Legacy.CSGPlane RealtimeCSG.Legacy.CSGPlane::Negated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  CSGPlane_Negated_m197727B4586DC28A9A753E82FFE636D05DB5F9E5 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method)
{
	{
		// public CSGPlane Negated() { return new CSGPlane(-a, -b, -c, -d); }
		float L_0 = __this->get_a_0();
		float L_1 = __this->get_b_1();
		float L_2 = __this->get_c_2();
		float L_3 = __this->get_d_3();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_4;
		memset((&L_4), 0, sizeof(L_4));
		CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402((&L_4), ((-L_0)), ((-L_1)), ((-L_2)), ((-L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  CSGPlane_Negated_m197727B4586DC28A9A753E82FFE636D05DB5F9E5_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_Negated_m197727B4586DC28A9A753E82FFE636D05DB5F9E5(_thisAdjusted, method);
}
// RealtimeCSG.Legacy.CSGPlane RealtimeCSG.Legacy.CSGPlane::Translated(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  CSGPlane_Translated_mAB1A8898F2402DAF9F25A44F2A503566C240443D (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___translation0, const RuntimeMethod* method)
{
	{
		// return new CSGPlane(a, b, c,
		//     // translated offset = Normal.Dotproduct(translation)
		//     // normal = A,B,C
		//                     d + (a * translation.x) +
		//                         (b * translation.y) +
		//                         (c * translation.z));
		float L_0 = __this->get_a_0();
		float L_1 = __this->get_b_1();
		float L_2 = __this->get_c_2();
		float L_3 = __this->get_d_3();
		float L_4 = __this->get_a_0();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = ___translation0;
		float L_6 = L_5.get_x_2();
		float L_7 = __this->get_b_1();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___translation0;
		float L_9 = L_8.get_y_3();
		float L_10 = __this->get_c_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = ___translation0;
		float L_12 = L_11.get_z_4();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_13;
		memset((&L_13), 0, sizeof(L_13));
		CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402((&L_13), L_0, L_1, L_2, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_6)))), (float)((float)il2cpp_codegen_multiply((float)L_7, (float)L_9)))), (float)((float)il2cpp_codegen_multiply((float)L_10, (float)L_12)))), /*hidden argument*/NULL);
		return L_13;
	}
}
IL2CPP_EXTERN_C  CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  CSGPlane_Translated_mAB1A8898F2402DAF9F25A44F2A503566C240443D_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___translation0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_Translated_mAB1A8898F2402DAF9F25A44F2A503566C240443D(_thisAdjusted, ___translation0, method);
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::Project(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_Project_m579D50E710FBC39A748ABEE2AAFCC2C6E9241B94 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	{
		// float px = point.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___point0;
		float L_1 = L_0.get_x_2();
		// float py = point.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___point0;
		float L_3 = L_2.get_y_3();
		V_0 = L_3;
		// float pz = point.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___point0;
		float L_5 = L_4.get_z_4();
		V_1 = L_5;
		// float nx = normal.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, /*hidden argument*/NULL);
		float L_7 = L_6.get_x_2();
		V_2 = L_7;
		// float ny = normal.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		V_3 = L_9;
		// float nz = normal.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, /*hidden argument*/NULL);
		float L_11 = L_10.get_z_4();
		V_4 = L_11;
		// float ax  = (px - (nx * d)) * nx;
		float L_12 = L_1;
		float L_13 = V_2;
		float L_14 = __this->get_d_3();
		float L_15 = V_2;
		// float ay  = (py - (ny * d)) * ny;
		float L_16 = V_0;
		float L_17 = V_3;
		float L_18 = __this->get_d_3();
		float L_19 = V_3;
		V_5 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_16, (float)((float)il2cpp_codegen_multiply((float)L_17, (float)L_18)))), (float)L_19));
		// float az  = (pz - (nz * d)) * nz;
		float L_20 = V_1;
		float L_21 = V_4;
		float L_22 = __this->get_d_3();
		float L_23 = V_4;
		V_6 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_20, (float)((float)il2cpp_codegen_multiply((float)L_21, (float)L_22)))), (float)L_23));
		// float dot = ax + ay + az;
		float L_24 = V_5;
		float L_25 = V_6;
		V_7 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_14)))), (float)L_15)), (float)L_24)), (float)L_25));
		// float rx = px - (dot * nx);
		float L_26 = V_7;
		float L_27 = V_2;
		// float ry = py - (dot * ny);
		float L_28 = V_0;
		float L_29 = V_7;
		float L_30 = V_3;
		V_8 = ((float)il2cpp_codegen_subtract((float)L_28, (float)((float)il2cpp_codegen_multiply((float)L_29, (float)L_30))));
		// float rz = pz - (dot * nz);
		float L_31 = V_1;
		float L_32 = V_7;
		float L_33 = V_4;
		V_9 = ((float)il2cpp_codegen_subtract((float)L_31, (float)((float)il2cpp_codegen_multiply((float)L_32, (float)L_33))));
		// return new Vector3(rx, ry, rz);
		float L_34 = V_8;
		float L_35 = V_9;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_36;
		memset((&L_36), 0, sizeof(L_36));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_36), ((float)il2cpp_codegen_subtract((float)L_12, (float)((float)il2cpp_codegen_multiply((float)L_26, (float)L_27)))), L_34, L_35, /*hidden argument*/NULL);
		return L_36;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CSGPlane_Project_m579D50E710FBC39A748ABEE2AAFCC2C6E9241B94_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_Project_m579D50E710FBC39A748ABEE2AAFCC2C6E9241B94(_thisAdjusted, ___point0, method);
}
// System.Int32 RealtimeCSG.Legacy.CSGPlane::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CSGPlane_GetHashCode_mA53E0BF2F7FDAA715588FBA59C4C49E6DFB1E33B (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method)
{
	{
		// return a.GetHashCode() ^
		//         b.GetHashCode() ^
		//         c.GetHashCode() ^
		//         d.GetHashCode();
		float* L_0 = __this->get_address_of_a_0();
		int32_t L_1 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_b_1();
		int32_t L_3 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_c_2();
		int32_t L_5 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_d_3();
		int32_t L_7 = Single_GetHashCode_m1BC0733E0C3851ED9D1B6C9C0B243BB88BE77AD0((float*)L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_3))^(int32_t)L_5))^(int32_t)L_7));
	}
}
IL2CPP_EXTERN_C  int32_t CSGPlane_GetHashCode_mA53E0BF2F7FDAA715588FBA59C4C49E6DFB1E33B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_GetHashCode_mA53E0BF2F7FDAA715588FBA59C4C49E6DFB1E33B(_thisAdjusted, method);
}
// System.Boolean RealtimeCSG.Legacy.CSGPlane::Equals(RealtimeCSG.Legacy.CSGPlane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (System.Object.ReferenceEquals(this, other))
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_0 = (*(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this);
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_1 = L_0;
		RuntimeObject * L_2 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_1);
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_3 = ___other0;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_4 = L_3;
		RuntimeObject * L_5 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_4);
		if ((!(((RuntimeObject*)(RuntimeObject *)L_2) == ((RuntimeObject*)(RuntimeObject *)L_5))))
		{
			goto IL_0015;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0015:
	{
		// if (System.Object.ReferenceEquals(other, null))
		goto IL_001f;
	}
	{
		// return false;
		return (bool)0;
	}

IL_001f:
	{
		// return    Mathf.Abs(this.Distance(other.pointOnPlane)) <= MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(other.Distance(this.pointOnPlane)) <= MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(a - other.a) <= MathConstants.NormalEpsilon &&
		//         Mathf.Abs(b - other.b) <= MathConstants.NormalEpsilon &&
		//         Mathf.Abs(c - other.c) <= MathConstants.NormalEpsilon;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___other0), /*hidden argument*/NULL);
		float L_8 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_9 = fabsf(L_8);
		if ((!(((float)L_9) <= ((float)(0.00015f)))))
		{
			goto IL_00a0;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, /*hidden argument*/NULL);
		float L_11 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___other0), L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_12 = fabsf(L_11);
		if ((!(((float)L_12) <= ((float)(0.00015f)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_13 = __this->get_a_0();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_14 = ___other0;
		float L_15 = L_14.get_a_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_16 = fabsf(((float)il2cpp_codegen_subtract((float)L_13, (float)L_15)));
		if ((!(((float)L_16) <= ((float)(0.0001f)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_17 = __this->get_b_1();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_18 = ___other0;
		float L_19 = L_18.get_b_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_20 = fabsf(((float)il2cpp_codegen_subtract((float)L_17, (float)L_19)));
		if ((!(((float)L_20) <= ((float)(0.0001f)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_21 = __this->get_c_2();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_22 = ___other0;
		float L_23 = L_22.get_c_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_24 = fabsf(((float)il2cpp_codegen_subtract((float)L_21, (float)L_23)));
		return (bool)((((int32_t)((!(((float)L_24) <= ((float)(0.0001f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00a0:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3_AdjustorThunk (RuntimeObject * __this, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3(_thisAdjusted, ___other0, method);
}
// System.Boolean RealtimeCSG.Legacy.CSGPlane::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (System.Object.ReferenceEquals(this, obj))
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_0 = (*(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this);
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_1 = L_0;
		RuntimeObject * L_2 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_1);
		RuntimeObject * L_3 = ___obj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_2) == ((RuntimeObject*)(RuntimeObject *)L_3))))
		{
			goto IL_0010;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0010:
	{
		// if (!(obj is CSGPlane))
		RuntimeObject * L_4 = ___obj0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_4, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_001a:
	{
		// CSGPlane other = (CSGPlane)obj;
		RuntimeObject * L_5 = ___obj0;
		V_0 = ((*(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)UnBox(L_5, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var))));
		// if (System.Object.ReferenceEquals(other, null))
		goto IL_002b;
	}
	{
		// return false;
		return (bool)0;
	}

IL_002b:
	{
		// return    Mathf.Abs(this.Distance(other.pointOnPlane)) <= MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(other.Distance(this.pointOnPlane)) <= MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(a - other.a) <= MathConstants.NormalEpsilon &&
		//         Mathf.Abs(b - other.b) <= MathConstants.NormalEpsilon &&
		//         Mathf.Abs(c - other.c) <= MathConstants.NormalEpsilon;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&V_0), /*hidden argument*/NULL);
		float L_8 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_9 = fabsf(L_8);
		if ((!(((float)L_9) <= ((float)(0.00015f)))))
		{
			goto IL_00ac;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)__this, /*hidden argument*/NULL);
		float L_11 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&V_0), L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_12 = fabsf(L_11);
		if ((!(((float)L_12) <= ((float)(0.00015f)))))
		{
			goto IL_00ac;
		}
	}
	{
		float L_13 = __this->get_a_0();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_14 = V_0;
		float L_15 = L_14.get_a_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_16 = fabsf(((float)il2cpp_codegen_subtract((float)L_13, (float)L_15)));
		if ((!(((float)L_16) <= ((float)(0.0001f)))))
		{
			goto IL_00ac;
		}
	}
	{
		float L_17 = __this->get_b_1();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_18 = V_0;
		float L_19 = L_18.get_b_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_20 = fabsf(((float)il2cpp_codegen_subtract((float)L_17, (float)L_19)));
		if ((!(((float)L_20) <= ((float)(0.0001f)))))
		{
			goto IL_00ac;
		}
	}
	{
		float L_21 = __this->get_c_2();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_22 = V_0;
		float L_23 = L_22.get_c_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_24 = fabsf(((float)il2cpp_codegen_subtract((float)L_21, (float)L_23)));
		return (bool)((((int32_t)((!(((float)L_24) <= ((float)(0.0001f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00ac:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D(_thisAdjusted, ___obj0, method);
}
// System.Boolean RealtimeCSG.Legacy.CSGPlane::op_Equality(RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_op_Equality_m5DDC85F3465F45DB2045D95DAC2CCE87D4DD1E00 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___left0, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___right1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_op_Equality_m5DDC85F3465F45DB2045D95DAC2CCE87D4DD1E00_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (System.Object.ReferenceEquals(left, right))
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_0 = ___left0;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_1 = L_0;
		RuntimeObject * L_2 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_1);
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_3 = ___right1;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_4 = L_3;
		RuntimeObject * L_5 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_4);
		if ((!(((RuntimeObject*)(RuntimeObject *)L_2) == ((RuntimeObject*)(RuntimeObject *)L_5))))
		{
			goto IL_0010;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0010:
	{
		// if (System.Object.ReferenceEquals(left, null) ||
		//     System.Object.ReferenceEquals(right, null))
	}
	{
		goto IL_0022;
	}

IL_0020:
	{
		// return false;
		return (bool)0;
	}

IL_0022:
	{
		// return    Mathf.Abs(left.Distance(right.pointOnPlane)) <= MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(right.Distance(left.pointOnPlane)) <= MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(left.a - right.a) <= MathConstants.NormalEpsilon &&
		//         Mathf.Abs(left.b - right.b) <= MathConstants.NormalEpsilon &&
		//         Mathf.Abs(left.c - right.c) <= MathConstants.NormalEpsilon;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___right1), /*hidden argument*/NULL);
		float L_9 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___left0), L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_10 = fabsf(L_9);
		if ((!(((float)L_10) <= ((float)(0.00015f)))))
		{
			goto IL_00a5;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___left0), /*hidden argument*/NULL);
		float L_12 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___right1), L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_13 = fabsf(L_12);
		if ((!(((float)L_13) <= ((float)(0.00015f)))))
		{
			goto IL_00a5;
		}
	}
	{
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_14 = ___left0;
		float L_15 = L_14.get_a_0();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_16 = ___right1;
		float L_17 = L_16.get_a_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_18 = fabsf(((float)il2cpp_codegen_subtract((float)L_15, (float)L_17)));
		if ((!(((float)L_18) <= ((float)(0.0001f)))))
		{
			goto IL_00a5;
		}
	}
	{
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_19 = ___left0;
		float L_20 = L_19.get_b_1();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_21 = ___right1;
		float L_22 = L_21.get_b_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_23 = fabsf(((float)il2cpp_codegen_subtract((float)L_20, (float)L_22)));
		if ((!(((float)L_23) <= ((float)(0.0001f)))))
		{
			goto IL_00a5;
		}
	}
	{
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_24 = ___left0;
		float L_25 = L_24.get_c_2();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_26 = ___right1;
		float L_27 = L_26.get_c_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_28 = fabsf(((float)il2cpp_codegen_subtract((float)L_25, (float)L_27)));
		return (bool)((((int32_t)((!(((float)L_28) <= ((float)(0.0001f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00a5:
	{
		return (bool)0;
	}
}
// System.Boolean RealtimeCSG.Legacy.CSGPlane::op_Inequality(RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CSGPlane_op_Inequality_m351E7463AB7335D358F0B8A814FFE638F67965B9 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___left0, CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  ___right1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_op_Inequality_m351E7463AB7335D358F0B8A814FFE638F67965B9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (System.Object.ReferenceEquals(left, right))
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_0 = ___left0;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_1 = L_0;
		RuntimeObject * L_2 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_1);
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_3 = ___right1;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_4 = L_3;
		RuntimeObject * L_5 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_4);
		if ((!(((RuntimeObject*)(RuntimeObject *)L_2) == ((RuntimeObject*)(RuntimeObject *)L_5))))
		{
			goto IL_0010;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0010:
	{
		// if (System.Object.ReferenceEquals(left, null) ||
		//     System.Object.ReferenceEquals(right, null))
	}
	{
		goto IL_0022;
	}

IL_0020:
	{
		// return true;
		return (bool)1;
	}

IL_0022:
	{
		// return    Mathf.Abs(left.Distance(right.pointOnPlane)) > MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(right.Distance(left.pointOnPlane)) > MathConstants.DistanceEpsilon &&
		//         Mathf.Abs(left.a - right.a) > MathConstants.NormalEpsilon ||
		//         Mathf.Abs(left.b - right.b) > MathConstants.NormalEpsilon ||
		//         Mathf.Abs(left.c - right.c) > MathConstants.NormalEpsilon;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___right1), /*hidden argument*/NULL);
		float L_9 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___left0), L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_10 = fabsf(L_9);
		if ((!(((float)L_10) > ((float)(0.00015f)))))
		{
			goto IL_006f;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___left0), /*hidden argument*/NULL);
		float L_12 = CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)(&___right1), L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_13 = fabsf(L_12);
		if ((!(((float)L_13) > ((float)(0.00015f)))))
		{
			goto IL_006f;
		}
	}
	{
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_14 = ___left0;
		float L_15 = L_14.get_a_0();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_16 = ___right1;
		float L_17 = L_16.get_a_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_18 = fabsf(((float)il2cpp_codegen_subtract((float)L_15, (float)L_17)));
		if ((((float)L_18) > ((float)(0.0001f))))
		{
			goto IL_00a2;
		}
	}

IL_006f:
	{
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_19 = ___left0;
		float L_20 = L_19.get_b_1();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_21 = ___right1;
		float L_22 = L_21.get_b_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_23 = fabsf(((float)il2cpp_codegen_subtract((float)L_20, (float)L_22)));
		if ((((float)L_23) > ((float)(0.0001f))))
		{
			goto IL_00a2;
		}
	}
	{
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_24 = ___left0;
		float L_25 = L_24.get_c_2();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_26 = ___right1;
		float L_27 = L_26.get_c_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_28 = fabsf(((float)il2cpp_codegen_subtract((float)L_25, (float)L_27)));
		return (bool)((((float)L_28) > ((float)(0.0001f)))? 1 : 0);
	}

IL_00a2:
	{
		return (bool)1;
	}
}
// System.String RealtimeCSG.Legacy.CSGPlane::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8 (CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() { return string.Format(CultureInfo.InvariantCulture, "({0}, {1}, {2}, {3})", a,b,c,d); }
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_0 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		float L_3 = __this->get_a_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_2;
		float L_7 = __this->get_b_1();
		float L_8 = L_7;
		RuntimeObject * L_9 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_9);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_6;
		float L_11 = __this->get_c_2();
		float L_12 = L_11;
		RuntimeObject * L_13 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_13);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_14 = L_10;
		float L_15 = __this->get_d_3();
		float L_16 = L_15;
		RuntimeObject * L_17 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_17);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_17);
		String_t* L_18 = String_Format_mF68EE0DEC1AA5ADE9DFEF9AE0508E428FBB10EFD(L_0, _stringLiteralEB047BACA03D70516231AF895D077E14DE1E26F6, L_14, /*hidden argument*/NULL);
		return L_18;
	}
}
IL2CPP_EXTERN_C  String_t* CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * _thisAdjusted = reinterpret_cast<CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *>(__this + _offset);
	return CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RealtimeCSG.Legacy.ControlMesh::get_Valid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ControlMesh_get_Valid_m271463949CA9305A922B043B3678B1D5951014DC (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, const RuntimeMethod* method)
{
	{
		// public bool            Valid { get { return valid; } set { valid = value; } }
		bool L_0 = __this->get_valid_3();
		return L_0;
	}
}
// System.Void RealtimeCSG.Legacy.ControlMesh::set_Valid(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh_set_Valid_m3902886AF73159E55499A3FAE5C8C77FE3BE06FC (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool            Valid { get { return valid; } set { valid = value; } }
		bool L_0 = ___value0;
		__this->set_valid_3(L_0);
		// public bool            Valid { get { return valid; } set { valid = value; } }
		return;
	}
}
// System.Void RealtimeCSG.Legacy.ControlMesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh__ctor_m93DFAB3F904281143760B4713D2A2AE89453E3E4 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, const RuntimeMethod* method)
{
	{
		// public ControlMesh() { }
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// public ControlMesh() { }
		return;
	}
}
// System.Void RealtimeCSG.Legacy.ControlMesh::.ctor(RealtimeCSG.Legacy.ControlMesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh__ctor_m12DF3940EE5E54F6E41903867A37567A0D4743B4 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * ___other0, const RuntimeMethod* method)
{
	{
		// public ControlMesh(ControlMesh other) { CopyFrom(other); }
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// public ControlMesh(ControlMesh other) { CopyFrom(other); }
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_0 = ___other0;
		ControlMesh_CopyFrom_m010324448CDA71EAC8776694628A7307CE186C42(__this, L_0, /*hidden argument*/NULL);
		// public ControlMesh(ControlMesh other) { CopyFrom(other); }
		return;
	}
}
// System.Void RealtimeCSG.Legacy.ControlMesh::SetDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh_SetDirty_mE2DD5AB15A4B2717E50ADF05196B50C5995AA093 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, const RuntimeMethod* method)
{
	{
		// public void SetDirty() { Generation++; }
		int32_t L_0 = __this->get_Generation_4();
		__this->set_Generation_4(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		// public void SetDirty() { Generation++; }
		return;
	}
}
// System.Void RealtimeCSG.Legacy.ControlMesh::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh_Reset_m70A47B47DED0106F851EF950DDCA2FAF497800F1 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, const RuntimeMethod* method)
{
	{
		// Vertices = null;
		__this->set_Vertices_0((Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)NULL);
		// Edges = null;
		__this->set_Edges_1((HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436*)NULL);
		// Polygons = null;
		__this->set_Polygons_2((PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513*)NULL);
		// valid = false;
		__this->set_valid_3((bool)0);
		// Generation =  0;
		__this->set_Generation_4(0);
		// }
		return;
	}
}
// System.Void RealtimeCSG.Legacy.ControlMesh::CopyFrom(RealtimeCSG.Legacy.ControlMesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ControlMesh_CopyFrom_m010324448CDA71EAC8776694628A7307CE186C42 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ControlMesh_CopyFrom_m010324448CDA71EAC8776694628A7307CE186C42_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* V_1 = NULL;
	{
		// if (other == null)
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		// Reset();
		ControlMesh_Reset_m70A47B47DED0106F851EF950DDCA2FAF497800F1(__this, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_000a:
	{
		// if (other.Vertices != null)
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_1 = ___other0;
		NullCheck(L_1);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_2 = L_1->get_Vertices_0();
		if (!L_2)
		{
			goto IL_005a;
		}
	}
	{
		// if (Vertices == null || Vertices.Length != other.Vertices.Length)
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_3 = __this->get_Vertices_0();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_4 = __this->get_Vertices_0();
		NullCheck(L_4);
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_5 = ___other0;
		NullCheck(L_5);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_6 = L_5->get_Vertices_0();
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length)))))))
		{
			goto IL_003f;
		}
	}

IL_002c:
	{
		// Vertices        = new Vector3[other.Vertices.Length];
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_7 = ___other0;
		NullCheck(L_7);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_8 = L_7->get_Vertices_0();
		NullCheck(L_8);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_9 = (Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)SZArrayNew(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))));
		__this->set_Vertices_0(L_9);
	}

IL_003f:
	{
		// Array.Copy(other.Vertices, Vertices, other.Vertices.Length);
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_10 = ___other0;
		NullCheck(L_10);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_11 = L_10->get_Vertices_0();
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_12 = __this->get_Vertices_0();
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_13 = ___other0;
		NullCheck(L_13);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_14 = L_13->get_Vertices_0();
		NullCheck(L_14);
		Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434((RuntimeArray *)(RuntimeArray *)L_11, (RuntimeArray *)(RuntimeArray *)L_12, (((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))), /*hidden argument*/NULL);
		// } else
		goto IL_0061;
	}

IL_005a:
	{
		// Vertices = null;
		__this->set_Vertices_0((Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)NULL);
	}

IL_0061:
	{
		// if (other.Edges != null)
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_15 = ___other0;
		NullCheck(L_15);
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_16 = L_15->get_Edges_1();
		if (!L_16)
		{
			goto IL_00b1;
		}
	}
	{
		// if (Edges == null || Edges.Length != other.Edges.Length)
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_17 = __this->get_Edges_1();
		if (!L_17)
		{
			goto IL_0083;
		}
	}
	{
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_18 = __this->get_Edges_1();
		NullCheck(L_18);
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_19 = ___other0;
		NullCheck(L_19);
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_20 = L_19->get_Edges_1();
		NullCheck(L_20);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))))
		{
			goto IL_0096;
		}
	}

IL_0083:
	{
		// Edges        = new HalfEdge[other.Edges.Length];
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_21 = ___other0;
		NullCheck(L_21);
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_22 = L_21->get_Edges_1();
		NullCheck(L_22);
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_23 = (HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436*)(HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436*)SZArrayNew(HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length)))));
		__this->set_Edges_1(L_23);
	}

IL_0096:
	{
		// Array.Copy(other.Edges, Edges, other.Edges.Length);
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_24 = ___other0;
		NullCheck(L_24);
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_25 = L_24->get_Edges_1();
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_26 = __this->get_Edges_1();
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_27 = ___other0;
		NullCheck(L_27);
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_28 = L_27->get_Edges_1();
		NullCheck(L_28);
		Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434((RuntimeArray *)(RuntimeArray *)L_25, (RuntimeArray *)(RuntimeArray *)L_26, (((int32_t)((int32_t)(((RuntimeArray*)L_28)->max_length)))), /*hidden argument*/NULL);
		// } else
		goto IL_00b8;
	}

IL_00b1:
	{
		// Edges = null;
		__this->set_Edges_1((HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436*)NULL);
	}

IL_00b8:
	{
		// if (other.Polygons != null)
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_29 = ___other0;
		NullCheck(L_29);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_30 = L_29->get_Polygons_2();
		if (!L_30)
		{
			goto IL_0176;
		}
	}
	{
		// if (Polygons == null || Polygons.Length != other.Polygons.Length)
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_31 = __this->get_Polygons_2();
		if (!L_31)
		{
			goto IL_00dd;
		}
	}
	{
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_32 = __this->get_Polygons_2();
		NullCheck(L_32);
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_33 = ___other0;
		NullCheck(L_33);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_34 = L_33->get_Polygons_2();
		NullCheck(L_34);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_32)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_34)->max_length)))))))
		{
			goto IL_00f0;
		}
	}

IL_00dd:
	{
		// Polygons = new Polygon[other.Polygons.Length];
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_35 = ___other0;
		NullCheck(L_35);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_36 = L_35->get_Polygons_2();
		NullCheck(L_36);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_37 = (PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513*)(PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513*)SZArrayNew(PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_36)->max_length)))));
		__this->set_Polygons_2(L_37);
	}

IL_00f0:
	{
		// for (var p = 0; p < other.Polygons.Length; p++)
		V_0 = 0;
		goto IL_0169;
	}

IL_00f4:
	{
		// if (other.Polygons[p].EdgeIndices == null ||
		//     other.Polygons[p].EdgeIndices.Length == 0)
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_38 = ___other0;
		NullCheck(L_38);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_39 = L_38->get_Polygons_2();
		int32_t L_40 = V_0;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_42);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_43 = L_42->get_EdgeIndices_0();
		if (!L_43)
		{
			goto IL_0165;
		}
	}
	{
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_44 = ___other0;
		NullCheck(L_44);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_45 = L_44->get_Polygons_2();
		int32_t L_46 = V_0;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_48);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_49 = L_48->get_EdgeIndices_0();
		NullCheck(L_49);
		if (!(((RuntimeArray*)L_49)->max_length))
		{
			goto IL_0165;
		}
	}
	{
		// var newEdges = new int[other.Polygons[p].EdgeIndices.Length];
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_50 = ___other0;
		NullCheck(L_50);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_51 = L_50->get_Polygons_2();
		int32_t L_52 = V_0;
		NullCheck(L_51);
		int32_t L_53 = L_52;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		NullCheck(L_54);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_55 = L_54->get_EdgeIndices_0();
		NullCheck(L_55);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_56 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_55)->max_length)))));
		V_1 = L_56;
		// Array.Copy(other.Polygons[p].EdgeIndices, newEdges, other.Polygons[p].EdgeIndices.Length);
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_57 = ___other0;
		NullCheck(L_57);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_58 = L_57->get_Polygons_2();
		int32_t L_59 = V_0;
		NullCheck(L_58);
		int32_t L_60 = L_59;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		NullCheck(L_61);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_62 = L_61->get_EdgeIndices_0();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_63 = V_1;
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_64 = ___other0;
		NullCheck(L_64);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_65 = L_64->get_Polygons_2();
		int32_t L_66 = V_0;
		NullCheck(L_65);
		int32_t L_67 = L_66;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		NullCheck(L_68);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_69 = L_68->get_EdgeIndices_0();
		NullCheck(L_69);
		Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434((RuntimeArray *)(RuntimeArray *)L_62, (RuntimeArray *)(RuntimeArray *)L_63, (((int32_t)((int32_t)(((RuntimeArray*)L_69)->max_length)))), /*hidden argument*/NULL);
		// Polygons[p] = new Polygon(newEdges, other.Polygons[p].TexGenIndex);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_70 = __this->get_Polygons_2();
		int32_t L_71 = V_0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_72 = V_1;
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_73 = ___other0;
		NullCheck(L_73);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_74 = L_73->get_Polygons_2();
		int32_t L_75 = V_0;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		NullCheck(L_77);
		int32_t L_78 = L_77->get_TexGenIndex_1();
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_79 = (Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 *)il2cpp_codegen_object_new(Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19_il2cpp_TypeInfo_var);
		Polygon__ctor_m2E15F7F95331FC3DB64EE581891D0F1746349511(L_79, L_72, L_78, /*hidden argument*/NULL);
		NullCheck(L_70);
		ArrayElementTypeCheck (L_70, L_79);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(L_71), (Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 *)L_79);
	}

IL_0165:
	{
		// for (var p = 0; p < other.Polygons.Length; p++)
		int32_t L_80 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_80, (int32_t)1));
	}

IL_0169:
	{
		// for (var p = 0; p < other.Polygons.Length; p++)
		int32_t L_81 = V_0;
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_82 = ___other0;
		NullCheck(L_82);
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_83 = L_82->get_Polygons_2();
		NullCheck(L_83);
		if ((((int32_t)L_81) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_83)->max_length)))))))
		{
			goto IL_00f4;
		}
	}
	{
		// } else
		goto IL_017d;
	}

IL_0176:
	{
		// Polygons = null;
		__this->set_Polygons_2((PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513*)NULL);
	}

IL_017d:
	{
		// valid = other.valid;
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_84 = ___other0;
		NullCheck(L_84);
		bool L_85 = L_84->get_valid_3();
		__this->set_valid_3(L_85);
		// Generation = other.Generation;
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_86 = ___other0;
		NullCheck(L_86);
		int32_t L_87 = L_86->get_Generation_4();
		__this->set_Generation_4(L_87);
		// }
		return;
	}
}
// RealtimeCSG.Legacy.ControlMesh RealtimeCSG.Legacy.ControlMesh::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * ControlMesh_Clone_m22DA0E5DE55549B13B5E4B2FB235A9764CC1E912 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ControlMesh_Clone_m22DA0E5DE55549B13B5E4B2FB235A9764CC1E912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public ControlMesh Clone() { return new ControlMesh(this); }
		ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * L_0 = (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 *)il2cpp_codegen_object_new(ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0_il2cpp_TypeInfo_var);
		ControlMesh__ctor_m12DF3940EE5E54F6E41903867A37567A0D4743B4(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetVertex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ControlMesh_GetVertex_mE0F471B92EB47F38895C9D97A12F066B885F9662 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ControlMesh_GetVertex_mE0F471B92EB47F38895C9D97A12F066B885F9662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int16_t V_0 = 0;
	{
		// if (halfEdgeIndex < 0 || halfEdgeIndex >= Edges.Length)
		int32_t L_0 = ___halfEdgeIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___halfEdgeIndex0;
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_2 = __this->get_Edges_1();
		NullCheck(L_2);
		if ((((int32_t)L_1) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length)))))))
		{
			goto IL_0015;
		}
	}

IL_000f:
	{
		// return MathConstants.zeroVector3;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		return L_3;
	}

IL_0015:
	{
		// var vertexIndex = Edges[halfEdgeIndex].VertexIndex;
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_4 = __this->get_Edges_1();
		int32_t L_5 = ___halfEdgeIndex0;
		NullCheck(L_4);
		int16_t L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_VertexIndex_3();
		V_0 = L_6;
		// if (vertexIndex < 0 || vertexIndex >= Vertices.Length)
		int16_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int16_t L_8 = V_0;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_9 = __this->get_Vertices_0();
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))))
		{
			goto IL_003c;
		}
	}

IL_0036:
	{
		// return MathConstants.zeroVector3;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		return L_10;
	}

IL_003c:
	{
		// return Vertices[vertexIndex];
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_11 = __this->get_Vertices_0();
		int16_t L_12 = V_0;
		NullCheck(L_11);
		int16_t L_13 = L_12;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		return L_14;
	}
}
// UnityEngine.Vector3[] RealtimeCSG.Legacy.ControlMesh::GetVertices(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ControlMesh_GetVertices_mD78ABF872903C01463776ADD972AC9667D4EB3FB (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___halfEdgeIndices0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ControlMesh_GetVertices_mD78ABF872903C01463776ADD972AC9667D4EB3FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int16_t V_3 = 0;
	{
		// var vertices = new Vector3[halfEdgeIndices.Length];
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_0 = ___halfEdgeIndices0;
		NullCheck(L_0);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_1 = (Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)SZArrayNew(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)))));
		V_0 = L_1;
		// for (var i = 0; i < halfEdgeIndices.Length; i++)
		V_1 = 0;
		goto IL_0074;
	}

IL_000d:
	{
		// var halfEdgeIndex = halfEdgeIndices[i];
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_2 = ___halfEdgeIndices0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
		// if (halfEdgeIndex < 0 || halfEdgeIndex >= Edges.Length)
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_7 = V_2;
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_8 = __this->get_Edges_1();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))))
		{
			goto IL_002e;
		}
	}

IL_0020:
	{
		// vertices[i] = MathConstants.zeroVector3;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_9 = V_0;
		int32_t L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_11);
		// continue;
		goto IL_0070;
	}

IL_002e:
	{
		// var vertexIndex = Edges[halfEdgeIndex].VertexIndex;
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_12 = __this->get_Edges_1();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int16_t L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_VertexIndex_3();
		V_3 = L_14;
		// if (vertexIndex < 0 || vertexIndex >= Vertices.Length)
		int16_t L_15 = V_3;
		if ((((int32_t)L_15) < ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int16_t L_16 = V_3;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_17 = __this->get_Vertices_0();
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))))
		{
			goto IL_005d;
		}
	}

IL_004f:
	{
		// vertices[i] = MathConstants.zeroVector3;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_18 = V_0;
		int32_t L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_20);
		// continue;
		goto IL_0070;
	}

IL_005d:
	{
		// vertices[i] = Vertices[vertexIndex];
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_21 = V_0;
		int32_t L_22 = V_1;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_23 = __this->get_Vertices_0();
		int16_t L_24 = V_3;
		NullCheck(L_23);
		int16_t L_25 = L_24;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_26);
	}

IL_0070:
	{
		// for (var i = 0; i < halfEdgeIndices.Length; i++)
		int32_t L_27 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_0074:
	{
		// for (var i = 0; i < halfEdgeIndices.Length; i++)
		int32_t L_28 = V_1;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_29 = ___halfEdgeIndices0;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_29)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		// return vertices;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_30 = V_0;
		return L_30;
	}
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetVertex(RealtimeCSG.Legacy.HalfEdge&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ControlMesh_GetVertex_m30F61A0C56229557886C7B4CF27481530A929D43 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * ___halfEdge0, const RuntimeMethod* method)
{
	{
		// public Vector3    GetVertex                (ref HalfEdge halfEdge)        { return Vertices[halfEdge.VertexIndex]; }
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_0 = __this->get_Vertices_0();
		HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * L_1 = ___halfEdge0;
		int16_t L_2 = L_1->get_VertexIndex_3();
		NullCheck(L_0);
		int16_t L_3 = L_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		return L_4;
	}
}
// System.Int16 RealtimeCSG.Legacy.ControlMesh::GetVertexIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t ControlMesh_GetVertexIndex_mDFAB5A9F8DD849F862D5899498E929F6334949FB (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	{
		// public short    GetVertexIndex            (int halfEdgeIndex)            { return Edges[halfEdgeIndex].VertexIndex; }
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		int32_t L_1 = ___halfEdgeIndex0;
		NullCheck(L_0);
		int16_t L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_VertexIndex_3();
		return L_2;
	}
}
// System.Int16 RealtimeCSG.Legacy.ControlMesh::GetVertexIndex(RealtimeCSG.Legacy.HalfEdge&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t ControlMesh_GetVertexIndex_mC4DC0CC7D01599B06B5F8AF0FFB5EFB3E453B628 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * ___halfEdge0, const RuntimeMethod* method)
{
	{
		// public short    GetVertexIndex            (ref HalfEdge halfEdge)        { return halfEdge.VertexIndex; }
		HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * L_0 = ___halfEdge0;
		int16_t L_1 = L_0->get_VertexIndex_3();
		return L_1;
	}
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertex(RealtimeCSG.Legacy.HalfEdge&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ControlMesh_GetTwinEdgeVertex_mC68325F10BC9FC6E6E2DB022451671DAD33F33C4 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * ___halfEdge0, const RuntimeMethod* method)
{
	{
		// public Vector3    GetTwinEdgeVertex        (ref HalfEdge halfEdge)        { return Vertices[Edges[halfEdge.TwinIndex].VertexIndex]; }
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_0 = __this->get_Vertices_0();
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_1 = __this->get_Edges_1();
		HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * L_2 = ___halfEdge0;
		int32_t L_3 = L_2->get_TwinIndex_0();
		NullCheck(L_1);
		int16_t L_4 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_VertexIndex_3();
		NullCheck(L_0);
		int16_t L_5 = L_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ControlMesh_GetTwinEdgeVertex_m8AECF1CB123CB39ADCBBCD9AE0833522577DDE85 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	{
		// public Vector3    GetTwinEdgeVertex        (int halfEdgeIndex)            { return Vertices[Edges[Edges[halfEdgeIndex].TwinIndex].VertexIndex]; }
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_0 = __this->get_Vertices_0();
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_1 = __this->get_Edges_1();
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_2 = __this->get_Edges_1();
		int32_t L_3 = ___halfEdgeIndex0;
		NullCheck(L_2);
		int32_t L_4 = ((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_TwinIndex_0();
		NullCheck(L_1);
		int16_t L_5 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))->get_VertexIndex_3();
		NullCheck(L_0);
		int16_t L_6 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		return L_7;
	}
}
// System.Int16 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertexIndex(RealtimeCSG.Legacy.HalfEdge&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t ControlMesh_GetTwinEdgeVertexIndex_m4E5F9B2AEAA2E5BC5FDB7B5DD374167EE366ED14 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * ___halfEdge0, const RuntimeMethod* method)
{
	{
		// public short    GetTwinEdgeVertexIndex    (ref HalfEdge halfEdge)        { return Edges[halfEdge.TwinIndex].VertexIndex; }
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * L_1 = ___halfEdge0;
		int32_t L_2 = L_1->get_TwinIndex_0();
		NullCheck(L_0);
		int16_t L_3 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))->get_VertexIndex_3();
		return L_3;
	}
}
// System.Int16 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertexIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t ControlMesh_GetTwinEdgeVertexIndex_m062983016F73A2E02A2E9A626857946E4FD4B027 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	{
		// public short    GetTwinEdgeVertexIndex    (int halfEdgeIndex)            { return Edges[Edges[halfEdgeIndex].TwinIndex].VertexIndex; }
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_1 = __this->get_Edges_1();
		int32_t L_2 = ___halfEdgeIndex0;
		NullCheck(L_1);
		int32_t L_3 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))->get_TwinIndex_0();
		NullCheck(L_0);
		int16_t L_4 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_VertexIndex_3();
		return L_4;
	}
}
// System.Int32 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeIndex(RealtimeCSG.Legacy.HalfEdge&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ControlMesh_GetTwinEdgeIndex_m5F64058A123F28EC0C48F645E4F67057E3AE20C1 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * ___halfEdge0, const RuntimeMethod* method)
{
	{
		// public int        GetTwinEdgeIndex        (ref HalfEdge halfEdge)        { return halfEdge.TwinIndex; }
		HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * L_0 = ___halfEdge0;
		int32_t L_1 = L_0->get_TwinIndex_0();
		return L_1;
	}
}
// System.Int32 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ControlMesh_GetTwinEdgeIndex_mEB16E205B788C2614EFB4CD2EA3A1F62C224B7F3 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	{
		// public int        GetTwinEdgeIndex        (int halfEdgeIndex)            { return Edges[halfEdgeIndex].TwinIndex; }
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		int32_t L_1 = ___halfEdgeIndex0;
		NullCheck(L_0);
		int32_t L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_TwinIndex_0();
		return L_2;
	}
}
// System.Int16 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgePolygonIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t ControlMesh_GetTwinEdgePolygonIndex_m7E8B0F67E65B6D92C4DDBEA3A0EE09341CB953C8 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	{
		// public short    GetTwinEdgePolygonIndex    (int halfEdgeIndex)            { return Edges[Edges[halfEdgeIndex].TwinIndex].PolygonIndex; }
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_1 = __this->get_Edges_1();
		int32_t L_2 = ___halfEdgeIndex0;
		NullCheck(L_1);
		int32_t L_3 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))->get_TwinIndex_0();
		NullCheck(L_0);
		int16_t L_4 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_PolygonIndex_1();
		return L_4;
	}
}
// System.Int16 RealtimeCSG.Legacy.ControlMesh::GetEdgePolygonIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t ControlMesh_GetEdgePolygonIndex_m7A1C2726753E24BA4A0043CC12B68A74E3019AA6 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	{
		// public short    GetEdgePolygonIndex        (int halfEdgeIndex)            { return Edges[halfEdgeIndex].PolygonIndex; }
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		int32_t L_1 = ___halfEdgeIndex0;
		NullCheck(L_0);
		int16_t L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_PolygonIndex_1();
		return L_2;
	}
}
// System.Int32 RealtimeCSG.Legacy.ControlMesh::GetNextEdgeIndexAroundVertex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ControlMesh_GetNextEdgeIndexAroundVertex_m8F5FD766D00A1C04EF36ACBCE3B91E664A096145 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	{
		// public int        GetNextEdgeIndexAroundVertex    (int halfEdgeIndex) { return GetTwinEdgeIndex(GetNextEdgeIndex(halfEdgeIndex)); }
		int32_t L_0 = ___halfEdgeIndex0;
		int32_t L_1 = ControlMesh_GetNextEdgeIndex_m0E2F6BC7E88481CBDE3C3B166BBA15BB94D744D6(__this, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ControlMesh_GetTwinEdgeIndex_mEB16E205B788C2614EFB4CD2EA3A1F62C224B7F3(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 RealtimeCSG.Legacy.ControlMesh::GetPrevEdgeIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ControlMesh_GetPrevEdgeIndex_m90955D2563E0C0B7374C23D1FAC8D31F28E4ED1F (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	int16_t V_0 = 0;
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// var edge    = Edges[halfEdgeIndex];
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		int32_t L_1 = ___halfEdgeIndex0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		// var polygonIndex = edge.PolygonIndex;
		int16_t L_4 = L_3.get_PolygonIndex_1();
		V_0 = L_4;
		// if (polygonIndex < 0 || polygonIndex >= Polygons.Length)
		int16_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		int16_t L_6 = V_0;
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_7 = __this->get_Polygons_2();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))))))
		{
			goto IL_0023;
		}
	}

IL_0021:
	{
		// return -1;
		return (-1);
	}

IL_0023:
	{
		// var edgeIndices = Polygons[polygonIndex].EdgeIndices;
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_8 = __this->get_Polygons_2();
		int16_t L_9 = V_0;
		NullCheck(L_8);
		int16_t L_10 = L_9;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_12 = L_11->get_EdgeIndices_0();
		V_1 = L_12;
		// for (int i = 1; i < edgeIndices.Length; i++)
		V_2 = 1;
		goto IL_0045;
	}

IL_0035:
	{
		// if (edgeIndices[i] == halfEdgeIndex)
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_13 = V_1;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = ___halfEdgeIndex0;
		if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
		{
			goto IL_0041;
		}
	}
	{
		// return edgeIndices[i - 1];
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_18 = V_1;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		return L_21;
	}

IL_0041:
	{
		// for (int i = 1; i < edgeIndices.Length; i++)
		int32_t L_22 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0045:
	{
		// for (int i = 1; i < edgeIndices.Length; i++)
		int32_t L_23 = V_2;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_24 = V_1;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)))))))
		{
			goto IL_0035;
		}
	}
	{
		// return edgeIndices[edgeIndices.Length - 1];
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_25 = V_1;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_26 = V_1;
		NullCheck(L_26);
		NullCheck(L_25);
		int32_t L_27 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_26)->max_length)))), (int32_t)1));
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		return L_28;
	}
}
// System.Int32 RealtimeCSG.Legacy.ControlMesh::GetNextEdgeIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ControlMesh_GetNextEdgeIndex_m0E2F6BC7E88481CBDE3C3B166BBA15BB94D744D6 (ControlMesh_t8E0B7C345924A789BD83CA48951F5C752ADF0FD0 * __this, int32_t ___halfEdgeIndex0, const RuntimeMethod* method)
{
	int16_t V_0 = 0;
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// var edge    = Edges[halfEdgeIndex];
		HalfEdgeU5BU5D_tC7D1E0FC17A4627C60654AA71FADD8A284849436* L_0 = __this->get_Edges_1();
		int32_t L_1 = ___halfEdgeIndex0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		// var polygonIndex = edge.PolygonIndex;
		int16_t L_4 = L_3.get_PolygonIndex_1();
		V_0 = L_4;
		// if (polygonIndex < 0 || polygonIndex >= Polygons.Length)
		int16_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		int16_t L_6 = V_0;
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_7 = __this->get_Polygons_2();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))))))
		{
			goto IL_0023;
		}
	}

IL_0021:
	{
		// return -1;
		return (-1);
	}

IL_0023:
	{
		// var edgeIndices = Polygons[polygonIndex].EdgeIndices;
		PolygonU5BU5D_tBCDE95937F9ED12AE938779965F43BF0371F0513* L_8 = __this->get_Polygons_2();
		int16_t L_9 = V_0;
		NullCheck(L_8);
		int16_t L_10 = L_9;
		Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_12 = L_11->get_EdgeIndices_0();
		V_1 = L_12;
		// for (int i = 0; i < edgeIndices.Length - 1; i++)
		V_2 = 0;
		goto IL_0045;
	}

IL_0035:
	{
		// if (edgeIndices[i] == halfEdgeIndex)
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_13 = V_1;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = ___halfEdgeIndex0;
		if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
		{
			goto IL_0041;
		}
	}
	{
		// return edgeIndices[i + 1];
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_18 = V_1;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		return L_21;
	}

IL_0041:
	{
		// for (int i = 0; i < edgeIndices.Length - 1; i++)
		int32_t L_22 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0045:
	{
		// for (int i = 0; i < edgeIndices.Length - 1; i++)
		int32_t L_23 = V_2;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_24 = V_1;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)))), (int32_t)1)))))
		{
			goto IL_0035;
		}
	}
	{
		// return edgeIndices[0];
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_25 = V_1;
		NullCheck(L_25);
		int32_t L_26 = 0;
		int32_t L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		return L_27;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: RealtimeCSG.Legacy.HalfEdge
IL2CPP_EXTERN_C void HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshal_pinvoke(const HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42& unmarshaled, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_pinvoke& marshaled)
{
	marshaled.___TwinIndex_0 = unmarshaled.get_TwinIndex_0();
	marshaled.___PolygonIndex_1 = unmarshaled.get_PolygonIndex_1();
	marshaled.___HardEdge_2 = static_cast<int32_t>(unmarshaled.get_HardEdge_2());
	marshaled.___VertexIndex_3 = unmarshaled.get_VertexIndex_3();
}
IL2CPP_EXTERN_C void HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshal_pinvoke_back(const HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_pinvoke& marshaled, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42& unmarshaled)
{
	int32_t unmarshaled_TwinIndex_temp_0 = 0;
	unmarshaled_TwinIndex_temp_0 = marshaled.___TwinIndex_0;
	unmarshaled.set_TwinIndex_0(unmarshaled_TwinIndex_temp_0);
	int16_t unmarshaled_PolygonIndex_temp_1 = 0;
	unmarshaled_PolygonIndex_temp_1 = marshaled.___PolygonIndex_1;
	unmarshaled.set_PolygonIndex_1(unmarshaled_PolygonIndex_temp_1);
	bool unmarshaled_HardEdge_temp_2 = false;
	unmarshaled_HardEdge_temp_2 = static_cast<bool>(marshaled.___HardEdge_2);
	unmarshaled.set_HardEdge_2(unmarshaled_HardEdge_temp_2);
	int16_t unmarshaled_VertexIndex_temp_3 = 0;
	unmarshaled_VertexIndex_temp_3 = marshaled.___VertexIndex_3;
	unmarshaled.set_VertexIndex_3(unmarshaled_VertexIndex_temp_3);
}
// Conversion method for clean up from marshalling of: RealtimeCSG.Legacy.HalfEdge
IL2CPP_EXTERN_C void HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshal_pinvoke_cleanup(HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: RealtimeCSG.Legacy.HalfEdge
IL2CPP_EXTERN_C void HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshal_com(const HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42& unmarshaled, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_com& marshaled)
{
	marshaled.___TwinIndex_0 = unmarshaled.get_TwinIndex_0();
	marshaled.___PolygonIndex_1 = unmarshaled.get_PolygonIndex_1();
	marshaled.___HardEdge_2 = static_cast<int32_t>(unmarshaled.get_HardEdge_2());
	marshaled.___VertexIndex_3 = unmarshaled.get_VertexIndex_3();
}
IL2CPP_EXTERN_C void HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshal_com_back(const HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_com& marshaled, HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42& unmarshaled)
{
	int32_t unmarshaled_TwinIndex_temp_0 = 0;
	unmarshaled_TwinIndex_temp_0 = marshaled.___TwinIndex_0;
	unmarshaled.set_TwinIndex_0(unmarshaled_TwinIndex_temp_0);
	int16_t unmarshaled_PolygonIndex_temp_1 = 0;
	unmarshaled_PolygonIndex_temp_1 = marshaled.___PolygonIndex_1;
	unmarshaled.set_PolygonIndex_1(unmarshaled_PolygonIndex_temp_1);
	bool unmarshaled_HardEdge_temp_2 = false;
	unmarshaled_HardEdge_temp_2 = static_cast<bool>(marshaled.___HardEdge_2);
	unmarshaled.set_HardEdge_2(unmarshaled_HardEdge_temp_2);
	int16_t unmarshaled_VertexIndex_temp_3 = 0;
	unmarshaled_VertexIndex_temp_3 = marshaled.___VertexIndex_3;
	unmarshaled.set_VertexIndex_3(unmarshaled_VertexIndex_temp_3);
}
// Conversion method for clean up from marshalling of: RealtimeCSG.Legacy.HalfEdge
IL2CPP_EXTERN_C void HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshal_com_cleanup(HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42_marshaled_com& marshaled)
{
}
// System.Void RealtimeCSG.Legacy.HalfEdge::.ctor(System.Int16,System.Int32,System.Int16,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalfEdge__ctor_m3A641C7DC2F71E2E1BFC0B960494AA9258BA8F1A (HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * __this, int16_t ___polygonIndex0, int32_t ___twinIndex1, int16_t ___vertexIndex2, bool ___hardEdge3, const RuntimeMethod* method)
{
	{
		// TwinIndex        = twinIndex;
		int32_t L_0 = ___twinIndex1;
		__this->set_TwinIndex_0(L_0);
		// PolygonIndex    = polygonIndex;
		int16_t L_1 = ___polygonIndex0;
		__this->set_PolygonIndex_1(L_1);
		// HardEdge        = hardEdge;
		bool L_2 = ___hardEdge3;
		__this->set_HardEdge_2(L_2);
		// VertexIndex        = vertexIndex;
		int16_t L_3 = ___vertexIndex2;
		__this->set_VertexIndex_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void HalfEdge__ctor_m3A641C7DC2F71E2E1BFC0B960494AA9258BA8F1A_AdjustorThunk (RuntimeObject * __this, int16_t ___polygonIndex0, int32_t ___twinIndex1, int16_t ___vertexIndex2, bool ___hardEdge3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 * _thisAdjusted = reinterpret_cast<HalfEdge_t3EFE0D301644DBFA3AFFDAFC1E1CF42D2993BB42 *>(__this + _offset);
	HalfEdge__ctor_m3A641C7DC2F71E2E1BFC0B960494AA9258BA8F1A(_thisAdjusted, ___polygonIndex0, ___twinIndex1, ___vertexIndex2, ___hardEdge3, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.Legacy.Polygon::.ctor(System.Int32[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Polygon__ctor_m2E15F7F95331FC3DB64EE581891D0F1746349511 (Polygon_tA4424997B5520CBEBDCB5D84E2354A9CAAD29C19 * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___edges0, int32_t ___texGenIndex1, const RuntimeMethod* method)
{
	{
		// public Polygon(int[] edges, int texGenIndex) { EdgeIndices = edges; TexGenIndex = texGenIndex; }
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// public Polygon(int[] edges, int texGenIndex) { EdgeIndices = edges; TexGenIndex = texGenIndex; }
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_0 = ___edges0;
		__this->set_EdgeIndices_0(L_0);
		// public Polygon(int[] edges, int texGenIndex) { EdgeIndices = edges; TexGenIndex = texGenIndex; }
		int32_t L_1 = ___texGenIndex1;
		__this->set_TexGenIndex_1(L_1);
		// public Polygon(int[] edges, int texGenIndex) { EdgeIndices = edges; TexGenIndex = texGenIndex; }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RealtimeCSG.Legacy.Shape::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shape__ctor_m8D108B1E6FEEE23E0E75E1C7EF6D36D373B9BD65 (Shape_tC2B394A23C585E9413F3CF3C95B1B13C209F159F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shape__ctor_m8D108B1E6FEEE23E0E75E1C7EF6D36D373B9BD65_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] public float           Version         = 1.0f;
		__this->set_Version_0((1.0f));
		// [SerializeField] public Surface[]        Surfaces        = new Surface[0];
		SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C* L_0 = (SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C*)(SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C*)SZArrayNew(SurfaceU5BU5D_t07F6B02AF55C0C6552EF814AF71330D9F1B4987C_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_Surfaces_1(L_0);
		// [SerializeField] public TexGen[]        TexGens            = new TexGen[0];
		TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437* L_1 = (TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437*)(TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437*)SZArrayNew(TexGenU5BU5D_tCBA7AC22729243879E90146D05D42EB4187AC437_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_TexGens_2(L_1);
		// [SerializeField] public TexGenFlags[]    TexGenFlags        = new TexGenFlags[0];
		TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628* L_2 = (TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628*)(TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628*)SZArrayNew(TexGenFlagsU5BU5D_tDCB90EBC121E1998315835C3218C68E90DB99628_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_TexGenFlags_3(L_2);
		// [SerializeField] internal Material[]    Materials        = new Material[0];
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_3 = (MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398*)(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398*)SZArrayNew(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_Materials_4(L_3);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Matrix4x4 RealtimeCSG.Legacy.Surface::GenerateLocalBrushSpaceToPlaneSpaceMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C (Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// var normal         = Plane.normal;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * L_0 = __this->get_address_of_Plane_0();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var tangent         = Tangent;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = __this->get_Tangent_1();
		V_1 = L_2;
		// var biNormal     = BiNormal;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = __this->get_BiNormal_2();
		V_2 = L_3;
		// var pointOnPlane = Plane.pointOnPlane;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 * L_4 = __this->get_address_of_Plane_0();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB((CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403 *)L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		// return new Matrix4x4()
		// {
		//     m00 = tangent.x,  m01 = tangent.y,  m02 = tangent.z,  m03 = Vector3.Dot(tangent,  pointOnPlane),
		//     m10 = biNormal.x, m11 = biNormal.y, m12 = biNormal.z, m13 = Vector3.Dot(biNormal, pointOnPlane),
		//     m20 = normal.x,   m21 = normal.y,   m22 = normal.z,   m23 = Vector3.Dot(normal,   pointOnPlane),
		//     m30 = 0.0f, m31 = 0.0f, m32 = 0.0f, m33 = 1.0f
		// };
		il2cpp_codegen_initobj((&V_4), sizeof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA ));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = V_1;
		float L_7 = L_6.get_x_2();
		(&V_4)->set_m00_0(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_1;
		float L_9 = L_8.get_y_3();
		(&V_4)->set_m01_4(L_9);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_1;
		float L_11 = L_10.get_z_4();
		(&V_4)->set_m02_8(L_11);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_14 = Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1(L_12, L_13, /*hidden argument*/NULL);
		(&V_4)->set_m03_12(L_14);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_2;
		float L_16 = L_15.get_x_2();
		(&V_4)->set_m10_1(L_16);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = V_2;
		float L_18 = L_17.get_y_3();
		(&V_4)->set_m11_5(L_18);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = V_2;
		float L_20 = L_19.get_z_4();
		(&V_4)->set_m12_9(L_20);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = V_3;
		float L_23 = Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1(L_21, L_22, /*hidden argument*/NULL);
		(&V_4)->set_m13_13(L_23);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = V_0;
		float L_25 = L_24.get_x_2();
		(&V_4)->set_m20_2(L_25);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = V_0;
		float L_27 = L_26.get_y_3();
		(&V_4)->set_m21_6(L_27);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = V_0;
		float L_29 = L_28.get_z_4();
		(&V_4)->set_m22_10(L_29);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = V_3;
		float L_32 = Vector3_Dot_m0C530E1C51278DE28B77906D56356506232272C1(L_30, L_31, /*hidden argument*/NULL);
		(&V_4)->set_m23_14(L_32);
		(&V_4)->set_m30_3((0.0f));
		(&V_4)->set_m31_7((0.0f));
		(&V_4)->set_m32_11((0.0f));
		(&V_4)->set_m33_15((1.0f));
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_33 = V_4;
		return L_33;
	}
}
IL2CPP_EXTERN_C  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * _thisAdjusted = reinterpret_cast<Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 *>(__this + _offset);
	return Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C(_thisAdjusted, method);
}
// System.String RealtimeCSG.Legacy.Surface::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB (Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string ToString() { return string.Format("Plane: {0} Tangent: {1} BiNormal: {2} TexGenIndex: {3}", Plane, Tangent, BiNormal, TexGenIndex); }
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_2 = __this->get_Plane_0();
		CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403  L_3 = L_2;
		RuntimeObject * L_4 = Box(CSGPlane_t327DB84E221B6B41CDA5F1B372757BF947B93403_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = __this->get_Tangent_1();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = L_6;
		RuntimeObject * L_8 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = __this->get_BiNormal_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = L_10;
		RuntimeObject * L_12 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_9;
		int32_t L_14 = __this->get_TexGenIndex_3();
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral3D803514209E4FD4CD38079A03486581A84DA097, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C  String_t* Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 * _thisAdjusted = reinterpret_cast<Surface_t366C342CEE27C20E70E49DAB700E260E4EDA3C17 *>(__this + _offset);
	return Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: RealtimeCSG.Legacy.TexGen
IL2CPP_EXTERN_C void TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshal_pinvoke(const TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45& unmarshaled, TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_pinvoke& marshaled)
{
	Exception_t* ___RenderMaterial_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'RenderMaterial' of type 'TexGen': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___RenderMaterial_3Exception, NULL);
}
IL2CPP_EXTERN_C void TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshal_pinvoke_back(const TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_pinvoke& marshaled, TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45& unmarshaled)
{
	Exception_t* ___RenderMaterial_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'RenderMaterial' of type 'TexGen': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___RenderMaterial_3Exception, NULL);
}
// Conversion method for clean up from marshalling of: RealtimeCSG.Legacy.TexGen
IL2CPP_EXTERN_C void TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshal_pinvoke_cleanup(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: RealtimeCSG.Legacy.TexGen
IL2CPP_EXTERN_C void TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshal_com(const TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45& unmarshaled, TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_com& marshaled)
{
	Exception_t* ___RenderMaterial_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'RenderMaterial' of type 'TexGen': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___RenderMaterial_3Exception, NULL);
}
IL2CPP_EXTERN_C void TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshal_com_back(const TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_com& marshaled, TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45& unmarshaled)
{
	Exception_t* ___RenderMaterial_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'RenderMaterial' of type 'TexGen': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___RenderMaterial_3Exception, NULL);
}
// Conversion method for clean up from marshalling of: RealtimeCSG.Legacy.TexGen
IL2CPP_EXTERN_C void TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshal_com_cleanup(TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45_marshaled_com& marshaled)
{
}
// System.Void RealtimeCSG.Legacy.TexGen::.ctor(UnityEngine.Material,UnityEngine.PhysicMaterial)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE (TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___renderMaterial0, PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___physicsMaterial1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Translation            = MathConstants.zeroVector3;
		IL2CPP_RUNTIME_CLASS_INIT(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_zeroVector3_16();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_0, /*hidden argument*/NULL);
		__this->set_Translation_0(L_1);
		// Scale                = MathConstants.oneVector3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ((MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_StaticFields*)il2cpp_codegen_static_fields_for(MathConstants_t2A83C9D5A2EB2C64D8F541784BE1A960FBB654FA_il2cpp_TypeInfo_var))->get_oneVector3_17();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_2, /*hidden argument*/NULL);
		__this->set_Scale_1(L_3);
		// RotationAngle        = 0;
		__this->set_RotationAngle_2((0.0f));
		// RenderMaterial        = renderMaterial;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_4 = ___renderMaterial0;
		__this->set_RenderMaterial_3(L_4);
		// PhysicsMaterial        = physicsMaterial;
		PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * L_5 = ___physicsMaterial1;
		__this->set_PhysicsMaterial_4(L_5);
		// SmoothingGroup        = 0;
		__this->set_SmoothingGroup_5(0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE_AdjustorThunk (RuntimeObject * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___renderMaterial0, PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___physicsMaterial1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * _thisAdjusted = reinterpret_cast<TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 *>(__this + _offset);
	TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE(_thisAdjusted, ___renderMaterial0, ___physicsMaterial1, method);
}
// UnityEngine.Matrix4x4 RealtimeCSG.Legacy.TexGen::GeneratePlaneSpaceToTextureSpaceMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8 (TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_8;
	memset((&V_8), 0, sizeof(V_8));
	{
		// var sx    = Scale.x;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_0 = __this->get_address_of_Scale_1();
		float L_1 = L_0->get_x_0();
		V_0 = L_1;
		// var sy    = Scale.y;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_2 = __this->get_address_of_Scale_1();
		float L_3 = L_2->get_y_1();
		V_1 = L_3;
		// var r    = Mathf.Deg2Rad * -RotationAngle;
		float L_4 = __this->get_RotationAngle_2();
		// var rs    = Mathf.Sin(r);
		float L_5 = ((float)il2cpp_codegen_multiply((float)(0.0174532924f), (float)((-L_4))));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_6 = sinf(L_5);
		V_2 = L_6;
		// var rc    = Mathf.Cos(r);
		float L_7 = cosf(L_5);
		V_3 = L_7;
		// var tx    = Translation.x;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_8 = __this->get_address_of_Translation_0();
		float L_9 = L_8->get_x_0();
		V_4 = L_9;
		// var ty    = Translation.y;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_10 = __this->get_address_of_Translation_0();
		float L_11 = L_10->get_y_1();
		V_5 = L_11;
		// var scaleMatrix = new Matrix4x4()
		// {
		//     m00 =  -sx, m10 = 0.0f, m20 = 0.0f, m30 = 0.0f,
		//     m01 = 0.0f, m11 =   sy, m21 = 0.0f, m31 = 0.0f,
		//     m02 = 0.0f, m12 = 0.0f, m22 = 1.0f, m32 = 0.0f,
		//     m03 = 0.0f, m13 = 0.0f, m23 = 0.0f, m33 = 1.0f
		// };
		il2cpp_codegen_initobj((&V_8), sizeof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA ));
		float L_12 = V_0;
		(&V_8)->set_m00_0(((-L_12)));
		(&V_8)->set_m10_1((0.0f));
		(&V_8)->set_m20_2((0.0f));
		(&V_8)->set_m30_3((0.0f));
		(&V_8)->set_m01_4((0.0f));
		float L_13 = V_1;
		(&V_8)->set_m11_5(L_13);
		(&V_8)->set_m21_6((0.0f));
		(&V_8)->set_m31_7((0.0f));
		(&V_8)->set_m02_8((0.0f));
		(&V_8)->set_m12_9((0.0f));
		(&V_8)->set_m22_10((1.0f));
		(&V_8)->set_m32_11((0.0f));
		(&V_8)->set_m03_12((0.0f));
		(&V_8)->set_m13_13((0.0f));
		(&V_8)->set_m23_14((0.0f));
		(&V_8)->set_m33_15((1.0f));
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_14 = V_8;
		V_6 = L_14;
		// var translationMatrix = new Matrix4x4()
		// {
		//     m00 = 1.0f, m10 = 0.0f, m20 = 0.0f, m30 = 0.0f,
		//     m01 = 0.0f, m11 = 1.0f, m21 = 0.0f, m31 = 0.0f,
		//     m02 = 0.0f, m12 = 0.0f, m22 = 1.0f, m32 = 0.0f,
		//     m03 =   tx, m13 =   ty, m23 = 0.0f, m33 = 1.0f
		// };
		il2cpp_codegen_initobj((&V_8), sizeof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA ));
		(&V_8)->set_m00_0((1.0f));
		(&V_8)->set_m10_1((0.0f));
		(&V_8)->set_m20_2((0.0f));
		(&V_8)->set_m30_3((0.0f));
		(&V_8)->set_m01_4((0.0f));
		(&V_8)->set_m11_5((1.0f));
		(&V_8)->set_m21_6((0.0f));
		(&V_8)->set_m31_7((0.0f));
		(&V_8)->set_m02_8((0.0f));
		(&V_8)->set_m12_9((0.0f));
		(&V_8)->set_m22_10((1.0f));
		(&V_8)->set_m32_11((0.0f));
		float L_15 = V_4;
		(&V_8)->set_m03_12(L_15);
		float L_16 = V_5;
		(&V_8)->set_m13_13(L_16);
		(&V_8)->set_m23_14((0.0f));
		(&V_8)->set_m33_15((1.0f));
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_17 = V_8;
		// var rotationMatrix = new Matrix4x4()
		// {
		//     m00 =   rc, m10 =  -rs, m20 = 0.0f, m30 = 0.0f,
		//     m01 =   rs, m11 =   rc, m21 = 0.0f, m31 = 0.0f,
		//     m02 = 0.0f, m12 = 0.0f, m22 = 1.0f, m32 = 0.0f,
		//     m03 = 0.0f, m13 = 0.0f, m23 = 0.0f, m33 = 1.0f
		// };
		il2cpp_codegen_initobj((&V_8), sizeof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA ));
		float L_18 = V_3;
		(&V_8)->set_m00_0(L_18);
		float L_19 = V_2;
		(&V_8)->set_m10_1(((-L_19)));
		(&V_8)->set_m20_2((0.0f));
		(&V_8)->set_m30_3((0.0f));
		float L_20 = V_2;
		(&V_8)->set_m01_4(L_20);
		float L_21 = V_3;
		(&V_8)->set_m11_5(L_21);
		(&V_8)->set_m21_6((0.0f));
		(&V_8)->set_m31_7((0.0f));
		(&V_8)->set_m02_8((0.0f));
		(&V_8)->set_m12_9((0.0f));
		(&V_8)->set_m22_10((1.0f));
		(&V_8)->set_m32_11((0.0f));
		(&V_8)->set_m03_12((0.0f));
		(&V_8)->set_m13_13((0.0f));
		(&V_8)->set_m23_14((0.0f));
		(&V_8)->set_m33_15((1.0f));
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_22 = V_8;
		V_7 = L_22;
		// return (translationMatrix
		//         * scaleMatrix)
		//         * rotationMatrix;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_23 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_24 = Matrix4x4_op_Multiply_mF6693A950E1917204E356366892C3CCB0553436E(L_17, L_23, /*hidden argument*/NULL);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_25 = V_7;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_26 = Matrix4x4_op_Multiply_mF6693A950E1917204E356366892C3CCB0553436E(L_24, L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
IL2CPP_EXTERN_C  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 * _thisAdjusted = reinterpret_cast<TexGen_tBAA77ED76F0A268807BDA3CEABB9C64A9E673E45 *>(__this + _offset);
	return TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
