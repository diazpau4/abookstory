﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void GameObjectExtensions::Destroy(UnityEngine.Object)
extern void GameObjectExtensions_Destroy_m7F688FC353A6D032E74735F218D717755882AD4F (void);
// 0x00000002 System.Void GameObjectExtensions::Destroy(UnityEngine.GameObject)
extern void GameObjectExtensions_Destroy_mC3E6EF0B1DA3A96073259D8882A9CC5D23EF7375 (void);
// 0x00000003 System.Void GameObjectExtensions::SanitizeGameObject(UnityEngine.GameObject)
extern void GameObjectExtensions_SanitizeGameObject_mE95D20EB8201288ADEC9F31C694DF9CAC6D751DE (void);
// 0x00000004 System.Boolean GameObjectExtensions::TryDestroy(UnityEngine.GameObject)
extern void GameObjectExtensions_TryDestroy_m2336410EBE2F3B30E857C090F0392C4EB4AA0B8C (void);
// 0x00000005 System.Void InternalRealtimeCSG.HiddenComponentData::.ctor()
extern void HiddenComponentData__ctor_m01542360BAD54D4EEFF8A4139826D6EB7C20B977 (void);
// 0x00000006 System.Void InternalRealtimeCSG.CSGModelExported::Awake()
extern void CSGModelExported_Awake_mF191E38CA0ED42C2B9B86682740E4E8C3957741E (void);
// 0x00000007 System.Void InternalRealtimeCSG.CSGModelExported::.ctor()
extern void CSGModelExported__ctor_mEB8026CFB8477A739C7E196C95BA0D6D037E35AD (void);
// 0x00000008 System.Void InternalRealtimeCSG.GeneratedMeshInstance::Awake()
extern void GeneratedMeshInstance_Awake_m0DAF3822B276AB9315F3A3CA3FD859515BE308F2 (void);
// 0x00000009 System.Void InternalRealtimeCSG.GeneratedMeshInstance::.ctor()
extern void GeneratedMeshInstance__ctor_m5074F8C7790B1F24FD9C6DF507A7728524C6EA55 (void);
// 0x0000000A System.Void InternalRealtimeCSG.GeneratedMeshes::Awake()
extern void GeneratedMeshes_Awake_m09A83476C251049B04AA716B49E2015A5B39FE75 (void);
// 0x0000000B System.Void InternalRealtimeCSG.GeneratedMeshes::.ctor()
extern void GeneratedMeshes__ctor_mEA03FAA18011B37C2BCC4C7DC37B61887591332F (void);
// 0x0000000C System.Void InternalRealtimeCSG.LegacyGeneratedMeshContainer::Awake()
extern void LegacyGeneratedMeshContainer_Awake_m856864A9B92959D607A19300A513DA9D99B24D3F (void);
// 0x0000000D System.Void InternalRealtimeCSG.LegacyGeneratedMeshContainer::.ctor()
extern void LegacyGeneratedMeshContainer__ctor_m3F0DC1CC4ACEF1770812F4CD2EE88E440A1D94ED (void);
// 0x0000000E System.Void InternalRealtimeCSG.MathConstants::.cctor()
extern void MathConstants__cctor_m11C0D67BFD9B99DDDA20168C4981644FFA5BBC8F (void);
// 0x0000000F System.Void RealtimeCSG.EnumAsFlagsAttribute::.ctor()
extern void EnumAsFlagsAttribute__ctor_m115FEF82DC42DF1EB3A67C1E3297A75E7BB72A36 (void);
// 0x00000010 UnityEngine.Mesh RealtimeCSG.CSGMeshUtility::Clone(UnityEngine.Mesh)
extern void CSGMeshUtility_Clone_m4E3FDDC630BAFC1A52B2E34E9ED98FC179BA81D8 (void);
// 0x00000011 UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::get_normal()
extern void CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF_AdjustorThunk (void);
// 0x00000012 System.Void RealtimeCSG.Legacy.CSGPlane::set_normal(UnityEngine.Vector3)
extern void CSGPlane_set_normal_m946BA6635B964F0675FBC69F5EFC9BD95706D8BD_AdjustorThunk (void);
// 0x00000013 UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::get_pointOnPlane()
extern void CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB_AdjustorThunk (void);
// 0x00000014 System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Plane)
extern void CSGPlane__ctor_m93D9A7BCD9C724AF1005E13B7DF4FCA19D50963F_AdjustorThunk (void);
// 0x00000015 System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,System.Single)
extern void CSGPlane__ctor_m31DC6D44D2B5A5C6C686E45A5523EF7E4ECA77F2_AdjustorThunk (void);
// 0x00000016 System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3_AdjustorThunk (void);
// 0x00000017 System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130_AdjustorThunk (void);
// 0x00000018 System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402_AdjustorThunk (void);
// 0x00000019 System.Void RealtimeCSG.Legacy.CSGPlane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1_AdjustorThunk (void);
// 0x0000001A UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::RayIntersection(UnityEngine.Ray)
extern void CSGPlane_RayIntersection_m617BEB5AFB352E3526C5D3980CEEDA793F89DD0B_AdjustorThunk (void);
// 0x0000001B System.Boolean RealtimeCSG.Legacy.CSGPlane::TryRayIntersection(UnityEngine.Ray,UnityEngine.Vector3&)
extern void CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5_AdjustorThunk (void);
// 0x0000001C UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::LineIntersection(UnityEngine.Vector3,UnityEngine.Vector3)
extern void CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580_AdjustorThunk (void);
// 0x0000001D UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::Intersection(RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane)
extern void CSGPlane_Intersection_m77828575E659430171184D441835224E158F1898 (void);
// 0x0000001E System.Single RealtimeCSG.Legacy.CSGPlane::Distance(UnityEngine.Vector3)
extern void CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC_AdjustorThunk (void);
// 0x0000001F System.Void RealtimeCSG.Legacy.CSGPlane::Normalize()
extern void CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581_AdjustorThunk (void);
// 0x00000020 System.Void RealtimeCSG.Legacy.CSGPlane::Transform(UnityEngine.Matrix4x4)
extern void CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB_AdjustorThunk (void);
// 0x00000021 RealtimeCSG.Legacy.CSGPlane RealtimeCSG.Legacy.CSGPlane::Negated()
extern void CSGPlane_Negated_m197727B4586DC28A9A753E82FFE636D05DB5F9E5_AdjustorThunk (void);
// 0x00000022 RealtimeCSG.Legacy.CSGPlane RealtimeCSG.Legacy.CSGPlane::Translated(UnityEngine.Vector3)
extern void CSGPlane_Translated_mAB1A8898F2402DAF9F25A44F2A503566C240443D_AdjustorThunk (void);
// 0x00000023 UnityEngine.Vector3 RealtimeCSG.Legacy.CSGPlane::Project(UnityEngine.Vector3)
extern void CSGPlane_Project_m579D50E710FBC39A748ABEE2AAFCC2C6E9241B94_AdjustorThunk (void);
// 0x00000024 System.Int32 RealtimeCSG.Legacy.CSGPlane::GetHashCode()
extern void CSGPlane_GetHashCode_mA53E0BF2F7FDAA715588FBA59C4C49E6DFB1E33B_AdjustorThunk (void);
// 0x00000025 System.Boolean RealtimeCSG.Legacy.CSGPlane::Equals(RealtimeCSG.Legacy.CSGPlane)
extern void CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3_AdjustorThunk (void);
// 0x00000026 System.Boolean RealtimeCSG.Legacy.CSGPlane::Equals(System.Object)
extern void CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D_AdjustorThunk (void);
// 0x00000027 System.Boolean RealtimeCSG.Legacy.CSGPlane::op_Equality(RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane)
extern void CSGPlane_op_Equality_m5DDC85F3465F45DB2045D95DAC2CCE87D4DD1E00 (void);
// 0x00000028 System.Boolean RealtimeCSG.Legacy.CSGPlane::op_Inequality(RealtimeCSG.Legacy.CSGPlane,RealtimeCSG.Legacy.CSGPlane)
extern void CSGPlane_op_Inequality_m351E7463AB7335D358F0B8A814FFE638F67965B9 (void);
// 0x00000029 System.String RealtimeCSG.Legacy.CSGPlane::ToString()
extern void CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8_AdjustorThunk (void);
// 0x0000002A System.Void RealtimeCSG.Legacy.HalfEdge::.ctor(System.Int16,System.Int32,System.Int16,System.Boolean)
extern void HalfEdge__ctor_m3A641C7DC2F71E2E1BFC0B960494AA9258BA8F1A_AdjustorThunk (void);
// 0x0000002B System.Void RealtimeCSG.Legacy.Polygon::.ctor(System.Int32[],System.Int32)
extern void Polygon__ctor_m2E15F7F95331FC3DB64EE581891D0F1746349511 (void);
// 0x0000002C System.Boolean RealtimeCSG.Legacy.ControlMesh::get_Valid()
extern void ControlMesh_get_Valid_m271463949CA9305A922B043B3678B1D5951014DC (void);
// 0x0000002D System.Void RealtimeCSG.Legacy.ControlMesh::set_Valid(System.Boolean)
extern void ControlMesh_set_Valid_m3902886AF73159E55499A3FAE5C8C77FE3BE06FC (void);
// 0x0000002E System.Void RealtimeCSG.Legacy.ControlMesh::.ctor()
extern void ControlMesh__ctor_m93DFAB3F904281143760B4713D2A2AE89453E3E4 (void);
// 0x0000002F System.Void RealtimeCSG.Legacy.ControlMesh::.ctor(RealtimeCSG.Legacy.ControlMesh)
extern void ControlMesh__ctor_m12DF3940EE5E54F6E41903867A37567A0D4743B4 (void);
// 0x00000030 System.Void RealtimeCSG.Legacy.ControlMesh::SetDirty()
extern void ControlMesh_SetDirty_mE2DD5AB15A4B2717E50ADF05196B50C5995AA093 (void);
// 0x00000031 System.Void RealtimeCSG.Legacy.ControlMesh::Reset()
extern void ControlMesh_Reset_m70A47B47DED0106F851EF950DDCA2FAF497800F1 (void);
// 0x00000032 System.Void RealtimeCSG.Legacy.ControlMesh::CopyFrom(RealtimeCSG.Legacy.ControlMesh)
extern void ControlMesh_CopyFrom_m010324448CDA71EAC8776694628A7307CE186C42 (void);
// 0x00000033 RealtimeCSG.Legacy.ControlMesh RealtimeCSG.Legacy.ControlMesh::Clone()
extern void ControlMesh_Clone_m22DA0E5DE55549B13B5E4B2FB235A9764CC1E912 (void);
// 0x00000034 UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetVertex(System.Int32)
extern void ControlMesh_GetVertex_mE0F471B92EB47F38895C9D97A12F066B885F9662 (void);
// 0x00000035 UnityEngine.Vector3[] RealtimeCSG.Legacy.ControlMesh::GetVertices(System.Int32[])
extern void ControlMesh_GetVertices_mD78ABF872903C01463776ADD972AC9667D4EB3FB (void);
// 0x00000036 UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetVertex(RealtimeCSG.Legacy.HalfEdge&)
extern void ControlMesh_GetVertex_m30F61A0C56229557886C7B4CF27481530A929D43 (void);
// 0x00000037 System.Int16 RealtimeCSG.Legacy.ControlMesh::GetVertexIndex(System.Int32)
extern void ControlMesh_GetVertexIndex_mDFAB5A9F8DD849F862D5899498E929F6334949FB (void);
// 0x00000038 System.Int16 RealtimeCSG.Legacy.ControlMesh::GetVertexIndex(RealtimeCSG.Legacy.HalfEdge&)
extern void ControlMesh_GetVertexIndex_mC4DC0CC7D01599B06B5F8AF0FFB5EFB3E453B628 (void);
// 0x00000039 UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertex(RealtimeCSG.Legacy.HalfEdge&)
extern void ControlMesh_GetTwinEdgeVertex_mC68325F10BC9FC6E6E2DB022451671DAD33F33C4 (void);
// 0x0000003A UnityEngine.Vector3 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertex(System.Int32)
extern void ControlMesh_GetTwinEdgeVertex_m8AECF1CB123CB39ADCBBCD9AE0833522577DDE85 (void);
// 0x0000003B System.Int16 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertexIndex(RealtimeCSG.Legacy.HalfEdge&)
extern void ControlMesh_GetTwinEdgeVertexIndex_m4E5F9B2AEAA2E5BC5FDB7B5DD374167EE366ED14 (void);
// 0x0000003C System.Int16 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeVertexIndex(System.Int32)
extern void ControlMesh_GetTwinEdgeVertexIndex_m062983016F73A2E02A2E9A626857946E4FD4B027 (void);
// 0x0000003D System.Int32 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeIndex(RealtimeCSG.Legacy.HalfEdge&)
extern void ControlMesh_GetTwinEdgeIndex_m5F64058A123F28EC0C48F645E4F67057E3AE20C1 (void);
// 0x0000003E System.Int32 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgeIndex(System.Int32)
extern void ControlMesh_GetTwinEdgeIndex_mEB16E205B788C2614EFB4CD2EA3A1F62C224B7F3 (void);
// 0x0000003F System.Int16 RealtimeCSG.Legacy.ControlMesh::GetTwinEdgePolygonIndex(System.Int32)
extern void ControlMesh_GetTwinEdgePolygonIndex_m7E8B0F67E65B6D92C4DDBEA3A0EE09341CB953C8 (void);
// 0x00000040 System.Int16 RealtimeCSG.Legacy.ControlMesh::GetEdgePolygonIndex(System.Int32)
extern void ControlMesh_GetEdgePolygonIndex_m7A1C2726753E24BA4A0043CC12B68A74E3019AA6 (void);
// 0x00000041 System.Int32 RealtimeCSG.Legacy.ControlMesh::GetNextEdgeIndexAroundVertex(System.Int32)
extern void ControlMesh_GetNextEdgeIndexAroundVertex_m8F5FD766D00A1C04EF36ACBCE3B91E664A096145 (void);
// 0x00000042 System.Int32 RealtimeCSG.Legacy.ControlMesh::GetPrevEdgeIndex(System.Int32)
extern void ControlMesh_GetPrevEdgeIndex_m90955D2563E0C0B7374C23D1FAC8D31F28E4ED1F (void);
// 0x00000043 System.Int32 RealtimeCSG.Legacy.ControlMesh::GetNextEdgeIndex(System.Int32)
extern void ControlMesh_GetNextEdgeIndex_m0E2F6BC7E88481CBDE3C3B166BBA15BB94D744D6 (void);
// 0x00000044 System.Void RealtimeCSG.Legacy.Shape::.ctor()
extern void Shape__ctor_m8D108B1E6FEEE23E0E75E1C7EF6D36D373B9BD65 (void);
// 0x00000045 UnityEngine.Matrix4x4 RealtimeCSG.Legacy.Surface::GenerateLocalBrushSpaceToPlaneSpaceMatrix()
extern void Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C_AdjustorThunk (void);
// 0x00000046 System.String RealtimeCSG.Legacy.Surface::ToString()
extern void Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB_AdjustorThunk (void);
// 0x00000047 System.Void RealtimeCSG.Legacy.TexGen::.ctor(UnityEngine.Material,UnityEngine.PhysicMaterial)
extern void TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE_AdjustorThunk (void);
// 0x00000048 UnityEngine.Matrix4x4 RealtimeCSG.Legacy.TexGen::GeneratePlaneSpaceToTextureSpaceMatrix()
extern void TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8_AdjustorThunk (void);
// 0x00000049 System.Void RealtimeCSG.Foundation.GeneratedMeshContents::CopyTo(UnityEngine.Mesh)
extern void GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088 (void);
// 0x0000004A System.Void RealtimeCSG.Foundation.GeneratedMeshContents::.ctor()
extern void GeneratedMeshContents__ctor_mE4E0696A024F80FD5CAD7950FD95B0CAAB64A7F1 (void);
// 0x0000004B System.Boolean RealtimeCSG.Foundation.GeneratedMeshDescription::Equals(System.Object)
extern void GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF_AdjustorThunk (void);
// 0x0000004C System.Int32 RealtimeCSG.Foundation.GeneratedMeshDescription::GetHashCode()
extern void GeneratedMeshDescription_GetHashCode_mAEEFC5F7B260A59A966113874DC4AA3F77277C0B_AdjustorThunk (void);
// 0x0000004D System.Boolean RealtimeCSG.Foundation.GeneratedMeshDescription::op_Equality(RealtimeCSG.Foundation.GeneratedMeshDescription,RealtimeCSG.Foundation.GeneratedMeshDescription)
extern void GeneratedMeshDescription_op_Equality_m7DD880CECD1AC75CC2C1193DCAA7585CB0721B10 (void);
// 0x0000004E System.Boolean RealtimeCSG.Foundation.GeneratedMeshDescription::op_Inequality(RealtimeCSG.Foundation.GeneratedMeshDescription,RealtimeCSG.Foundation.GeneratedMeshDescription)
extern void GeneratedMeshDescription_op_Inequality_m50A84C22C19C8CD2DC1909CBC8363C992550AD06 (void);
// 0x0000004F System.Void RealtimeCSG.Foundation.MeshQuery::.ctor(RealtimeCSG.Foundation.LayerUsageFlags,RealtimeCSG.Foundation.LayerUsageFlags,RealtimeCSG.Foundation.LayerParameterIndex,RealtimeCSG.Foundation.VertexChannelFlags)
extern void MeshQuery__ctor_mAFFC39222E6FE5BD686A17A40FDF664DCFB6210D_AdjustorThunk (void);
// 0x00000050 RealtimeCSG.Foundation.LayerUsageFlags RealtimeCSG.Foundation.MeshQuery::get_LayerQuery()
extern void MeshQuery_get_LayerQuery_m4703C9835C13FCF59B4B409303FCB42E0532FA36_AdjustorThunk (void);
// 0x00000051 System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerQuery(RealtimeCSG.Foundation.LayerUsageFlags)
extern void MeshQuery_set_LayerQuery_mC9946EC13A1FD87A223B830FF7A421B68D883374_AdjustorThunk (void);
// 0x00000052 RealtimeCSG.Foundation.LayerUsageFlags RealtimeCSG.Foundation.MeshQuery::get_LayerQueryMask()
extern void MeshQuery_get_LayerQueryMask_mC503693548152B334FD4BE66859983D6D045C84D_AdjustorThunk (void);
// 0x00000053 System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerQueryMask(RealtimeCSG.Foundation.LayerUsageFlags)
extern void MeshQuery_set_LayerQueryMask_mDE272289C3E300914F392DFC5F893CE18D83DD57_AdjustorThunk (void);
// 0x00000054 RealtimeCSG.Foundation.LayerParameterIndex RealtimeCSG.Foundation.MeshQuery::get_LayerParameterIndex()
extern void MeshQuery_get_LayerParameterIndex_mDCB4D744B10C68E3B7D28DE40ECFEE24CD264186_AdjustorThunk (void);
// 0x00000055 System.Void RealtimeCSG.Foundation.MeshQuery::set_LayerParameterIndex(RealtimeCSG.Foundation.LayerParameterIndex)
extern void MeshQuery_set_LayerParameterIndex_m8369D65DEFA1422B838AE8E56EAEEBFB5B733A6D_AdjustorThunk (void);
// 0x00000056 RealtimeCSG.Foundation.VertexChannelFlags RealtimeCSG.Foundation.MeshQuery::get_UsedVertexChannels()
extern void MeshQuery_get_UsedVertexChannels_mA414AB75F94B51B487825C0DD43BA83759836E6A_AdjustorThunk (void);
// 0x00000057 System.Void RealtimeCSG.Foundation.MeshQuery::set_UsedVertexChannels(RealtimeCSG.Foundation.VertexChannelFlags)
extern void MeshQuery_set_UsedVertexChannels_mE603C1D0593AA6FB6012D060B4047A3F6E8BD3F0_AdjustorThunk (void);
// 0x00000058 System.String RealtimeCSG.Foundation.MeshQuery::ToString()
extern void MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5_AdjustorThunk (void);
// 0x00000059 System.Boolean RealtimeCSG.Foundation.MeshQuery::op_Equality(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
extern void MeshQuery_op_Equality_m3A2B9D930633CA85ABB5CE3A89E3E6C176D0F7BE (void);
// 0x0000005A System.Boolean RealtimeCSG.Foundation.MeshQuery::op_Inequality(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
extern void MeshQuery_op_Inequality_m754576CB4F0C360EF2DDB08983FCAB87513DB3FC (void);
// 0x0000005B System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(System.Object)
extern void MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA_AdjustorThunk (void);
// 0x0000005C System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(RealtimeCSG.Foundation.MeshQuery)
extern void MeshQuery_Equals_m855368ED8A4BB10172F924F3050916596BBEFB74_AdjustorThunk (void);
// 0x0000005D System.Boolean RealtimeCSG.Foundation.MeshQuery::Equals(RealtimeCSG.Foundation.MeshQuery,RealtimeCSG.Foundation.MeshQuery)
extern void MeshQuery_Equals_m8F0D1975C3DC6414A0B3917BF3A3E788138D5366 (void);
// 0x0000005E System.Int32 RealtimeCSG.Foundation.MeshQuery::GetHashCode()
extern void MeshQuery_GetHashCode_m5FBF184A4ED516B0A600B2B4C0DC2C1C4DDE1B10_AdjustorThunk (void);
// 0x0000005F System.Void RealtimeCSG.Components.CSGBrush::.ctor()
extern void CSGBrush__ctor_m954F3029A94264D30A3A35AE1343EF327BC20364 (void);
// 0x00000060 System.Boolean RealtimeCSG.Components.CSGModel::get_IsRenderable()
extern void CSGModel_get_IsRenderable_m7523B8AB0B0889D549728402BB305A85203FC394 (void);
// 0x00000061 System.Boolean RealtimeCSG.Components.CSGModel::get_IsTwoSidedShadows()
extern void CSGModel_get_IsTwoSidedShadows_m51838E2565C55977464F948440710B12DC2FFF0C (void);
// 0x00000062 System.Boolean RealtimeCSG.Components.CSGModel::get_HaveCollider()
extern void CSGModel_get_HaveCollider_m354F9EAB9A3AA0F5EEC799199817E094DED64A6E (void);
// 0x00000063 System.Boolean RealtimeCSG.Components.CSGModel::get_IsTrigger()
extern void CSGModel_get_IsTrigger_mC4B35628103C632A811008B15BA795DC4DF92AAE (void);
// 0x00000064 System.Boolean RealtimeCSG.Components.CSGModel::get_InvertedWorld()
extern void CSGModel_get_InvertedWorld_m6C5FA2F7E53BB1F4A8DD0D54BDD19BC19DA5DF5B (void);
// 0x00000065 System.Boolean RealtimeCSG.Components.CSGModel::get_SetColliderConvex()
extern void CSGModel_get_SetColliderConvex_mD9B220CD338F81D758CD3D4F539AD46BCD0DA848 (void);
// 0x00000066 System.Boolean RealtimeCSG.Components.CSGModel::get_NeedAutoUpdateRigidBody()
extern void CSGModel_get_NeedAutoUpdateRigidBody_m2F9D1D4B737B6837B8C84F8D255B51F5FFE6AE35 (void);
// 0x00000067 System.Boolean RealtimeCSG.Components.CSGModel::get_PreserveUVs()
extern void CSGModel_get_PreserveUVs_mF08E71E035F262083F67FE8CCDACC429E23428D9 (void);
// 0x00000068 System.Boolean RealtimeCSG.Components.CSGModel::get_StitchLightmapSeams()
extern void CSGModel_get_StitchLightmapSeams_mEE2D674002DB25512A1925E08AEDB5C1652804D1 (void);
// 0x00000069 System.Boolean RealtimeCSG.Components.CSGModel::get_AutoRebuildUVs()
extern void CSGModel_get_AutoRebuildUVs_m1307BCFF754F6A001AD138571A0B0704F6AA87AC (void);
// 0x0000006A System.Boolean RealtimeCSG.Components.CSGModel::get_IgnoreNormals()
extern void CSGModel_get_IgnoreNormals_m557DC787F6EB5E2376CD7DF8CDDE6BBAD0EFEA34 (void);
// 0x0000006B System.Void RealtimeCSG.Components.CSGModel::.ctor()
extern void CSGModel__ctor_m156A0623EA2BF1DF350B2D71E5F745CA8854FB2D (void);
// 0x0000006C System.Void RealtimeCSG.Components.CSGModel::.cctor()
extern void CSGModel__cctor_m68D842D99DC57996CD00259E16E32A676EF40B3B (void);
// 0x0000006D System.Void RealtimeCSG.Components.CSGNode::.ctor()
extern void CSGNode__ctor_mE9B0ADCA67AFD7B22004171B6E48C7C386FB3EE0 (void);
// 0x0000006E System.Void RealtimeCSG.Components.CSGOperation::.ctor()
extern void CSGOperation__ctor_mD3CA12D90C37AE6BE3C6C8D1FE9402E80FFAB050 (void);
static Il2CppMethodPointer s_methodPointers[110] = 
{
	GameObjectExtensions_Destroy_m7F688FC353A6D032E74735F218D717755882AD4F,
	GameObjectExtensions_Destroy_mC3E6EF0B1DA3A96073259D8882A9CC5D23EF7375,
	GameObjectExtensions_SanitizeGameObject_mE95D20EB8201288ADEC9F31C694DF9CAC6D751DE,
	GameObjectExtensions_TryDestroy_m2336410EBE2F3B30E857C090F0392C4EB4AA0B8C,
	HiddenComponentData__ctor_m01542360BAD54D4EEFF8A4139826D6EB7C20B977,
	CSGModelExported_Awake_mF191E38CA0ED42C2B9B86682740E4E8C3957741E,
	CSGModelExported__ctor_mEB8026CFB8477A739C7E196C95BA0D6D037E35AD,
	GeneratedMeshInstance_Awake_m0DAF3822B276AB9315F3A3CA3FD859515BE308F2,
	GeneratedMeshInstance__ctor_m5074F8C7790B1F24FD9C6DF507A7728524C6EA55,
	GeneratedMeshes_Awake_m09A83476C251049B04AA716B49E2015A5B39FE75,
	GeneratedMeshes__ctor_mEA03FAA18011B37C2BCC4C7DC37B61887591332F,
	LegacyGeneratedMeshContainer_Awake_m856864A9B92959D607A19300A513DA9D99B24D3F,
	LegacyGeneratedMeshContainer__ctor_m3F0DC1CC4ACEF1770812F4CD2EE88E440A1D94ED,
	MathConstants__cctor_m11C0D67BFD9B99DDDA20168C4981644FFA5BBC8F,
	EnumAsFlagsAttribute__ctor_m115FEF82DC42DF1EB3A67C1E3297A75E7BB72A36,
	CSGMeshUtility_Clone_m4E3FDDC630BAFC1A52B2E34E9ED98FC179BA81D8,
	CSGPlane_get_normal_m4E69AE6983C96E4EC33550BBFAF5DAA5CB3DD5AF_AdjustorThunk,
	CSGPlane_set_normal_m946BA6635B964F0675FBC69F5EFC9BD95706D8BD_AdjustorThunk,
	CSGPlane_get_pointOnPlane_m1012EAAA2220CEBC32B79E79270719828DED02DB_AdjustorThunk,
	CSGPlane__ctor_m93D9A7BCD9C724AF1005E13B7DF4FCA19D50963F_AdjustorThunk,
	CSGPlane__ctor_m31DC6D44D2B5A5C6C686E45A5523EF7E4ECA77F2_AdjustorThunk,
	CSGPlane__ctor_m946B8981BCACE3B94F2C918095A880F6066909A3_AdjustorThunk,
	CSGPlane__ctor_m1B9A3537E44681EF3C089E210173F1B6556D8130_AdjustorThunk,
	CSGPlane__ctor_m73F3572882DC7E581E36ECD29BB0F0E60CA2F402_AdjustorThunk,
	CSGPlane__ctor_m88F23F335892984073C7316F1F802A43DD09FCA1_AdjustorThunk,
	CSGPlane_RayIntersection_m617BEB5AFB352E3526C5D3980CEEDA793F89DD0B_AdjustorThunk,
	CSGPlane_TryRayIntersection_m52F708225B88741AAA1661ABCEBAF62506D4CFF5_AdjustorThunk,
	CSGPlane_LineIntersection_m28071A87059ADCC51A483F683BEC2C33E23A5580_AdjustorThunk,
	CSGPlane_Intersection_m77828575E659430171184D441835224E158F1898,
	CSGPlane_Distance_mC49DFD8F85C7A7B4B0B38A5D3D0AD8A7A30476AC_AdjustorThunk,
	CSGPlane_Normalize_m292BE6E38E9196D6B445DB86FA93CF7E16D5A581_AdjustorThunk,
	CSGPlane_Transform_m031F60F82E91AE311EFB8318D5C419C76A9195DB_AdjustorThunk,
	CSGPlane_Negated_m197727B4586DC28A9A753E82FFE636D05DB5F9E5_AdjustorThunk,
	CSGPlane_Translated_mAB1A8898F2402DAF9F25A44F2A503566C240443D_AdjustorThunk,
	CSGPlane_Project_m579D50E710FBC39A748ABEE2AAFCC2C6E9241B94_AdjustorThunk,
	CSGPlane_GetHashCode_mA53E0BF2F7FDAA715588FBA59C4C49E6DFB1E33B_AdjustorThunk,
	CSGPlane_Equals_m222BE1402EBF61F22DFBFFFC942F6E703E78C9A3_AdjustorThunk,
	CSGPlane_Equals_m6795D48FD41DC9CD59359B5D7C234A8904B1139D_AdjustorThunk,
	CSGPlane_op_Equality_m5DDC85F3465F45DB2045D95DAC2CCE87D4DD1E00,
	CSGPlane_op_Inequality_m351E7463AB7335D358F0B8A814FFE638F67965B9,
	CSGPlane_ToString_mCA19197B427027F9BCBE4615D6505A3A75F68ED8_AdjustorThunk,
	HalfEdge__ctor_m3A641C7DC2F71E2E1BFC0B960494AA9258BA8F1A_AdjustorThunk,
	Polygon__ctor_m2E15F7F95331FC3DB64EE581891D0F1746349511,
	ControlMesh_get_Valid_m271463949CA9305A922B043B3678B1D5951014DC,
	ControlMesh_set_Valid_m3902886AF73159E55499A3FAE5C8C77FE3BE06FC,
	ControlMesh__ctor_m93DFAB3F904281143760B4713D2A2AE89453E3E4,
	ControlMesh__ctor_m12DF3940EE5E54F6E41903867A37567A0D4743B4,
	ControlMesh_SetDirty_mE2DD5AB15A4B2717E50ADF05196B50C5995AA093,
	ControlMesh_Reset_m70A47B47DED0106F851EF950DDCA2FAF497800F1,
	ControlMesh_CopyFrom_m010324448CDA71EAC8776694628A7307CE186C42,
	ControlMesh_Clone_m22DA0E5DE55549B13B5E4B2FB235A9764CC1E912,
	ControlMesh_GetVertex_mE0F471B92EB47F38895C9D97A12F066B885F9662,
	ControlMesh_GetVertices_mD78ABF872903C01463776ADD972AC9667D4EB3FB,
	ControlMesh_GetVertex_m30F61A0C56229557886C7B4CF27481530A929D43,
	ControlMesh_GetVertexIndex_mDFAB5A9F8DD849F862D5899498E929F6334949FB,
	ControlMesh_GetVertexIndex_mC4DC0CC7D01599B06B5F8AF0FFB5EFB3E453B628,
	ControlMesh_GetTwinEdgeVertex_mC68325F10BC9FC6E6E2DB022451671DAD33F33C4,
	ControlMesh_GetTwinEdgeVertex_m8AECF1CB123CB39ADCBBCD9AE0833522577DDE85,
	ControlMesh_GetTwinEdgeVertexIndex_m4E5F9B2AEAA2E5BC5FDB7B5DD374167EE366ED14,
	ControlMesh_GetTwinEdgeVertexIndex_m062983016F73A2E02A2E9A626857946E4FD4B027,
	ControlMesh_GetTwinEdgeIndex_m5F64058A123F28EC0C48F645E4F67057E3AE20C1,
	ControlMesh_GetTwinEdgeIndex_mEB16E205B788C2614EFB4CD2EA3A1F62C224B7F3,
	ControlMesh_GetTwinEdgePolygonIndex_m7E8B0F67E65B6D92C4DDBEA3A0EE09341CB953C8,
	ControlMesh_GetEdgePolygonIndex_m7A1C2726753E24BA4A0043CC12B68A74E3019AA6,
	ControlMesh_GetNextEdgeIndexAroundVertex_m8F5FD766D00A1C04EF36ACBCE3B91E664A096145,
	ControlMesh_GetPrevEdgeIndex_m90955D2563E0C0B7374C23D1FAC8D31F28E4ED1F,
	ControlMesh_GetNextEdgeIndex_m0E2F6BC7E88481CBDE3C3B166BBA15BB94D744D6,
	Shape__ctor_m8D108B1E6FEEE23E0E75E1C7EF6D36D373B9BD65,
	Surface_GenerateLocalBrushSpaceToPlaneSpaceMatrix_mDE118B1C4B1C0070D3DC37B7908420619C45E63C_AdjustorThunk,
	Surface_ToString_mD0C7F664F6938BB3A498148515FEE938E8CB5BDB_AdjustorThunk,
	TexGen__ctor_m994C4FCD11459DD9C28D4BA65F0F6C48C472C1CE_AdjustorThunk,
	TexGen_GeneratePlaneSpaceToTextureSpaceMatrix_m00F50F6E70073335B92029EE5A843D0DD5409FD8_AdjustorThunk,
	GeneratedMeshContents_CopyTo_m8158F1BC8FB9187508D3B015B0D5A1CEAB323088,
	GeneratedMeshContents__ctor_mE4E0696A024F80FD5CAD7950FD95B0CAAB64A7F1,
	GeneratedMeshDescription_Equals_m55582FBC39FB4CFF4C0E637E7E8A2C104E81A0EF_AdjustorThunk,
	GeneratedMeshDescription_GetHashCode_mAEEFC5F7B260A59A966113874DC4AA3F77277C0B_AdjustorThunk,
	GeneratedMeshDescription_op_Equality_m7DD880CECD1AC75CC2C1193DCAA7585CB0721B10,
	GeneratedMeshDescription_op_Inequality_m50A84C22C19C8CD2DC1909CBC8363C992550AD06,
	MeshQuery__ctor_mAFFC39222E6FE5BD686A17A40FDF664DCFB6210D_AdjustorThunk,
	MeshQuery_get_LayerQuery_m4703C9835C13FCF59B4B409303FCB42E0532FA36_AdjustorThunk,
	MeshQuery_set_LayerQuery_mC9946EC13A1FD87A223B830FF7A421B68D883374_AdjustorThunk,
	MeshQuery_get_LayerQueryMask_mC503693548152B334FD4BE66859983D6D045C84D_AdjustorThunk,
	MeshQuery_set_LayerQueryMask_mDE272289C3E300914F392DFC5F893CE18D83DD57_AdjustorThunk,
	MeshQuery_get_LayerParameterIndex_mDCB4D744B10C68E3B7D28DE40ECFEE24CD264186_AdjustorThunk,
	MeshQuery_set_LayerParameterIndex_m8369D65DEFA1422B838AE8E56EAEEBFB5B733A6D_AdjustorThunk,
	MeshQuery_get_UsedVertexChannels_mA414AB75F94B51B487825C0DD43BA83759836E6A_AdjustorThunk,
	MeshQuery_set_UsedVertexChannels_mE603C1D0593AA6FB6012D060B4047A3F6E8BD3F0_AdjustorThunk,
	MeshQuery_ToString_m21B8DDA8083D9591013A55045A35F04BFFE56BD5_AdjustorThunk,
	MeshQuery_op_Equality_m3A2B9D930633CA85ABB5CE3A89E3E6C176D0F7BE,
	MeshQuery_op_Inequality_m754576CB4F0C360EF2DDB08983FCAB87513DB3FC,
	MeshQuery_Equals_mA5BDAED46077D6560E86D42CF3F3C202C31D76CA_AdjustorThunk,
	MeshQuery_Equals_m855368ED8A4BB10172F924F3050916596BBEFB74_AdjustorThunk,
	MeshQuery_Equals_m8F0D1975C3DC6414A0B3917BF3A3E788138D5366,
	MeshQuery_GetHashCode_m5FBF184A4ED516B0A600B2B4C0DC2C1C4DDE1B10_AdjustorThunk,
	CSGBrush__ctor_m954F3029A94264D30A3A35AE1343EF327BC20364,
	CSGModel_get_IsRenderable_m7523B8AB0B0889D549728402BB305A85203FC394,
	CSGModel_get_IsTwoSidedShadows_m51838E2565C55977464F948440710B12DC2FFF0C,
	CSGModel_get_HaveCollider_m354F9EAB9A3AA0F5EEC799199817E094DED64A6E,
	CSGModel_get_IsTrigger_mC4B35628103C632A811008B15BA795DC4DF92AAE,
	CSGModel_get_InvertedWorld_m6C5FA2F7E53BB1F4A8DD0D54BDD19BC19DA5DF5B,
	CSGModel_get_SetColliderConvex_mD9B220CD338F81D758CD3D4F539AD46BCD0DA848,
	CSGModel_get_NeedAutoUpdateRigidBody_m2F9D1D4B737B6837B8C84F8D255B51F5FFE6AE35,
	CSGModel_get_PreserveUVs_mF08E71E035F262083F67FE8CCDACC429E23428D9,
	CSGModel_get_StitchLightmapSeams_mEE2D674002DB25512A1925E08AEDB5C1652804D1,
	CSGModel_get_AutoRebuildUVs_m1307BCFF754F6A001AD138571A0B0704F6AA87AC,
	CSGModel_get_IgnoreNormals_m557DC787F6EB5E2376CD7DF8CDDE6BBAD0EFEA34,
	CSGModel__ctor_m156A0623EA2BF1DF350B2D71E5F745CA8854FB2D,
	CSGModel__cctor_m68D842D99DC57996CD00259E16E32A676EF40B3B,
	CSGNode__ctor_mE9B0ADCA67AFD7B22004171B6E48C7C386FB3EE0,
	CSGOperation__ctor_mD3CA12D90C37AE6BE3C6C8D1FE9402E80FFAB050,
};
static const int32_t s_InvokerIndices[110] = 
{
	122,
	122,
	122,
	94,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	0,
	1130,
	1131,
	1130,
	2244,
	2245,
	1128,
	2246,
	1101,
	2247,
	2248,
	1133,
	2249,
	2250,
	2251,
	23,
	1115,
	2252,
	2253,
	1117,
	10,
	2254,
	9,
	2255,
	2255,
	14,
	2256,
	140,
	114,
	31,
	23,
	26,
	23,
	23,
	26,
	14,
	2257,
	28,
	2258,
	374,
	2259,
	2258,
	2257,
	2259,
	374,
	1108,
	37,
	374,
	374,
	37,
	37,
	37,
	23,
	1114,
	14,
	27,
	1114,
	26,
	23,
	9,
	10,
	2260,
	2260,
	1769,
	10,
	32,
	10,
	32,
	114,
	31,
	114,
	31,
	14,
	2261,
	2261,
	9,
	2262,
	2261,
	10,
	23,
	114,
	114,
	114,
	114,
	114,
	114,
	114,
	114,
	114,
	114,
	114,
	23,
	3,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	110,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
