﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Readme::.ctor()
extern void Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C (void);
// 0x00000002 System.Void AudioMenu::Awake()
extern void AudioMenu_Awake_mEA73D1E5E5F2F4C191332ABC1399405B6532B6B6 (void);
// 0x00000003 System.Void AudioMenu::ChangeVolumeMaster(System.Single)
extern void AudioMenu_ChangeVolumeMaster_m28440082A8F637D26E016F0917B2441385B72B61 (void);
// 0x00000004 System.Void AudioMenu::.ctor()
extern void AudioMenu__ctor_mB637F1E119EFD9BCA93BEB29D773F4AE4450932D (void);
// 0x00000005 System.Void CameraControl::Start()
extern void CameraControl_Start_m3465A1D3F245B238C70894B541053010EF48569C (void);
// 0x00000006 System.Void CameraControl::Update()
extern void CameraControl_Update_mADA15FA22CFBD70CC2D84EB6270E470B770C253C (void);
// 0x00000007 System.Void CameraControl::.ctor()
extern void CameraControl__ctor_m40CDAD0D4D59CE6C6EC7BD8A5EA9048B5DE4C162 (void);
// 0x00000008 System.Void ControlPlayer::Start()
extern void ControlPlayer_Start_mA6976DFFB9DDE03F523B3527730C6ADE8D65993E (void);
// 0x00000009 System.Void ControlPlayer::Update()
extern void ControlPlayer_Update_m5C9FE2DFE2691A4BD5B0FE4BAEDBE612EC744E57 (void);
// 0x0000000A System.Void ControlPlayer::FixedUpdate()
extern void ControlPlayer_FixedUpdate_m837A96938552946C7DB89E289C5D322B1FDA3A34 (void);
// 0x0000000B System.Void ControlPlayer::.ctor()
extern void ControlPlayer__ctor_m07B8E2A58A9B6C4039667826780CE9B387F5145C (void);
// 0x0000000C System.Void Explocion::Start()
extern void Explocion_Start_m1408D753813A0AA0715BD911BB184D4C86E181F7 (void);
// 0x0000000D System.Void Explocion::Main()
extern void Explocion_Main_m7CEE1A7AC344B65E6A5B4DD240E19F14EDA6282D (void);
// 0x0000000E System.Void Explocion::crearcubo(UnityEngine.Vector3)
extern void Explocion_crearcubo_m6869D2FE6115BF2E420765F62A485EC1BC208655 (void);
// 0x0000000F System.Void Explocion::.ctor()
extern void Explocion__ctor_mD0F385BDB7C39E25FD4010028152F3ECEB641F7A (void);
// 0x00000010 System.Void Boss::Start()
extern void Boss_Start_m41E8DE6E2E2C0CAE1529BB89B5173BCB2B0C9416 (void);
// 0x00000011 System.Collections.IEnumerator Boss::Esperar()
extern void Boss_Esperar_mFA83371A099A39E89A32072665901E5DB95F4AC2 (void);
// 0x00000012 System.Void Boss::OnCollisionEnter(UnityEngine.Collision)
extern void Boss_OnCollisionEnter_m068B51F65CAFE229E09796861419F5EFC28E55BB (void);
// 0x00000013 System.Void Boss::.ctor()
extern void Boss__ctor_m9D713F5E7D7456ADC925AF9C6AF65062E39AFB82 (void);
// 0x00000014 System.Void Cubo::OnCollisionEnter(UnityEngine.Collision)
extern void Cubo_OnCollisionEnter_mBAF137EB2889C64115AB7EBA5E654128529F10D2 (void);
// 0x00000015 System.Void Cubo::.ctor()
extern void Cubo__ctor_mB6C1E4D761EC14867453F81696AC13E5E099573F (void);
// 0x00000016 System.Void Enemigo::Start()
extern void Enemigo_Start_mB46AEBD27A2237AA6155F659105714837F7C943E (void);
// 0x00000017 System.Void Enemigo::damage()
extern void Enemigo_damage_m04415499128401D171AC5E455C94BD9DDAE98735 (void);
// 0x00000018 System.Void Enemigo::OnTriggerEnter(UnityEngine.Collider)
extern void Enemigo_OnTriggerEnter_m6B4D5A189237AD067840DA26D337676D61C2E1F2 (void);
// 0x00000019 System.Void Enemigo::.ctor()
extern void Enemigo__ctor_m2B75C10A622F7343F26F8E8DDA23D6A91A3898E3 (void);
// 0x0000001A System.Void Plataforma::Update()
extern void Plataforma_Update_mBC0F35C9FCB3750F0E8EE76107ABCE7036C690A7 (void);
// 0x0000001B System.Void Plataforma::Subir()
extern void Plataforma_Subir_mCB10FA067D0ED38393BE73FCE22F4BCEA380A72C (void);
// 0x0000001C System.Void Plataforma::Bajar()
extern void Plataforma_Bajar_mFFD43D7556071974DDC229E4325166AEEF50434A (void);
// 0x0000001D System.Void Plataforma::.ctor()
extern void Plataforma__ctor_m0731064900D4E868852A3238F25E9391B71D9DAB (void);
// 0x0000001E System.Void guardia0::Update()
extern void guardia0_Update_m43BA6BD195EA6F35EC32A6F8B426A0AF817E304B (void);
// 0x0000001F System.Void guardia0::iz()
extern void guardia0_iz_mA66AA235F4E0D1CF32B9CB46044319926F5F5AAA (void);
// 0x00000020 System.Void guardia0::der()
extern void guardia0_der_m68CE8C8949A311183563CB59CF1B516DA61F8405 (void);
// 0x00000021 System.Void guardia0::.ctor()
extern void guardia0__ctor_mF8D409BFD19B6A321EE6783C0F8A8224D130675F (void);
// 0x00000022 System.Void guardia1::Update()
extern void guardia1_Update_m9F6B6F5886694A18E4A939613238D7477359B871 (void);
// 0x00000023 System.Void guardia1::iz()
extern void guardia1_iz_m621B6EB51A6D305E94CEE2306F736B6F86B3CF9B (void);
// 0x00000024 System.Void guardia1::der()
extern void guardia1_der_mAC00A619636FCD1F1FE7C4C19CBFABA0C0F9995D (void);
// 0x00000025 System.Void guardia1::.ctor()
extern void guardia1__ctor_m9A2EAF92B9A07FF6D638EC1597BD1342F5DD705E (void);
// 0x00000026 System.Void guardia2::Update()
extern void guardia2_Update_m95B95846A50EE254C09B66F9CBB0FA2939E8F0E6 (void);
// 0x00000027 System.Void guardia2::iz()
extern void guardia2_iz_m1E972CA2ABE454D6695310A5343CB8F046C08206 (void);
// 0x00000028 System.Void guardia2::der()
extern void guardia2_der_m1359479EB39EBB25EA5E535C94A0D075A5CC70C8 (void);
// 0x00000029 System.Void guardia2::.ctor()
extern void guardia2__ctor_m62237334F6B29696FC6A0088FB27B2B40D211D52 (void);
// 0x0000002A System.Void Menu::Jugar()
extern void Menu_Jugar_m671475320629921318FACFEE9DFAC12570189AFF (void);
// 0x0000002B System.Void Menu::Salir()
extern void Menu_Salir_mB1B272C802AB1586398225D3E4E6A898FA2C07FE (void);
// 0x0000002C System.Void Menu::.ctor()
extern void Menu__ctor_m87A18732D59A6382889C2771A372B622FD6CD58D (void);
// 0x0000002D System.Void Player::Start()
extern void Player_Start_mD6E1D31879EB485356D1C22C8AE12C5DF6392E79 (void);
// 0x0000002E System.Void Player::Update()
extern void Player_Update_m10202D3DF1DE1AD29B6B00E0092D9C41BD3861F7 (void);
// 0x0000002F System.Boolean Player::EstaEnPiso()
extern void Player_EstaEnPiso_mBCEC470C183699050998EC996A08C515ED4FFD94 (void);
// 0x00000030 System.Void Player::OnTriggerEnter(UnityEngine.Collider)
extern void Player_OnTriggerEnter_mA6ABD2C1D67397209766117A4403F33A33AC5090 (void);
// 0x00000031 System.Void Player::OnCollisionEnter(UnityEngine.Collision)
extern void Player_OnCollisionEnter_mF61214330F7892D625B413AF21709E4C4AC7E848 (void);
// 0x00000032 System.Void Player::textoss()
extern void Player_textoss_m4BFAB6C6950FD33600C93B407AE739B5C6D0083A (void);
// 0x00000033 System.Void Player::.ctor()
extern void Player__ctor_mAEC38956EFD0E61D848D4E5AFB83BABCE2DF1E23 (void);
// 0x00000034 System.Void Rotator::Update()
extern void Rotator_Update_m5EEA78DA43EE3059DD3A2B5E0A378709E0DF21FA (void);
// 0x00000035 System.Void Rotator::.ctor()
extern void Rotator__ctor_m6FA12381051396D6275FE1B56FBD254201D5B127 (void);
// 0x00000036 System.Void SideWays::Start()
extern void SideWays_Start_m92B2655C4E1FD7E93579687E6DEF633A01B35292 (void);
// 0x00000037 System.Void SideWays::Update()
extern void SideWays_Update_mFC47A85BA15861E96162B7221D483B4BD40C6FEA (void);
// 0x00000038 System.Void SideWays::OnDrawGizmos()
extern void SideWays_OnDrawGizmos_m39A1DE2AB5EC1617B589C7862440F37B1C6E2C23 (void);
// 0x00000039 System.Void SideWays::.ctor()
extern void SideWays__ctor_mBAB9A336D721EA31320504610605375EB79DA416 (void);
// 0x0000003A System.Void UpDown::Update()
extern void UpDown_Update_m7F4CFC7EE7AB4A88C51F82CD446C4267DE23DA47 (void);
// 0x0000003B System.Void UpDown::Subir()
extern void UpDown_Subir_m2308252EBC9ABA6076C1E50BCB19D45770007147 (void);
// 0x0000003C System.Void UpDown::Bajar()
extern void UpDown_Bajar_mC2800DEF27832BF2EE1F2A949BE790C6670D878F (void);
// 0x0000003D System.Void UpDown::.ctor()
extern void UpDown__ctor_m455C91485231D1FD0EEF11AA10610E38C117AAA4 (void);
// 0x0000003E System.Void UpDownBlue::Update()
extern void UpDownBlue_Update_m549DEADEF4C5C510280343CEEB0CF2B234890B22 (void);
// 0x0000003F System.Void UpDownBlue::subir()
extern void UpDownBlue_subir_m561856BE95C55E0E7C23F66D984CAFB1D39AF6A8 (void);
// 0x00000040 System.Void UpDownBlue::bajar()
extern void UpDownBlue_bajar_m85464797A9EE929E11C6DEFE60377E6D9BCD09B6 (void);
// 0x00000041 System.Void UpDownBlue::.ctor()
extern void UpDownBlue__ctor_mA852A1F8D6EF1AC43D264C3B7BBF96F6D39ED560 (void);
// 0x00000042 System.Void Zone::OnTriggerEnter(UnityEngine.Collider)
extern void Zone_OnTriggerEnter_m1CD8E0D77260D94F8160A41A539A4A188D1BF5B8 (void);
// 0x00000043 System.Void Zone::.ctor()
extern void Zone__ctor_m64ABF2AB830869565387F6DBDFA61DE9B2661F1A (void);
// 0x00000044 System.Void camera::Start()
extern void camera_Start_m22865970959C4F2865ECD1828AEB4EC189FEC283 (void);
// 0x00000045 System.Void camera::LateUpdate()
extern void camera_LateUpdate_mAA2A9AE23412061599039FF4B3FB262CEA570E0A (void);
// 0x00000046 System.Void camera::Update()
extern void camera_Update_m48A5F841D659E26CF79A195FFA2C56385FFDB815 (void);
// 0x00000047 System.Void camera::.ctor()
extern void camera__ctor_mA8AD71605FCC052C1D33B8EBF59F10148A8AF334 (void);
// 0x00000048 System.Void camera1::Start()
extern void camera1_Start_m5A4E4F92F06624D6BB845E7FF55092EC49A064CF (void);
// 0x00000049 System.Void camera1::LateUpdate()
extern void camera1_LateUpdate_m5162B730A40A560A45CF604039B5D09982DD16C6 (void);
// 0x0000004A System.Void camera1::Update()
extern void camera1_Update_m161F990BF018BE757AD27901B9DDB1EA93EF46D1 (void);
// 0x0000004B System.Void camera1::.ctor()
extern void camera1__ctor_m181C0E2893E541961A246AD81C6768EE111965E4 (void);
// 0x0000004C System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8 (void);
// 0x0000004D UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47 (void);
// 0x0000004E System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F (void);
// 0x0000004F System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E (void);
// 0x00000050 System.Void Readme_Section::.ctor()
extern void Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96 (void);
// 0x00000051 System.Void Boss_<Esperar>d__4::.ctor(System.Int32)
extern void U3CEsperarU3Ed__4__ctor_m92F02DD06B1059F0C1913B22E2EE6355DDAC9AC9 (void);
// 0x00000052 System.Void Boss_<Esperar>d__4::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__4_System_IDisposable_Dispose_m41893EBB1E730F1C339A066B5E34A3E727D07A13 (void);
// 0x00000053 System.Boolean Boss_<Esperar>d__4::MoveNext()
extern void U3CEsperarU3Ed__4_MoveNext_m4FE0BE8752C230B803D477CA7A8933CF34A6A7A7 (void);
// 0x00000054 System.Object Boss_<Esperar>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65507FD6818F6FA1D15BC854B465F2827B5F5BEB (void);
// 0x00000055 System.Void Boss_<Esperar>d__4::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__4_System_Collections_IEnumerator_Reset_m5603A2AB5F676C8CA256B63A88BACDBD674F510C (void);
// 0x00000056 System.Object Boss_<Esperar>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__4_System_Collections_IEnumerator_get_Current_m68AB3F528F10772E5DEB50A6AE8939673B6C8015 (void);
// 0x00000057 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E (void);
// 0x00000058 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247 (void);
// 0x00000059 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController_CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282 (void);
// 0x0000005A System.Void UnityTemplateProjects.SimpleCameraController_CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623 (void);
// 0x0000005B System.Void UnityTemplateProjects.SimpleCameraController_CameraState::.ctor()
extern void CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322 (void);
static Il2CppMethodPointer s_methodPointers[91] = 
{
	Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C,
	AudioMenu_Awake_mEA73D1E5E5F2F4C191332ABC1399405B6532B6B6,
	AudioMenu_ChangeVolumeMaster_m28440082A8F637D26E016F0917B2441385B72B61,
	AudioMenu__ctor_mB637F1E119EFD9BCA93BEB29D773F4AE4450932D,
	CameraControl_Start_m3465A1D3F245B238C70894B541053010EF48569C,
	CameraControl_Update_mADA15FA22CFBD70CC2D84EB6270E470B770C253C,
	CameraControl__ctor_m40CDAD0D4D59CE6C6EC7BD8A5EA9048B5DE4C162,
	ControlPlayer_Start_mA6976DFFB9DDE03F523B3527730C6ADE8D65993E,
	ControlPlayer_Update_m5C9FE2DFE2691A4BD5B0FE4BAEDBE612EC744E57,
	ControlPlayer_FixedUpdate_m837A96938552946C7DB89E289C5D322B1FDA3A34,
	ControlPlayer__ctor_m07B8E2A58A9B6C4039667826780CE9B387F5145C,
	Explocion_Start_m1408D753813A0AA0715BD911BB184D4C86E181F7,
	Explocion_Main_m7CEE1A7AC344B65E6A5B4DD240E19F14EDA6282D,
	Explocion_crearcubo_m6869D2FE6115BF2E420765F62A485EC1BC208655,
	Explocion__ctor_mD0F385BDB7C39E25FD4010028152F3ECEB641F7A,
	Boss_Start_m41E8DE6E2E2C0CAE1529BB89B5173BCB2B0C9416,
	Boss_Esperar_mFA83371A099A39E89A32072665901E5DB95F4AC2,
	Boss_OnCollisionEnter_m068B51F65CAFE229E09796861419F5EFC28E55BB,
	Boss__ctor_m9D713F5E7D7456ADC925AF9C6AF65062E39AFB82,
	Cubo_OnCollisionEnter_mBAF137EB2889C64115AB7EBA5E654128529F10D2,
	Cubo__ctor_mB6C1E4D761EC14867453F81696AC13E5E099573F,
	Enemigo_Start_mB46AEBD27A2237AA6155F659105714837F7C943E,
	Enemigo_damage_m04415499128401D171AC5E455C94BD9DDAE98735,
	Enemigo_OnTriggerEnter_m6B4D5A189237AD067840DA26D337676D61C2E1F2,
	Enemigo__ctor_m2B75C10A622F7343F26F8E8DDA23D6A91A3898E3,
	Plataforma_Update_mBC0F35C9FCB3750F0E8EE76107ABCE7036C690A7,
	Plataforma_Subir_mCB10FA067D0ED38393BE73FCE22F4BCEA380A72C,
	Plataforma_Bajar_mFFD43D7556071974DDC229E4325166AEEF50434A,
	Plataforma__ctor_m0731064900D4E868852A3238F25E9391B71D9DAB,
	guardia0_Update_m43BA6BD195EA6F35EC32A6F8B426A0AF817E304B,
	guardia0_iz_mA66AA235F4E0D1CF32B9CB46044319926F5F5AAA,
	guardia0_der_m68CE8C8949A311183563CB59CF1B516DA61F8405,
	guardia0__ctor_mF8D409BFD19B6A321EE6783C0F8A8224D130675F,
	guardia1_Update_m9F6B6F5886694A18E4A939613238D7477359B871,
	guardia1_iz_m621B6EB51A6D305E94CEE2306F736B6F86B3CF9B,
	guardia1_der_mAC00A619636FCD1F1FE7C4C19CBFABA0C0F9995D,
	guardia1__ctor_m9A2EAF92B9A07FF6D638EC1597BD1342F5DD705E,
	guardia2_Update_m95B95846A50EE254C09B66F9CBB0FA2939E8F0E6,
	guardia2_iz_m1E972CA2ABE454D6695310A5343CB8F046C08206,
	guardia2_der_m1359479EB39EBB25EA5E535C94A0D075A5CC70C8,
	guardia2__ctor_m62237334F6B29696FC6A0088FB27B2B40D211D52,
	Menu_Jugar_m671475320629921318FACFEE9DFAC12570189AFF,
	Menu_Salir_mB1B272C802AB1586398225D3E4E6A898FA2C07FE,
	Menu__ctor_m87A18732D59A6382889C2771A372B622FD6CD58D,
	Player_Start_mD6E1D31879EB485356D1C22C8AE12C5DF6392E79,
	Player_Update_m10202D3DF1DE1AD29B6B00E0092D9C41BD3861F7,
	Player_EstaEnPiso_mBCEC470C183699050998EC996A08C515ED4FFD94,
	Player_OnTriggerEnter_mA6ABD2C1D67397209766117A4403F33A33AC5090,
	Player_OnCollisionEnter_mF61214330F7892D625B413AF21709E4C4AC7E848,
	Player_textoss_m4BFAB6C6950FD33600C93B407AE739B5C6D0083A,
	Player__ctor_mAEC38956EFD0E61D848D4E5AFB83BABCE2DF1E23,
	Rotator_Update_m5EEA78DA43EE3059DD3A2B5E0A378709E0DF21FA,
	Rotator__ctor_m6FA12381051396D6275FE1B56FBD254201D5B127,
	SideWays_Start_m92B2655C4E1FD7E93579687E6DEF633A01B35292,
	SideWays_Update_mFC47A85BA15861E96162B7221D483B4BD40C6FEA,
	SideWays_OnDrawGizmos_m39A1DE2AB5EC1617B589C7862440F37B1C6E2C23,
	SideWays__ctor_mBAB9A336D721EA31320504610605375EB79DA416,
	UpDown_Update_m7F4CFC7EE7AB4A88C51F82CD446C4267DE23DA47,
	UpDown_Subir_m2308252EBC9ABA6076C1E50BCB19D45770007147,
	UpDown_Bajar_mC2800DEF27832BF2EE1F2A949BE790C6670D878F,
	UpDown__ctor_m455C91485231D1FD0EEF11AA10610E38C117AAA4,
	UpDownBlue_Update_m549DEADEF4C5C510280343CEEB0CF2B234890B22,
	UpDownBlue_subir_m561856BE95C55E0E7C23F66D984CAFB1D39AF6A8,
	UpDownBlue_bajar_m85464797A9EE929E11C6DEFE60377E6D9BCD09B6,
	UpDownBlue__ctor_mA852A1F8D6EF1AC43D264C3B7BBF96F6D39ED560,
	Zone_OnTriggerEnter_m1CD8E0D77260D94F8160A41A539A4A188D1BF5B8,
	Zone__ctor_m64ABF2AB830869565387F6DBDFA61DE9B2661F1A,
	camera_Start_m22865970959C4F2865ECD1828AEB4EC189FEC283,
	camera_LateUpdate_mAA2A9AE23412061599039FF4B3FB262CEA570E0A,
	camera_Update_m48A5F841D659E26CF79A195FFA2C56385FFDB815,
	camera__ctor_mA8AD71605FCC052C1D33B8EBF59F10148A8AF334,
	camera1_Start_m5A4E4F92F06624D6BB845E7FF55092EC49A064CF,
	camera1_LateUpdate_m5162B730A40A560A45CF604039B5D09982DD16C6,
	camera1_Update_m161F990BF018BE757AD27901B9DDB1EA93EF46D1,
	camera1__ctor_m181C0E2893E541961A246AD81C6768EE111965E4,
	SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8,
	SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47,
	SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F,
	SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E,
	Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96,
	U3CEsperarU3Ed__4__ctor_m92F02DD06B1059F0C1913B22E2EE6355DDAC9AC9,
	U3CEsperarU3Ed__4_System_IDisposable_Dispose_m41893EBB1E730F1C339A066B5E34A3E727D07A13,
	U3CEsperarU3Ed__4_MoveNext_m4FE0BE8752C230B803D477CA7A8933CF34A6A7A7,
	U3CEsperarU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65507FD6818F6FA1D15BC854B465F2827B5F5BEB,
	U3CEsperarU3Ed__4_System_Collections_IEnumerator_Reset_m5603A2AB5F676C8CA256B63A88BACDBD674F510C,
	U3CEsperarU3Ed__4_System_Collections_IEnumerator_get_Current_m68AB3F528F10772E5DEB50A6AE8939673B6C8015,
	CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E,
	CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247,
	CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282,
	CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623,
	CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322,
};
static const int32_t s_InvokerIndices[91] = 
{
	23,
	23,
	296,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1131,
	23,
	23,
	14,
	26,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1130,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	26,
	1131,
	1306,
	26,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	91,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
